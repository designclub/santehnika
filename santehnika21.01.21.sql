/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50647
Source Host           : localhost:3306
Source Database       : santehnika

Target Server Type    : MYSQL
Target Server Version : 50647
File Encoding         : 65001

Date: 2021-01-21 10:35:48
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `yupe_blog_blog`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_blog_blog`;
CREATE TABLE `yupe_blog_blog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `description` text,
  `icon` varchar(250) NOT NULL DEFAULT '',
  `slug` varchar(150) NOT NULL,
  `lang` char(2) DEFAULT NULL,
  `type` int(11) NOT NULL DEFAULT '1',
  `status` int(11) NOT NULL DEFAULT '1',
  `create_user_id` int(11) NOT NULL,
  `update_user_id` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `member_status` int(11) NOT NULL DEFAULT '1',
  `post_status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_blog_blog_slug_lang` (`slug`,`lang`),
  KEY `ix_yupe_blog_blog_create_user` (`create_user_id`),
  KEY `ix_yupe_blog_blog_update_user` (`update_user_id`),
  KEY `ix_yupe_blog_blog_status` (`status`),
  KEY `ix_yupe_blog_blog_type` (`type`),
  KEY `ix_yupe_blog_blog_create_date` (`create_time`),
  KEY `ix_yupe_blog_blog_update_date` (`update_time`),
  KEY `ix_yupe_blog_blog_lang` (`lang`),
  KEY `ix_yupe_blog_blog_slug` (`slug`),
  KEY `ix_yupe_blog_blog_category_id` (`category_id`),
  CONSTRAINT `fk_yupe_blog_blog_category_id` FOREIGN KEY (`category_id`) REFERENCES `yupe_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_blog_blog_create_user` FOREIGN KEY (`create_user_id`) REFERENCES `yupe_user_user` (`id`) ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_blog_blog_update_user` FOREIGN KEY (`update_user_id`) REFERENCES `yupe_user_user` (`id`) ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_blog_blog
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_blog_post`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_blog_post`;
CREATE TABLE `yupe_blog_post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `blog_id` int(11) NOT NULL,
  `create_user_id` int(11) NOT NULL,
  `update_user_id` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `publish_time` int(11) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `lang` char(2) DEFAULT NULL,
  `title` varchar(250) NOT NULL,
  `quote` text,
  `content` text NOT NULL,
  `link` varchar(250) NOT NULL DEFAULT '',
  `status` int(11) NOT NULL DEFAULT '0',
  `comment_status` int(11) NOT NULL DEFAULT '1',
  `create_user_ip` varchar(20) NOT NULL,
  `access_type` int(11) NOT NULL DEFAULT '1',
  `meta_keywords` varchar(250) NOT NULL DEFAULT '',
  `meta_description` varchar(250) NOT NULL DEFAULT '',
  `image` varchar(300) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `meta_title` varchar(250) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_blog_post_lang_slug` (`slug`,`lang`),
  KEY `ix_yupe_blog_post_blog_id` (`blog_id`),
  KEY `ix_yupe_blog_post_create_user_id` (`create_user_id`),
  KEY `ix_yupe_blog_post_update_user_id` (`update_user_id`),
  KEY `ix_yupe_blog_post_status` (`status`),
  KEY `ix_yupe_blog_post_access_type` (`access_type`),
  KEY `ix_yupe_blog_post_comment_status` (`comment_status`),
  KEY `ix_yupe_blog_post_lang` (`lang`),
  KEY `ix_yupe_blog_post_slug` (`slug`),
  KEY `ix_yupe_blog_post_publish_date` (`publish_time`),
  KEY `ix_yupe_blog_post_category_id` (`category_id`),
  CONSTRAINT `fk_yupe_blog_post_blog` FOREIGN KEY (`blog_id`) REFERENCES `yupe_blog_blog` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_blog_post_category_id` FOREIGN KEY (`category_id`) REFERENCES `yupe_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_blog_post_create_user` FOREIGN KEY (`create_user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_blog_post_update_user` FOREIGN KEY (`update_user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_blog_post
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_blog_post_to_tag`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_blog_post_to_tag`;
CREATE TABLE `yupe_blog_post_to_tag` (
  `post_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  PRIMARY KEY (`post_id`,`tag_id`),
  KEY `ix_yupe_blog_post_to_tag_post_id` (`post_id`),
  KEY `ix_yupe_blog_post_to_tag_tag_id` (`tag_id`),
  CONSTRAINT `fk_yupe_blog_post_to_tag_post_id` FOREIGN KEY (`post_id`) REFERENCES `yupe_blog_post` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_blog_post_to_tag_tag_id` FOREIGN KEY (`tag_id`) REFERENCES `yupe_blog_tag` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_blog_post_to_tag
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_blog_tag`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_blog_tag`;
CREATE TABLE `yupe_blog_tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_blog_tag_tag_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_blog_tag
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_blog_user_to_blog`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_blog_user_to_blog`;
CREATE TABLE `yupe_blog_user_to_blog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `blog_id` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `role` int(11) NOT NULL DEFAULT '1',
  `status` int(11) NOT NULL DEFAULT '1',
  `note` varchar(250) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_blog_user_to_blog_blog_user_to_blog_u_b` (`user_id`,`blog_id`),
  KEY `ix_yupe_blog_user_to_blog_blog_user_to_blog_user_id` (`user_id`),
  KEY `ix_yupe_blog_user_to_blog_blog_user_to_blog_id` (`blog_id`),
  KEY `ix_yupe_blog_user_to_blog_blog_user_to_blog_status` (`status`),
  KEY `ix_yupe_blog_user_to_blog_blog_user_to_blog_role` (`role`),
  CONSTRAINT `fk_yupe_blog_user_to_blog_blog_user_to_blog_blog_id` FOREIGN KEY (`blog_id`) REFERENCES `yupe_blog_blog` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_blog_user_to_blog_blog_user_to_blog_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_blog_user_to_blog
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_category_category`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_category_category`;
CREATE TABLE `yupe_category_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `slug` varchar(150) NOT NULL,
  `lang` char(2) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `image` varchar(250) DEFAULT NULL,
  `short_description` text,
  `description` text NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_category_category_alias_lang` (`slug`,`lang`),
  KEY `ix_yupe_category_category_parent_id` (`parent_id`),
  KEY `ix_yupe_category_category_status` (`status`),
  CONSTRAINT `fk_yupe_category_category_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `yupe_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_category_category
-- ----------------------------
INSERT INTO `yupe_category_category` VALUES ('1', null, 'novosti', 'ru', 'Новости', null, '', '', '1');

-- ----------------------------
-- Table structure for `yupe_comment_comment`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_comment_comment`;
CREATE TABLE `yupe_comment_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `model` varchar(100) NOT NULL,
  `model_id` int(11) NOT NULL,
  `url` varchar(150) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `name` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `text` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `ip` varchar(20) DEFAULT NULL,
  `level` int(11) DEFAULT '0',
  `root` int(11) DEFAULT '0',
  `lft` int(11) DEFAULT '0',
  `rgt` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ix_yupe_comment_comment_status` (`status`),
  KEY `ix_yupe_comment_comment_model_model_id` (`model`,`model_id`),
  KEY `ix_yupe_comment_comment_model` (`model`),
  KEY `ix_yupe_comment_comment_model_id` (`model_id`),
  KEY `ix_yupe_comment_comment_user_id` (`user_id`),
  KEY `ix_yupe_comment_comment_parent_id` (`parent_id`),
  KEY `ix_yupe_comment_comment_level` (`level`),
  KEY `ix_yupe_comment_comment_root` (`root`),
  KEY `ix_yupe_comment_comment_lft` (`lft`),
  KEY `ix_yupe_comment_comment_rgt` (`rgt`),
  CONSTRAINT `fk_yupe_comment_comment_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `yupe_comment_comment` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_comment_comment_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_comment_comment
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_contentblock_content_block`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_contentblock_content_block`;
CREATE TABLE `yupe_contentblock_content_block` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `code` varchar(100) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '1',
  `content` text NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_contentblock_content_block_code` (`code`),
  KEY `ix_yupe_contentblock_content_block_type` (`type`),
  KEY `ix_yupe_contentblock_content_block_status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_contentblock_content_block
-- ----------------------------
INSERT INTO `yupe_contentblock_content_block` VALUES ('1', 'Телефон:', 'telefon', '1', '<a href=\"tel:+73512234511\">+7 (351) <span class=\"color-orange\">223-45-11</span></a>', '', null, '1');
INSERT INTO `yupe_contentblock_content_block` VALUES ('2', 'E-mail:', 'e-mail', '1', '<a href=\"mailto:Santexnika174@yandex.ru\">Santexnika174@yandex.ru</a>', '', null, '1');
INSERT INTO `yupe_contentblock_content_block` VALUES ('3', 'Адрес:', 'adres', '1', '454048, г. Челябинск, ул. Доватора, 27', '', null, '1');
INSERT INTO `yupe_contentblock_content_block` VALUES ('4', 'Политика конфиденциальности', 'politika-konfidencialnosti', '1', '#', '', null, '1');
INSERT INTO `yupe_contentblock_content_block` VALUES ('5', 'Скрипты в шапке', 'skripty-v-shapke', '1', '', '', null, '1');
INSERT INTO `yupe_contentblock_content_block` VALUES ('6', 'Скрипты в футере', 'skripty-v-futere', '1', '', '', null, '1');
INSERT INTO `yupe_contentblock_content_block` VALUES ('7', 'График работы', 'grafik-raboty', '1', 'Пн–Вс, 09:00–20:00', '', null, '1');
INSERT INTO `yupe_contentblock_content_block` VALUES ('8', 'Карта яндекс', 'karta-yandeks', '1', 'https://yandex.ru/map-widget/v1/-/CCQt74SUXB', '', null, '1');
INSERT INTO `yupe_contentblock_content_block` VALUES ('9', 'Телефон2:', 'telefon2', '1', '<a href=\"tel:+73517011145\">+7 (351) 701-11-45</a>', '', null, '1');
INSERT INTO `yupe_contentblock_content_block` VALUES ('10', 'Наименование компании в футере', 'naimenovanie-kompanii-v-futere', '1', 'САНТЕХНИКА ДЛЯ ВСЕХ', '<p>в футере сайта</p>', null, '1');
INSERT INTO `yupe_contentblock_content_block` VALUES ('11', 'Текст про публичную оферту футер', 'tekst-pro-publichnuyu-ofertu-futer', '1', 'Цена и информация о наличии товара в интернет-магазине может отличаться от фактической цены и наличия в розничных магазинах “Сантехника для всех”. Изображения товара могут в незначительной мере отличаться от их фактического вида. Вся информация на сайте носит ознакомительный характер и не является публичной офертой.', '<p>в футере сайта</p>', null, '1');
INSERT INTO `yupe_contentblock_content_block` VALUES ('12', 'Текст про публичную оферту', 'tekst-pro-publichnuyu-ofertu', '1', 'Внимание! Информация о товарах, размещенная на сайте, не является публичной офертой, определяемой положениями Части 2 Статьи 437 Гражданского кодекса Российской Федерации. Производители вправе вносить изменения в технические характеристики, внешний вид и комплектацию товаров без предварительного уведомления. Уточняйте характеристики у наших менеджеров перед оформлением заказа.', '<p>в карточке товара в голубом блоке</p>', null, '1');
INSERT INTO `yupe_contentblock_content_block` VALUES ('13', 'Адрес 2:', 'adres-2', '1', '454901, г. Челябинск, Троицкий тракт, 21, ЧЕЛСИ номер отдела 28', '', null, '1');

-- ----------------------------
-- Table structure for `yupe_gallery_gallery`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_gallery_gallery`;
CREATE TABLE `yupe_gallery_gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `description` text,
  `status` int(11) NOT NULL DEFAULT '1',
  `owner` int(11) DEFAULT NULL,
  `preview_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `sort` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `ix_yupe_gallery_gallery_status` (`status`),
  KEY `ix_yupe_gallery_gallery_owner` (`owner`),
  KEY `fk_yupe_gallery_gallery_gallery_preview_to_image` (`preview_id`),
  KEY `fk_yupe_gallery_gallery_gallery_to_category` (`category_id`),
  KEY `ix_yupe_gallery_gallery_sort` (`sort`),
  CONSTRAINT `fk_yupe_gallery_gallery_gallery_preview_to_image` FOREIGN KEY (`preview_id`) REFERENCES `yupe_image_image` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_gallery_gallery_gallery_to_category` FOREIGN KEY (`category_id`) REFERENCES `yupe_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_gallery_gallery_owner` FOREIGN KEY (`owner`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_gallery_gallery
-- ----------------------------
INSERT INTO `yupe_gallery_gallery` VALUES ('1', 'Мы в социальных сетях', '<p>Социальные сети</p>', '1', '1', null, null, '1');
INSERT INTO `yupe_gallery_gallery` VALUES ('2', 'Логотипы платежных систем', '<p>Логотипы платежных систем</p>', '1', '1', null, null, '2');

-- ----------------------------
-- Table structure for `yupe_gallery_image_to_gallery`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_gallery_image_to_gallery`;
CREATE TABLE `yupe_gallery_image_to_gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image_id` int(11) NOT NULL,
  `gallery_id` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `position` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_gallery_image_to_gallery_gallery_to_image` (`image_id`,`gallery_id`),
  KEY `ix_yupe_gallery_image_to_gallery_gallery_to_image_image` (`image_id`),
  KEY `ix_yupe_gallery_image_to_gallery_gallery_to_image_gallery` (`gallery_id`),
  CONSTRAINT `fk_yupe_gallery_image_to_gallery_gallery_to_image_gallery` FOREIGN KEY (`gallery_id`) REFERENCES `yupe_gallery_gallery` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_gallery_image_to_gallery_gallery_to_image_image` FOREIGN KEY (`image_id`) REFERENCES `yupe_image_image` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_gallery_image_to_gallery
-- ----------------------------
INSERT INTO `yupe_gallery_image_to_gallery` VALUES ('1', '1', '1', '2020-09-01 10:27:41', '1');
INSERT INTO `yupe_gallery_image_to_gallery` VALUES ('2', '2', '1', '2020-09-01 10:28:54', '3');
INSERT INTO `yupe_gallery_image_to_gallery` VALUES ('3', '3', '1', '2020-09-01 10:28:56', '2');
INSERT INTO `yupe_gallery_image_to_gallery` VALUES ('4', '4', '2', '2020-09-01 10:43:41', '4');
INSERT INTO `yupe_gallery_image_to_gallery` VALUES ('5', '5', '2', '2020-09-01 10:43:43', '5');
INSERT INTO `yupe_gallery_image_to_gallery` VALUES ('6', '6', '2', '2020-09-01 10:43:45', '6');

-- ----------------------------
-- Table structure for `yupe_image_image`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_image_image`;
CREATE TABLE `yupe_image_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `description` text,
  `file` varchar(250) NOT NULL,
  `create_time` datetime NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `alt` varchar(250) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `sort` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `ix_yupe_image_image_status` (`status`),
  KEY `ix_yupe_image_image_user` (`user_id`),
  KEY `ix_yupe_image_image_type` (`type`),
  KEY `ix_yupe_image_image_category_id` (`category_id`),
  KEY `fk_yupe_image_image_parent_id` (`parent_id`),
  CONSTRAINT `fk_yupe_image_image_category_id` FOREIGN KEY (`category_id`) REFERENCES `yupe_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_image_image_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `yupe_image_image` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_image_image_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_image_image
-- ----------------------------
INSERT INTO `yupe_image_image` VALUES ('1', null, null, 'instagram.svg', '', '22c7b321fa357066f4e9a279d33c9dde.svg', '2020-09-01 10:27:41', '1', 'instagram.svg', '0', '1', '1');
INSERT INTO `yupe_image_image` VALUES ('2', null, null, 'telegram.svg', '', '8c67b8ee74024413bcd64f1b45dd5860.svg', '2020-09-01 10:28:54', '1', 'telegram.svg', '0', '1', '2');
INSERT INTO `yupe_image_image` VALUES ('3', null, null, 'vk.svg', 'https://vk.com/evrootdelko', 'bf74eea52cf43dcfea2d85fcf9444b6d.svg', '2020-09-01 10:28:56', '1', 'vk.svg', '0', '1', '3');
INSERT INTO `yupe_image_image` VALUES ('4', null, null, 'Mastercard', '', '5ea21114173db33efa8047cc4b404c6a.svg', '2020-09-01 10:43:41', '1', 'Mastercard', '0', '1', '4');
INSERT INTO `yupe_image_image` VALUES ('5', null, null, 'VISA', '', '7007933d98598f0c327d591d1767f2e1.svg', '2020-09-01 10:43:43', '1', 'VISA', '0', '1', '5');
INSERT INTO `yupe_image_image` VALUES ('6', null, null, 'MIR', '', 'ee8ff7a09d3e5ce2535a8509438463ea.svg', '2020-09-01 10:43:45', '1', 'MIR', '0', '1', '6');

-- ----------------------------
-- Table structure for `yupe_mail_mail_event`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_mail_mail_event`;
CREATE TABLE `yupe_mail_mail_event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(150) NOT NULL,
  `name` varchar(150) NOT NULL,
  `description` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_mail_mail_event_code` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_mail_mail_event
-- ----------------------------
INSERT INTO `yupe_mail_mail_event` VALUES ('1', 'zakazat-zvonok', 'Заказать звонок', '');

-- ----------------------------
-- Table structure for `yupe_mail_mail_order`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_mail_mail_order`;
CREATE TABLE `yupe_mail_mail_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `theme` varchar(255) DEFAULT NULL COMMENT 'Тема',
  `name` varchar(255) DEFAULT NULL COMMENT 'Имя',
  `phone` varchar(255) DEFAULT NULL COMMENT 'Телефон',
  `email` varchar(255) DEFAULT NULL COMMENT 'E-mail',
  `comment` text COMMENT 'Сообщение',
  `data` text COMMENT 'Данные',
  `group` varchar(255) DEFAULT NULL COMMENT 'Группа',
  `status` int(11) DEFAULT NULL COMMENT 'Статус',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_mail_mail_order
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_mail_mail_template`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_mail_mail_template`;
CREATE TABLE `yupe_mail_mail_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(150) NOT NULL,
  `event_id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `description` text,
  `from` varchar(150) NOT NULL,
  `to` varchar(150) NOT NULL,
  `theme` text NOT NULL,
  `body` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_mail_mail_template_code` (`code`),
  KEY `ix_yupe_mail_mail_template_status` (`status`),
  KEY `ix_yupe_mail_mail_template_event_id` (`event_id`),
  CONSTRAINT `fk_yupe_mail_mail_template_event_id` FOREIGN KEY (`event_id`) REFERENCES `yupe_mail_mail_event` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_mail_mail_template
-- ----------------------------
INSERT INTO `yupe_mail_mail_template` VALUES ('1', 'zakazat-zvonok', '1', 'Заказать звонок', '', 'no-reply@dcmr.ru', 'nariman-abenov@mail.ru', 'Заявка theme с сайта Сантехника174', '<table style=\"border-collapse: collapse; width: 100%;\" border=\"1\">\r\n<tbody>\r\n<tr>\r\n<td style=\"width: 49.0026%;\">Имя:</td>\r\n<td style=\"width: 49.0701%;\">name</td>\r\n</tr>\r\n<tr>\r\n<td style=\"width: 49.0026%;\">Телефон:</td>\r\n<td style=\"width: 49.0701%;\">phone</td>\r\n</tr>\r\n</tbody>\r\n</table>', '1');

-- ----------------------------
-- Table structure for `yupe_menu_menu`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_menu_menu`;
CREATE TABLE `yupe_menu_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_menu_menu_code` (`code`),
  KEY `ix_yupe_menu_menu_status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_menu_menu
-- ----------------------------
INSERT INTO `yupe_menu_menu` VALUES ('1', 'Верхнее меню', 'top-menu', 'Основное меню сайта, расположенное сверху в блоке mainmenu.', '1');
INSERT INTO `yupe_menu_menu` VALUES ('2', 'Нижнее меню', 'nizhnee-menyu', 'Нижнее меню', '1');

-- ----------------------------
-- Table structure for `yupe_menu_menu_item`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_menu_menu_item`;
CREATE TABLE `yupe_menu_menu_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `regular_link` tinyint(1) NOT NULL DEFAULT '0',
  `title` varchar(150) NOT NULL,
  `href` varchar(150) NOT NULL,
  `class` varchar(150) DEFAULT NULL,
  `title_attr` varchar(150) DEFAULT NULL,
  `before_link` varchar(150) DEFAULT NULL,
  `after_link` varchar(150) DEFAULT NULL,
  `target` varchar(150) DEFAULT NULL,
  `rel` varchar(150) DEFAULT NULL,
  `condition_name` varchar(150) DEFAULT '0',
  `condition_denial` int(11) DEFAULT '0',
  `sort` int(11) NOT NULL DEFAULT '1',
  `status` int(11) NOT NULL DEFAULT '1',
  `entity_module_name` varchar(40) DEFAULT NULL,
  `entity_name` varchar(40) DEFAULT NULL,
  `entity_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_yupe_menu_menu_item_menu_id` (`menu_id`),
  KEY `ix_yupe_menu_menu_item_sort` (`sort`),
  KEY `ix_yupe_menu_menu_item_status` (`status`),
  CONSTRAINT `fk_yupe_menu_menu_item_menu_id` FOREIGN KEY (`menu_id`) REFERENCES `yupe_menu_menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_menu_menu_item
-- ----------------------------
INSERT INTO `yupe_menu_menu_item` VALUES ('12', '0', '1', '1', 'Доставка', '/delivery', null, null, null, null, null, null, '0', '0', '3', '1', null, null, null);
INSERT INTO `yupe_menu_menu_item` VALUES ('13', '0', '1', '1', 'Оплата', '/payments', null, null, null, null, null, null, '0', '0', '4', '1', null, null, null);
INSERT INTO `yupe_menu_menu_item` VALUES ('14', '0', '1', '1', 'О компании', '/company', null, null, null, null, null, null, '0', '0', '5', '1', null, null, null);
INSERT INTO `yupe_menu_menu_item` VALUES ('15', '0', '1', '1', 'Блог', '/news', null, null, null, null, null, null, '0', '0', '6', '1', null, null, null);
INSERT INTO `yupe_menu_menu_item` VALUES ('16', '0', '1', '1', 'Контакты', '/contacts', null, null, null, null, null, null, '0', '0', '7', '1', null, null, null);
INSERT INTO `yupe_menu_menu_item` VALUES ('17', '0', '2', '1', 'Акции и скидки', '#', '', '', '', '', '', '', '', '0', '8', '1', '', '', null);
INSERT INTO `yupe_menu_menu_item` VALUES ('18', '0', '2', '1', 'Доставка', '/delivery', '', '', '', '', '', '', '', '0', '9', '1', '', '', null);
INSERT INTO `yupe_menu_menu_item` VALUES ('19', '0', '2', '1', 'Оплата', '/payments', '', '', '', '', '', '', '', '0', '10', '1', '', '', null);
INSERT INTO `yupe_menu_menu_item` VALUES ('20', '0', '2', '1', 'Обмен и возврат', '/obmen-i-vozvrat', '', '', '', '', '', '', '', '0', '11', '1', '', '', null);
INSERT INTO `yupe_menu_menu_item` VALUES ('21', '0', '2', '1', 'О компании', '/company', '', '', '', '', '', '', '', '0', '12', '1', '', '', null);
INSERT INTO `yupe_menu_menu_item` VALUES ('23', '0', '2', '1', 'Контакты', '/contacts', '', '', '', '', '', '', '', '0', '14', '1', '', '', null);
INSERT INTO `yupe_menu_menu_item` VALUES ('24', '0', '2', '1', 'Личный кабинет', '/store/account', '', '', '', '', '', '', '', '0', '15', '1', '', '', null);
INSERT INTO `yupe_menu_menu_item` VALUES ('25', '0', '1', '1', 'Каталог', '#', 'header-hidden', '', '', '', '', '', '', '0', '1', '1', '', '', null);
INSERT INTO `yupe_menu_menu_item` VALUES ('26', '25', '1', '1', '{{catalog}}', '#', '', '', '', '', '', '', '', '0', '2', '1', '', '', null);

-- ----------------------------
-- Table structure for `yupe_migrations`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_migrations`;
CREATE TABLE `yupe_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module` varchar(255) NOT NULL,
  `version` varchar(255) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_migrations_module` (`module`)
) ENGINE=InnoDB AUTO_INCREMENT=156 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_migrations
-- ----------------------------
INSERT INTO `yupe_migrations` VALUES ('1', 'user', 'm000000_000000_user_base', '1545824978');
INSERT INTO `yupe_migrations` VALUES ('2', 'user', 'm131019_212911_user_tokens', '1545824978');
INSERT INTO `yupe_migrations` VALUES ('3', 'user', 'm131025_152911_clean_user_table', '1545824979');
INSERT INTO `yupe_migrations` VALUES ('4', 'user', 'm131026_002234_prepare_hash_user_password', '1545824980');
INSERT INTO `yupe_migrations` VALUES ('5', 'user', 'm131106_111552_user_restore_fields', '1545824980');
INSERT INTO `yupe_migrations` VALUES ('6', 'user', 'm131121_190850_modify_tokes_table', '1545824981');
INSERT INTO `yupe_migrations` VALUES ('7', 'user', 'm140812_100348_add_expire_to_token_table', '1545824981');
INSERT INTO `yupe_migrations` VALUES ('8', 'user', 'm150416_113652_rename_fields', '1545824981');
INSERT INTO `yupe_migrations` VALUES ('9', 'user', 'm151006_000000_user_add_phone', '1545824981');
INSERT INTO `yupe_migrations` VALUES ('10', 'yupe', 'm000000_000000_yupe_base', '1545824982');
INSERT INTO `yupe_migrations` VALUES ('11', 'yupe', 'm130527_154455_yupe_change_unique_index', '1545824982');
INSERT INTO `yupe_migrations` VALUES ('12', 'yupe', 'm150416_125517_rename_fields', '1545824983');
INSERT INTO `yupe_migrations` VALUES ('13', 'yupe', 'm160204_195213_change_settings_type', '1545824983');
INSERT INTO `yupe_migrations` VALUES ('14', 'category', 'm000000_000000_category_base', '1545824984');
INSERT INTO `yupe_migrations` VALUES ('15', 'category', 'm150415_150436_rename_fields', '1545824984');
INSERT INTO `yupe_migrations` VALUES ('16', 'image', 'm000000_000000_image_base', '1545824986');
INSERT INTO `yupe_migrations` VALUES ('17', 'image', 'm150226_121100_image_order', '1545824986');
INSERT INTO `yupe_migrations` VALUES ('18', 'image', 'm150416_080008_rename_fields', '1545824986');
INSERT INTO `yupe_migrations` VALUES ('19', 'page', 'm000000_000000_page_base', '1545824988');
INSERT INTO `yupe_migrations` VALUES ('20', 'page', 'm130115_155600_columns_rename', '1545824988');
INSERT INTO `yupe_migrations` VALUES ('21', 'page', 'm140115_083618_add_layout', '1545824988');
INSERT INTO `yupe_migrations` VALUES ('22', 'page', 'm140620_072543_add_view', '1545824989');
INSERT INTO `yupe_migrations` VALUES ('23', 'page', 'm150312_151049_change_body_type', '1545824989');
INSERT INTO `yupe_migrations` VALUES ('24', 'page', 'm150416_101038_rename_fields', '1545824989');
INSERT INTO `yupe_migrations` VALUES ('25', 'page', 'm180224_105407_meta_title_column', '1545824989');
INSERT INTO `yupe_migrations` VALUES ('26', 'page', 'm180421_143324_update_page_meta_column', '1545824989');
INSERT INTO `yupe_migrations` VALUES ('27', 'mail', 'm000000_000000_mail_base', '1545824991');
INSERT INTO `yupe_migrations` VALUES ('28', 'comment', 'm000000_000000_comment_base', '1545824992');
INSERT INTO `yupe_migrations` VALUES ('29', 'comment', 'm130704_095200_comment_nestedsets', '1545824994');
INSERT INTO `yupe_migrations` VALUES ('30', 'comment', 'm150415_151804_rename_fields', '1545824994');
INSERT INTO `yupe_migrations` VALUES ('31', 'notify', 'm141031_091039_add_notify_table', '1545824994');
INSERT INTO `yupe_migrations` VALUES ('32', 'blog', 'm000000_000000_blog_base', '1545825001');
INSERT INTO `yupe_migrations` VALUES ('33', 'blog', 'm130503_091124_BlogPostImage', '1545825001');
INSERT INTO `yupe_migrations` VALUES ('34', 'blog', 'm130529_151602_add_post_category', '1545825002');
INSERT INTO `yupe_migrations` VALUES ('35', 'blog', 'm140226_052326_add_community_fields', '1545825003');
INSERT INTO `yupe_migrations` VALUES ('36', 'blog', 'm140714_110238_blog_post_quote_type', '1545825003');
INSERT INTO `yupe_migrations` VALUES ('37', 'blog', 'm150406_094809_blog_post_quote_type', '1545825004');
INSERT INTO `yupe_migrations` VALUES ('38', 'blog', 'm150414_180119_rename_date_fields', '1545825004');
INSERT INTO `yupe_migrations` VALUES ('39', 'blog', 'm160518_175903_alter_blog_foreign_keys', '1545825005');
INSERT INTO `yupe_migrations` VALUES ('40', 'blog', 'm180421_143937_update_blog_meta_column', '1545825005');
INSERT INTO `yupe_migrations` VALUES ('41', 'blog', 'm180421_143938_add_post_meta_title_column', '1545825006');
INSERT INTO `yupe_migrations` VALUES ('42', 'contentblock', 'm000000_000000_contentblock_base', '1545825007');
INSERT INTO `yupe_migrations` VALUES ('43', 'contentblock', 'm140715_130737_add_category_id', '1545825007');
INSERT INTO `yupe_migrations` VALUES ('44', 'contentblock', 'm150127_130425_add_status_column', '1545825007');
INSERT INTO `yupe_migrations` VALUES ('45', 'menu', 'm000000_000000_menu_base', '1545825009');
INSERT INTO `yupe_migrations` VALUES ('46', 'menu', 'm121220_001126_menu_test_data', '1545825009');
INSERT INTO `yupe_migrations` VALUES ('47', 'menu', 'm160914_134555_fix_menu_item_default_values', '1545825010');
INSERT INTO `yupe_migrations` VALUES ('48', 'menu', 'm181214_110527_menu_item_add_entity_fields', '1545825010');
INSERT INTO `yupe_migrations` VALUES ('49', 'gallery', 'm000000_000000_gallery_base', '1545825012');
INSERT INTO `yupe_migrations` VALUES ('50', 'gallery', 'm130427_120500_gallery_creation_user', '1545825013');
INSERT INTO `yupe_migrations` VALUES ('51', 'gallery', 'm150416_074146_rename_fields', '1545825013');
INSERT INTO `yupe_migrations` VALUES ('52', 'gallery', 'm160514_131314_add_preview_to_gallery', '1545825013');
INSERT INTO `yupe_migrations` VALUES ('53', 'gallery', 'm160515_123559_add_category_to_gallery', '1545825014');
INSERT INTO `yupe_migrations` VALUES ('54', 'gallery', 'm160515_151348_add_position_to_gallery_image', '1545825014');
INSERT INTO `yupe_migrations` VALUES ('55', 'gallery', 'm181224_072816_add_sort_to_gallery', '1545825014');
INSERT INTO `yupe_migrations` VALUES ('56', 'news', 'm000000_000000_news_base', '1545825016');
INSERT INTO `yupe_migrations` VALUES ('57', 'news', 'm150416_081251_rename_fields', '1545825016');
INSERT INTO `yupe_migrations` VALUES ('58', 'news', 'm180224_105353_meta_title_column', '1545825016');
INSERT INTO `yupe_migrations` VALUES ('59', 'news', 'm180421_142416_update_news_meta_column', '1545825016');
INSERT INTO `yupe_migrations` VALUES ('60', 'page', 'm180421_143325_add_column_image', '1547437100');
INSERT INTO `yupe_migrations` VALUES ('61', 'rbac', 'm140115_131455_auth_item', '1547444421');
INSERT INTO `yupe_migrations` VALUES ('62', 'rbac', 'm140115_132045_auth_item_child', '1547444422');
INSERT INTO `yupe_migrations` VALUES ('63', 'rbac', 'm140115_132319_auth_item_assign', '1547444423');
INSERT INTO `yupe_migrations` VALUES ('64', 'rbac', 'm140702_230000_initial_role_data', '1547444423');
INSERT INTO `yupe_migrations` VALUES ('65', 'page', 'm180421_143326_add_column_body_Short', '1574077121');
INSERT INTO `yupe_migrations` VALUES ('66', 'page', 'm180421_143328_add_page_image_tbl', '1574077121');
INSERT INTO `yupe_migrations` VALUES ('67', 'page', 'm180421_143329_add_page_title_page_h1', '1574077122');
INSERT INTO `yupe_migrations` VALUES ('68', 'page', 'm180421_143330_directions_add_column_data', '1590064486');
INSERT INTO `yupe_migrations` VALUES ('70', 'store', 'm140812_160000_store_attribute_group_base', '1598424697');
INSERT INTO `yupe_migrations` VALUES ('71', 'store', 'm140812_170000_store_attribute_base', '1598424699');
INSERT INTO `yupe_migrations` VALUES ('72', 'store', 'm140812_180000_store_attribute_option_base', '1598424701');
INSERT INTO `yupe_migrations` VALUES ('73', 'store', 'm140813_200000_store_category_base', '1598424702');
INSERT INTO `yupe_migrations` VALUES ('74', 'store', 'm140813_210000_store_type_base', '1598424703');
INSERT INTO `yupe_migrations` VALUES ('75', 'store', 'm140813_220000_store_type_attribute_base', '1598424705');
INSERT INTO `yupe_migrations` VALUES ('76', 'store', 'm140813_230000_store_producer_base', '1598424705');
INSERT INTO `yupe_migrations` VALUES ('77', 'store', 'm140814_000000_store_product_base', '1598424709');
INSERT INTO `yupe_migrations` VALUES ('78', 'store', 'm140814_000010_store_product_category_base', '1598424711');
INSERT INTO `yupe_migrations` VALUES ('79', 'store', 'm140814_000013_store_product_attribute_eav_base', '1598424713');
INSERT INTO `yupe_migrations` VALUES ('80', 'store', 'm140814_000018_store_product_image_base', '1598424714');
INSERT INTO `yupe_migrations` VALUES ('81', 'store', 'm140814_000020_store_product_variant_base', '1598424717');
INSERT INTO `yupe_migrations` VALUES ('82', 'store', 'm141014_210000_store_product_category_column', '1598424719');
INSERT INTO `yupe_migrations` VALUES ('83', 'store', 'm141015_170000_store_product_image_column', '1598424720');
INSERT INTO `yupe_migrations` VALUES ('84', 'store', 'm141218_091834_default_null', '1598424720');
INSERT INTO `yupe_migrations` VALUES ('85', 'store', 'm150210_063409_add_store_menu_item', '1598424720');
INSERT INTO `yupe_migrations` VALUES ('86', 'store', 'm150210_105811_add_price_column', '1598424722');
INSERT INTO `yupe_migrations` VALUES ('87', 'store', 'm150210_131238_order_category', '1598424723');
INSERT INTO `yupe_migrations` VALUES ('88', 'store', 'm150211_105453_add_position_for_product_variant', '1598424723');
INSERT INTO `yupe_migrations` VALUES ('89', 'store', 'm150226_065935_add_product_position', '1598424724');
INSERT INTO `yupe_migrations` VALUES ('90', 'store', 'm150416_112008_rename_fields', '1598424724');
INSERT INTO `yupe_migrations` VALUES ('91', 'store', 'm150417_180000_store_product_link_base', '1598424728');
INSERT INTO `yupe_migrations` VALUES ('92', 'store', 'm150825_184407_change_store_url', '1598424728');
INSERT INTO `yupe_migrations` VALUES ('93', 'store', 'm150907_084604_new_attributes', '1598424731');
INSERT INTO `yupe_migrations` VALUES ('94', 'store', 'm151218_081635_add_external_id_fields', '1598424732');
INSERT INTO `yupe_migrations` VALUES ('95', 'store', 'm151218_082939_add_external_id_ix', '1598424732');
INSERT INTO `yupe_migrations` VALUES ('96', 'store', 'm151218_142113_add_product_index', '1598424733');
INSERT INTO `yupe_migrations` VALUES ('97', 'store', 'm151223_140722_drop_product_type_categories', '1598424734');
INSERT INTO `yupe_migrations` VALUES ('98', 'store', 'm160210_084850_add_h1_and_canonical', '1598424736');
INSERT INTO `yupe_migrations` VALUES ('99', 'store', 'm160210_131541_add_main_image_alt_title', '1598424739');
INSERT INTO `yupe_migrations` VALUES ('100', 'store', 'm160211_180200_add_additional_images_alt_title', '1598424739');
INSERT INTO `yupe_migrations` VALUES ('101', 'store', 'm160215_110749_add_image_groups_table', '1598424741');
INSERT INTO `yupe_migrations` VALUES ('102', 'store', 'm160227_114934_rename_producer_order_column', '1598424741');
INSERT INTO `yupe_migrations` VALUES ('103', 'store', 'm160309_091039_add_attributes_sort_and_search_fields', '1598424742');
INSERT INTO `yupe_migrations` VALUES ('104', 'store', 'm160413_184551_add_type_attr_fk', '1598424742');
INSERT INTO `yupe_migrations` VALUES ('105', 'store', 'm160602_091243_add_position_product_index', '1598424743');
INSERT INTO `yupe_migrations` VALUES ('106', 'store', 'm160602_091909_add_producer_sort_index', '1598424743');
INSERT INTO `yupe_migrations` VALUES ('107', 'store', 'm160713_105449_remove_irrelevant_product_status', '1598424743');
INSERT INTO `yupe_migrations` VALUES ('108', 'store', 'm160805_070905_add_attribute_description', '1598424744');
INSERT INTO `yupe_migrations` VALUES ('109', 'store', 'm161015_121915_change_product_external_id_type', '1598424746');
INSERT INTO `yupe_migrations` VALUES ('110', 'store', 'm161122_090922_add_sort_product_position', '1598424746');
INSERT INTO `yupe_migrations` VALUES ('111', 'store', 'm161122_093736_add_store_layouts', '1598424748');
INSERT INTO `yupe_migrations` VALUES ('112', 'store', 'm181218_121815_store_product_variant_quantity_column', '1598424749');
INSERT INTO `yupe_migrations` VALUES ('113', 'store', 'm181218_121816_store_category_name_short_icon', '1598424751');
INSERT INTO `yupe_migrations` VALUES ('114', 'store', 'm181218_121821_store_category_add_table_badge', '1598424753');
INSERT INTO `yupe_migrations` VALUES ('117', 'payment', 'm140815_170000_store_payment_base', '1598424851');
INSERT INTO `yupe_migrations` VALUES ('119', 'store', 'm181218_121822_store_product_price_result', '1598425754');
INSERT INTO `yupe_migrations` VALUES ('120', 'store', 'm181218_121822_store_product_price_result', '1598425754');
INSERT INTO `yupe_migrations` VALUES ('121', 'store', 'm181218_121822_store_product_price_result', '1598425754');
INSERT INTO `yupe_migrations` VALUES ('122', 'delivery', 'm140815_190000_store_delivery_base', '1598425775');
INSERT INTO `yupe_migrations` VALUES ('123', 'delivery', 'm140815_200000_store_delivery_payment_base', '1598425776');
INSERT INTO `yupe_migrations` VALUES ('124', 'order', 'm140814_200000_store_order_base', '1598425783');
INSERT INTO `yupe_migrations` VALUES ('125', 'order', 'm150324_105949_order_status_table', '1598425786');
INSERT INTO `yupe_migrations` VALUES ('126', 'order', 'm150416_100212_rename_fields', '1598425786');
INSERT INTO `yupe_migrations` VALUES ('127', 'order', 'm150514_065554_change_order_price', '1598425786');
INSERT INTO `yupe_migrations` VALUES ('128', 'order', 'm151209_185124_split_address', '1598425789');
INSERT INTO `yupe_migrations` VALUES ('129', 'order', 'm151211_115447_add_appartment_field', '1598425789');
INSERT INTO `yupe_migrations` VALUES ('130', 'order', 'm160415_055344_add_manager_to_order', '1598425791');
INSERT INTO `yupe_migrations` VALUES ('131', 'order', 'm160618_145025_add_status_color', '1598425791');
INSERT INTO `yupe_migrations` VALUES ('132', 'coupon', 'm140816_200000_store_coupon_base', '1598425795');
INSERT INTO `yupe_migrations` VALUES ('133', 'coupon', 'm150414_124659_add_order_coupon_table', '1598425798');
INSERT INTO `yupe_migrations` VALUES ('134', 'coupon', 'm150415_153218_rename_fields', '1598425798');
INSERT INTO `yupe_migrations` VALUES ('135', 'slider', 'm000000_000000_slider_base', '1598428163');
INSERT INTO `yupe_migrations` VALUES ('136', 'slider', 'm000001_000001_slider_add_column', '1598428163');
INSERT INTO `yupe_migrations` VALUES ('137', 'favorite', 'm200703_105449_add_table_favorites', '1598429352');
INSERT INTO `yupe_migrations` VALUES ('138', 'store', 'm181218_121829_store_add_table_producer_city', '1598445692');
INSERT INTO `yupe_migrations` VALUES ('139', 'store', 'm181218_121830_store_producer_add_column_is_home', '1598446115');
INSERT INTO `yupe_migrations` VALUES ('140', 'news', 'm180421_142417_add_table_category', '1598593519');
INSERT INTO `yupe_migrations` VALUES ('145', 'news', 'm180421_142418_add_foreigkey', '1598868206');
INSERT INTO `yupe_migrations` VALUES ('146', 'news', 'm180421_142418_add_foreigkey', '1598868206');
INSERT INTO `yupe_migrations` VALUES ('147', 'news', 'm180421_142419_update_seo_meta_column', '1598868206');
INSERT INTO `yupe_migrations` VALUES ('148', 'store', 'm181218_121819_store_product_add_column_is_visible', '1600428840');
INSERT INTO `yupe_migrations` VALUES ('149', 'slider', 'm000001_000002_slider_add_table_category', '1600665520');
INSERT INTO `yupe_migrations` VALUES ('150', 'delivery', 'm200831_093440_add_module_column_to_delivery_table', '1601986379');
INSERT INTO `yupe_migrations` VALUES ('151', 'delivery', 'm200831_123501_add_settings_column_to_store_delivery_table', '1601986379');
INSERT INTO `yupe_migrations` VALUES ('152', 'order', 'm201001_053645_add_dadata_fields', '1601986389');
INSERT INTO `yupe_migrations` VALUES ('153', 'pickup', 'm200802_185033_create_pickup_table', '1601986413');
INSERT INTO `yupe_migrations` VALUES ('154', 'order', 'm200801_124738_add_region_columnt_to_store_order_table', '1602045264');
INSERT INTO `yupe_migrations` VALUES ('155', 'mail', 'm000000_000001_mail_order_base', '1608619105');

-- ----------------------------
-- Table structure for `yupe_news_category`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_news_category`;
CREATE TABLE `yupe_news_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_user_id` int(11) NOT NULL,
  `update_user_id` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `name_short` varchar(255) DEFAULT NULL COMMENT 'Короткое Название',
  `name` varchar(255) DEFAULT NULL COMMENT 'Название',
  `slug` varchar(255) DEFAULT NULL COMMENT 'Slug',
  `description` text COMMENT 'Описание',
  `icon` varchar(255) DEFAULT NULL COMMENT 'Иконка',
  `image` varchar(255) DEFAULT NULL COMMENT 'Изображение',
  `meta_title` varchar(250) DEFAULT NULL COMMENT 'Title (SEO)',
  `meta_keywords` text COMMENT 'Ключевые слова (SEO)',
  `meta_description` text COMMENT 'Описание (SEO)',
  `status` int(11) DEFAULT NULL COMMENT 'Статус',
  `position` int(11) DEFAULT NULL COMMENT 'Сортировка',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_news_category
-- ----------------------------
INSERT INTO `yupe_news_category` VALUES ('1', '1', '1', '2020-08-31 15:05:58', '2020-08-31 15:12:30', 'Новости и интересные материалы', 'Новости', 'novosti', '', null, null, '', '', '', '1', '1');

-- ----------------------------
-- Table structure for `yupe_news_news`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_news_news`;
CREATE TABLE `yupe_news_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `lang` char(2) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `date` date NOT NULL,
  `title` varchar(250) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `short_text` text,
  `full_text` text NOT NULL,
  `image` varchar(300) DEFAULT NULL,
  `link` varchar(300) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `is_protected` tinyint(1) NOT NULL DEFAULT '0',
  `meta_keywords` varchar(250) NOT NULL,
  `meta_description` varchar(250) NOT NULL,
  `meta_title` varchar(250) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_news_news_alias_lang` (`slug`,`lang`),
  KEY `ix_yupe_news_news_status` (`status`),
  KEY `ix_yupe_news_news_user_id` (`user_id`),
  KEY `ix_yupe_news_news_category_id` (`category_id`),
  KEY `ix_yupe_news_news_date` (`date`),
  CONSTRAINT `fk_yupe_news_news_category_id` FOREIGN KEY (`category_id`) REFERENCES `yupe_news_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_yupe_news_news_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_news_news
-- ----------------------------
INSERT INTO `yupe_news_news` VALUES ('1', '1', 'ru', '2020-08-28 10:19:55', '2020-08-28 10:19:55', '2020-01-29', 'Ассиметричная ванна', 'assimetrichnaya-vanna', '<p>Дополнительное описание новости, несколько строчек текста ограниченное по количеству символов...</p>', '<p>Раздел в стадии наполнения!</p>', 'e1b5c928026d299947b954268cc1732b.jpg', '', '1', '1', '0', '', '', '');
INSERT INTO `yupe_news_news` VALUES ('2', '1', 'ru', '2020-08-28 10:20:44', '2020-08-28 10:20:44', '2020-08-01', 'Заголовок новости в одну или две строчки.', 'zagolovok-novosti-v-odnu-ili-dve-strochki', '<p>Дополнительное описание новости, несколько строчек текста ограниченное по количеству символов...</p>', '<p>Раздел в стадии наполнения!</p>', '59e26c5911569c52559a51436129d2a3.jpg', '', '1', '1', '0', '', '', '');
INSERT INTO `yupe_news_news` VALUES ('3', '1', 'ru', '2020-08-28 10:21:56', '2020-08-28 10:21:56', '2020-08-25', 'Заголовок новости в одну или две строчки. Максимально допустимо делать заголовок  в три строчки текста.', 'zagolovok-novosti-v-odnu-ili-dve-strochki-maksimalno-dopustimo-delat-zagolovok-v-tri-strochki-teksta', '<p>Дополнительное описание новости, несколько строчек текста ограниченное по количеству символов...</p>', '<p>Раздел в стадии наполнения!</p>', '85bc16ac486065106e7769b5961627b1.jpg', '', '1', '1', '0', '', '', '');
INSERT INTO `yupe_news_news` VALUES ('4', '1', 'ru', '2020-08-28 10:22:43', '2020-08-31 15:07:21', '2020-08-26', 'Заголовок новости в одну или две строчки.', 'zagolovok-novosti-v-odnu-ili-dve-strochki-2', '<p>Дополнительное описание новости, несколько строчек текста ограниченное по количеству символов...</p>', '<p>Раздел в стадии наполнения!</p>', 'd0d9f949f2c9bcb2c45f403142e2a89d.jpg', '', '1', '1', '0', '', '', '');

-- ----------------------------
-- Table structure for `yupe_notify_settings`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_notify_settings`;
CREATE TABLE `yupe_notify_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `my_post` tinyint(1) NOT NULL DEFAULT '1',
  `my_comment` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `ix_yupe_notify_settings_user_id` (`user_id`),
  CONSTRAINT `fk_yupe_notify_settings_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_notify_settings
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_page_page`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_page_page`;
CREATE TABLE `yupe_page_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `lang` char(2) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `change_user_id` int(11) DEFAULT NULL,
  `title_short` varchar(150) NOT NULL,
  `title` varchar(250) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `body` mediumtext NOT NULL,
  `meta_keywords` varchar(250) NOT NULL,
  `meta_description` varchar(250) NOT NULL,
  `status` int(11) NOT NULL,
  `is_protected` tinyint(1) NOT NULL DEFAULT '0',
  `order` int(11) NOT NULL DEFAULT '0',
  `layout` varchar(250) DEFAULT NULL,
  `view` varchar(250) DEFAULT NULL,
  `meta_title` varchar(250) NOT NULL,
  `image` varchar(250) DEFAULT NULL,
  `icon` varchar(250) DEFAULT NULL,
  `body_short` text,
  `name_h1` varchar(255) DEFAULT NULL,
  `data` longtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_page_page_slug_lang` (`slug`,`lang`),
  KEY `ix_yupe_page_page_status` (`status`),
  KEY `ix_yupe_page_page_is_protected` (`is_protected`),
  KEY `ix_yupe_page_page_user_id` (`user_id`),
  KEY `ix_yupe_page_page_change_user_id` (`change_user_id`),
  KEY `ix_yupe_page_page_menu_order` (`order`),
  KEY `ix_yupe_page_page_category_id` (`category_id`),
  CONSTRAINT `fk_yupe_page_page_category_id` FOREIGN KEY (`category_id`) REFERENCES `yupe_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_page_page_change_user_id` FOREIGN KEY (`change_user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_page_page_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_page_page
-- ----------------------------
INSERT INTO `yupe_page_page` VALUES ('1', null, 'ru', null, '2019-01-14 09:39:32', '2019-01-14 09:39:32', '1', '1', 'Главная', 'Главная', 'glavnaya', '<p>TEST</p>', '', '', '1', '0', '1', '', '', '', null, null, null, null, null);
INSERT INTO `yupe_page_page` VALUES ('2', null, 'ru', null, '2019-01-14 09:57:27', '2020-09-30 17:42:04', '1', '1', 'Доставка', 'Доставка', 'delivery', '<p><strong>Правила оказания услуг доставки и подъема товара.</strong></p>\r\n<ol class=\"delivery-ol\">\r\n<li>Доставка товара производится по будним дням с 9:00 до 21:00, по субботам и воскресеньям с 8:00 до 19:00.</li>\r\n<li>Доставка товара со статусом \"В наличии\" осуществляется на следующий день после размещения заказа покупателем, в случае если заказ размещен до 20:00 в будние дни, в выходные дни до 18:00.</li>\r\n<li>При оформлении заказа после 20:00 доставка осуществляется на второй день после оформления заказа, либо в дату согласованную с покупателем.</li>\r\n<li>Доставка в точное время не предусмотрена.</li>\r\n<li>Доставка товара осуществляется на грузовом автомобиле&nbsp;до входной двери подъезда, офисного здания, калитки частного дома. &nbsp;При этом приемка и проверка товара, а так же подписание передаточных документов осуществляется в данном месте без дальнейшего перемещения товара.</li>\r\n<li>Если въезд до места доставки платный, покупатель компенсирует расходы на проезд, в противном случае заказ будет доставлен до места платного въезда.</li>\r\n<li>Покупатель может заказать услугу подъема товара, указав о необходимости данной услуги при заказе товара. За данную услугу взымается дополнительная плата.(услуга подъема оказывается только в будние дни).</li>\r\n<li>Предоставление услуг доставки и&nbsp;подъема возможно при наличии свободного парковочного места вблизи места адреса доставки (подъезда жилого дома, бизнес-центра и т.д.), а так же при условии, что стоянка в данном месте не противоречит Правилам дорожного движения.</li>\r\n<li>Подъем товара производится при условии беспрепятственного доступа к необходимому месту, свободным проходам, лестничным пролетам, а так же при условии, что дверные проемы подъезда и квартиры позволяют безопасно пронести товар.</li>\r\n<li>Стоимость подъема за этаж суммируется со стоимостью подъёма товара на первый этаж.</li>\r\n<li>Доставка от <strong>450 руб</strong>., сумма подъема зависит от габаритов изделия, <strong>от 50 до 500р</strong> этаж.</li>\r\n</ol>', '', '', '1', '0', '2', '', 'delivery-view', '', null, null, '', '', null);
INSERT INTO `yupe_page_page` VALUES ('3', null, 'ru', null, '2019-01-14 09:57:56', '2020-09-30 17:28:42', '1', '1', 'Оплата', 'Оплата', 'payments', '<p><strong>Наличный расчет</strong><br />Производится при согласии обеих сторон. Оплата производится по факту получения заказа.<br />При условии заказа товара, поставка которого осуществляется \"под заказ\" сроком от одной недели до трех месяцев, с клиентом заключается договор поставки, и вносится предоплата 100% от стоимости товара.</p>\r\n<div>[[w:CustomField|id=3;code=payments-cash;view=payment-list]]</div>\r\n<p>&nbsp;</p>\r\n<p><strong>Безналичный расчет</strong><br />При подтверждении заказа Вам будет выставлен счет по e-mail или по факсу, который необходимо оплатить в течении трех дней и сообщить нам об оплате. Отгрузка заказа осуществляется по факту поступления денежных средств на наш расчетный счет.</p>\r\n<div>[[w:CustomField|id=3;code=payments-invoice;view=payment-list]]</div>\r\n<p>&nbsp;</p>\r\n<p><strong>Банковской картой</strong><br />Оплата в магазинах компании при осуществлении самовывоза</p>\r\n<div>[[w:CustomField|id=3;code=payments-magazine;view=payment-list]]</div>\r\n<p>&nbsp;</p>\r\n<p><strong>Банковской картой на сайте<br /></strong>Оплату на сайте можно произвести в личном кабине после подтверждения заказа менеджером. Для онлайн-оплаты можно использовать банковские карты МИР, Visa, Visa Electron, MasterCard и Maestro. Ввод и обработка конфиденциальных платежных данных производится на стороне процессингового центра. Платежные данные передаются в банк в зашифрованном виде по защищенным каналам. Никто, даже продавец, не может получить введенные клиентом реквизиты банковской карты, что гарантирует полную безопасность его денежных средств и персональных данных.</p>\r\n<div>[[w:CustomField|id=3;code=payments;view=payment-list]]</div>\r\n<p>При необходимости возврата денежных средств требуется предоставить следующее:</p>\r\n<ol>\r\n<li>В случае, если товар отгружен &ndash; письмо на возврат товара (образец предоставляется менеджером)</li>\r\n<li>Реквизиты банковской карты отправителя (Внимание: не номер, а полные реквизиты карты).</li>\r\n</ol>\r\n<p>&nbsp;</p>', '', '', '1', '0', '3', '', '', '', null, null, '', '', 'a:4:{i:1;a:6:{s:4:\"name\";s:49:\"Банковской картой на сайте\";s:4:\"code\";s:8:\"payments\";s:8:\"template\";s:10:\"CodeMirror\";s:5:\"value\";s:0:\"\";s:7:\"gallery\";a:4:{i:0;a:5:{s:5:\"image\";s:24:\"3819e62d13034433ca06.png\";s:8:\"position\";i:1;s:5:\"title\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}i:1;a:5:{s:5:\"image\";s:24:\"43e4c93b140e595a3f33.png\";s:8:\"position\";i:2;s:5:\"title\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}i:2;a:5:{s:5:\"image\";s:24:\"f52db3ad710699c1152f.png\";s:8:\"position\";i:3;s:5:\"title\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}i:3;a:5:{s:5:\"image\";s:24:\"f6f2bf49d0921044ce81.png\";s:8:\"position\";i:4;s:5:\"title\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}}s:5:\"image\";N;}i:2;a:6:{s:4:\"name\";s:53:\"Банковской картой в магазине\";s:4:\"code\";s:17:\"payments-magazine\";s:8:\"template\";s:10:\"CodeMirror\";s:5:\"value\";s:0:\"\";s:7:\"gallery\";a:1:{i:0;a:5:{s:5:\"image\";s:24:\"728e975501136b0001ee.png\";s:8:\"position\";i:1;s:5:\"title\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}}s:5:\"image\";N;}i:3;a:6:{s:4:\"name\";s:35:\"Безналичный расчет\";s:4:\"code\";s:16:\"payments-invoice\";s:8:\"template\";s:10:\"CodeMirror\";s:5:\"value\";s:0:\"\";s:7:\"gallery\";a:1:{i:0;a:5:{s:5:\"image\";s:24:\"d5981f15e95704c06bff.png\";s:8:\"position\";i:1;s:5:\"title\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}}s:5:\"image\";N;}i:4;a:6:{s:4:\"name\";s:29:\"Наличный расчет\";s:4:\"code\";s:13:\"payments-cash\";s:8:\"template\";s:10:\"CodeMirror\";s:5:\"value\";s:0:\"\";s:7:\"gallery\";a:1:{i:0;a:5:{s:5:\"image\";s:24:\"c4b1f1d854e0208a160d.png\";s:8:\"position\";i:1;s:5:\"title\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}}s:5:\"image\";N;}}');
INSERT INTO `yupe_page_page` VALUES ('4', null, 'ru', null, '2019-01-14 09:58:14', '2020-09-30 09:59:10', '1', '1', 'О компании', 'О компании', 'company', '<p>Мы работаем с 2008 года и являемся поставщиком качественной и надежной сантехники, а также аксессуаров к ней. В Интернет-магазине нашей компании Вы можете приобрести все необходимое для оборудования ванной комнаты.</p>\r\n<p><strong>Преимущества покупки в нашем интернет-магазине&nbsp;:</strong></p>\r\n<ul>\r\n<li><strong>Широкий ассортимент сантехники в наличии и под заказ:</strong><br />для ванных комнат: раковины, ванны и душевые кабины, а также аксессуары и комплектующие. Если вы не нашли то, что искали, напишите нам запрос, мы сможем подобрать для вас оптимальный вариант.</li>\r\n<li><strong>Официальный дистрибьютор брендов:</strong><br />Hansgrohe, Roca, Geberit, Jacob Delafon, Kaldewei, Ravak, Laufen, Viega, Jika фаянс, Oras, Aquatek, Santek, Ideal Standart, Duravit, Aquaton, Gustavberg, Hueppe, Keramag, Sanita, Nobili, Keuco, Dyson, Gala, Kermi, Kolo, Sanit.</li>\r\n<li><strong>Качество продукции подтверждено сертификатами.</strong></li>\r\n<li><strong>Лучшие цены</strong>, скидки и акции на продукцию от ведущих производителей Испании, Италии, Финляндии, Германии, Франции, Дании, Швеции, США, России.</li>\r\n<li><strong>Подробные консультации</strong>&nbsp;специалистов. Проконсультируем по любым вопросам<br />по выбору сантехники.</li>\r\n<li><strong>Доставка осуществляется собственной службой доставки</strong><br />и транспортно-экспедиционной компанией &laquo;ПЭК&raquo;. Также доступна опция &ndash; самовывоз. Подробнее уточняйте у менеджеров при оформлении заказа.</li>\r\n<li><strong>Оплата товара</strong>:<br />наличными, безналичный расчет, банковской картой при самовывозе *</li>\r\n</ul>\r\n<p><strong>Если вам необходима помощь, напишите нам:</strong><br /><a href=\"mailto:santexnika174@yandex.ru\"><strong>santexnika</strong><strong>174@</strong><strong>yandex</strong><strong>.</strong><strong>ru</strong></a><strong>&nbsp;</strong></p>\r\n<p>Также вы можете выбрать продукцию для отопления и водоснабжения: котлы, бойлеры, водонагреватели и другую технику.</p>\r\n<p><br /><strong>Приятных покупок!</strong></p>', '', '', '1', '0', '4', '', '', '', null, null, '', '', null);
INSERT INTO `yupe_page_page` VALUES ('5', null, 'ru', null, '2019-01-14 09:58:29', '2020-09-30 15:35:03', '1', '1', 'Блог', 'Блог-тест', 'blog-test', '<p>Страница находится в разработке!</p>', '', '', '0', '0', '5', '', '', '', null, null, '', '', null);
INSERT INTO `yupe_page_page` VALUES ('6', null, 'ru', null, '2019-01-14 09:58:44', '2020-09-22 12:46:58', '1', '1', 'Контакты', 'Контакты', 'contacts', '<div>\r\n<div><strong>Адрес:</strong></div>\r\n[[w:ContentMyBlock|id=3]]<br /><br /></div>\r\n<div>\r\n<div><strong>Телефон:</strong></div>\r\n<div>[[w:ContentMyBlock|id=1]]<br /><br /></div>\r\n</div>\r\n<div>\r\n<div><strong>E-mail:</strong></div>\r\n[[w:ContentMyBlock|id=2]]<br /><br /></div>\r\n<div>\r\n<div><strong>График работы:</strong></div>\r\n[[w:ContentMyBlock|id=7]]<br /><br /></div>\r\n<div>\r\n<div><strong>Социальные сети:</strong></div>\r\n[[w:NewGallery|id=1;view=view-contact-soc]]</div>', '', '', '1', '0', '6', '', 'view-contact', '', null, null, '', '', null);
INSERT INTO `yupe_page_page` VALUES ('7', null, 'ru', null, '2019-01-14 09:58:58', '2020-09-30 15:57:01', '1', '1', 'Обмен и возврат', 'Обмен и возврат', 'obmen-i-vozvrat', '<p>Покупатель вправе отказаться от заказанного товара в любое время до его получения, а после получения товара &ndash; в течение 7 (семи) календарных дней с даты получения товара, при условии, если сохранены его товарный вид, потребительские свойства, упаковка, а также документ, подтверждающий факт и условия покупки указанного товара.</p>\r\n<p>При этом Покупатель обязан вернуть товар в пункт выдачи товара Продавца самостоятельно и за свой счет.</p>\r\n<p>Возврат осуществляется в будние дни с 9 до 17:30</p>\r\n<p>Продавец возвращает Покупателю стоимость оплаченного товара, за вычетом стоимости доставки товара, в течение десяти дней со дня предъявления Покупателем соответствующего требования.</p>\r\n<p>Согласно п. 1 ст. 25 Закона РФ &laquo;Защите прав потребителей&raquo; от 07.02.1992 № 2300-1 невозможен возврат товара, бывшего в употреблении.</p>\r\n<p>При отказе Покупателя от заказанного Товара Продавец удерживает из суммы, уплаченной Покупателем за Товар в соответствии с договором, расходы Продавца на доставку от Покупателя возвращённого товара.</p>\r\n<p>Покупатель не вправе возвратить товары надлежащего качества, указанные в <a href=\"/uploads/files/perechen-tov-nevozvrat.pdf\" target=\"_blank\" rel=\"noopener\"><u>Перечне непродовольственных товаров надлежащего качества, не подлежащих возврату или обмену</u></a>, утвержденном Постановлением Правительства РФ от 19.01.1998 № 55.</p>\r\n<p>В том числе:</p>\r\n<ol>\r\n<li>Предметы санитарии и гигиены из металла, резины, текстиля и других материалов, инструменты, приборы и аппаратура медицинские, средства гигиены полости рта, линзы очковые, предметы по уходу за детьми</li>\r\n<li>Предметы личной гигиены (зубные щетки, расчески, заколки, бигуди для волос, парики, шиньоны и другие аналогичные товары)\\</li>\r\n</ol>\r\n<p>Обмен и возврат товара производится на основании Заявления, заполненного и подписанного Покупателем.</p>\r\n<p>При возврате Покупателем товара надлежащего качества составляются накладная или акт о возврате товара, в котором указываются:</p>\r\n<ul>\r\n<li>полное фирменное наименование (наименование) Продавца</li>\r\n<li>фамилия, имя, отчество Покупателя</li>\r\n<li>наименование товара</li>\r\n<li>даты заключения договора и передачи товара</li>\r\n<li>сумма, подлежащая возврату</li>\r\n<li>подписи продавца и покупателя</li>\r\n</ul>\r\n<p>Если необходимо - ознакомьтесь с <a href=\"/uploads/files/zakon-rf-o-zashite-prav-potrebiteley-01-10-2017.pdf\" target=\"_blank\" rel=\"noopener\">законом о защите прав потребителей</a>.</p>', '', '', '1', '0', '7', '', '', '', null, null, '', '', null);

-- ----------------------------
-- Table structure for `yupe_page_page_image`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_page_page_image`;
CREATE TABLE `yupe_page_page_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) DEFAULT NULL,
  `image` varchar(255) NOT NULL COMMENT 'Изображение',
  `title` varchar(255) NOT NULL COMMENT 'Название изображения',
  `alt` varchar(255) NOT NULL COMMENT 'Alt изображения',
  `position` int(11) DEFAULT NULL COMMENT 'Сортировка',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_page_page_image
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_pickup_pickup`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_pickup_pickup`;
CREATE TABLE `yupe_pickup_pickup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `description` text,
  `mode` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `latitude` varchar(255) DEFAULT NULL,
  `longitude` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_pickup_pickup
-- ----------------------------
INSERT INTO `yupe_pickup_pickup` VALUES ('1', 'г Челябинск, ул Доватора, д 27', 'г Челябинск, ул Доватора, д 27', 'Магазин Сантехника для всех', 'Пн–Вс, 09:00–20:00', '+7 (351) 223-45-11', '', '55.1447185', '61.3848513', '1');
INSERT INTO `yupe_pickup_pickup` VALUES ('2', 'г Челябинск, Троицкий тракт, д 21', 'г Челябинск, Троицкий тракт, д 21', 'ЧЕЛСИ номер отдела 28', 'Пн–Вс, 09:00–20:00', '+7 (351) 701-11-45', '', '55.09338', '61.387085', '1');

-- ----------------------------
-- Table structure for `yupe_slider`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_slider`;
CREATE TABLE `yupe_slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT 'Название',
  `name_short` varchar(255) DEFAULT NULL COMMENT 'Короткое Название',
  `image` varchar(255) DEFAULT NULL COMMENT 'Изображение',
  `description` text COMMENT 'Описание',
  `description_short` text COMMENT 'Краткое описание',
  `button_name` varchar(255) DEFAULT NULL COMMENT 'Название кнопки',
  `button_link` varchar(255) DEFAULT NULL COMMENT 'url для кнопки',
  `status` int(11) DEFAULT NULL COMMENT 'Статус',
  `position` int(11) DEFAULT NULL COMMENT 'Сортировка',
  `image_xs` varchar(250) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_yupe_slider_category_id` (`category_id`),
  CONSTRAINT `fk_yupe_slider_category_id` FOREIGN KEY (`category_id`) REFERENCES `yupe_slider_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_slider
-- ----------------------------
INSERT INTO `yupe_slider` VALUES ('1', 'Слайд1', null, '10d4baf673bd263700206e5d938c9265.jpg', null, null, null, null, '1', '1', null, '1');
INSERT INTO `yupe_slider` VALUES ('2', 'Слайд2', null, '85e6eb6beb3518ade6143f3aff6b7f6c.jpg', null, null, null, null, '1', '2', null, '1');
INSERT INTO `yupe_slider` VALUES ('3', 'При заказе на сумму от 30 000 руб доставка осуществляется <strong>бесплатно</strong>!', null, '3affaeecf861dac6ab6b7aee20823b13.png', null, 'Доставка осуществляется <br>в пределаг города', 'Подробнее', '#', '1', '3', null, '2');

-- ----------------------------
-- Table structure for `yupe_slider_category`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_slider_category`;
CREATE TABLE `yupe_slider_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT 'Название',
  `status` int(11) DEFAULT NULL COMMENT 'Статус',
  `position` int(11) DEFAULT NULL COMMENT 'Сортировка',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_slider_category
-- ----------------------------
INSERT INTO `yupe_slider_category` VALUES ('1', 'Основной слайд на главной', '1', '1');
INSERT INTO `yupe_slider_category` VALUES ('2', 'Карусель над шапкой', '1', '2');

-- ----------------------------
-- Table structure for `yupe_store_attribute`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_store_attribute`;
CREATE TABLE `yupe_store_attribute` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `title` varchar(250) DEFAULT NULL,
  `type` tinyint(4) DEFAULT NULL,
  `unit` varchar(30) DEFAULT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `sort` int(11) NOT NULL DEFAULT '0',
  `is_filter` smallint(6) NOT NULL DEFAULT '1',
  `description` text,
  `is_visible` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_store_attribute_name_group` (`name`,`group_id`),
  KEY `ix_yupe_store_attribute_title` (`title`),
  KEY `fk_yupe_store_attribute_group` (`group_id`),
  CONSTRAINT `fk_yupe_store_attribute_group` FOREIGN KEY (`group_id`) REFERENCES `yupe_store_attribute_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_store_attribute
-- ----------------------------
INSERT INTO `yupe_store_attribute` VALUES ('1', null, 'dlina', 'Длина', '4', 'см', '0', '1', '1', '', '0');
INSERT INTO `yupe_store_attribute` VALUES ('2', null, 'shirina', 'Ширина', '4', 'см', '0', '2', '1', '', '0');
INSERT INTO `yupe_store_attribute` VALUES ('3', null, 'obem', 'Объем', '6', 'литров', '0', '3', '1', '', '0');

-- ----------------------------
-- Table structure for `yupe_store_attribute_group`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_store_attribute_group`;
CREATE TABLE `yupe_store_attribute_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `position` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_store_attribute_group
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_store_attribute_option`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_store_attribute_option`;
CREATE TABLE `yupe_store_attribute_option` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_id` int(11) DEFAULT NULL,
  `position` tinyint(4) DEFAULT NULL,
  `value` varchar(255) DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `ix_yupe_store_attribute_option_attribute_id` (`attribute_id`),
  KEY `ix_yupe_store_attribute_option_position` (`position`),
  CONSTRAINT `fk_yupe_store_attribute_option_attribute` FOREIGN KEY (`attribute_id`) REFERENCES `yupe_store_attribute` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_store_attribute_option
-- ----------------------------
INSERT INTO `yupe_store_attribute_option` VALUES ('1', '1', '1', '100');
INSERT INTO `yupe_store_attribute_option` VALUES ('2', '1', '2', '120');
INSERT INTO `yupe_store_attribute_option` VALUES ('3', '1', '3', '130');
INSERT INTO `yupe_store_attribute_option` VALUES ('4', '1', '4', '140');
INSERT INTO `yupe_store_attribute_option` VALUES ('5', '1', '5', '150');
INSERT INTO `yupe_store_attribute_option` VALUES ('6', '1', '6', '155');
INSERT INTO `yupe_store_attribute_option` VALUES ('7', '1', '7', '160');
INSERT INTO `yupe_store_attribute_option` VALUES ('8', '1', '8', '165');
INSERT INTO `yupe_store_attribute_option` VALUES ('9', '1', '9', '167');
INSERT INTO `yupe_store_attribute_option` VALUES ('10', '1', '10', '170');
INSERT INTO `yupe_store_attribute_option` VALUES ('11', '1', '11', '173');
INSERT INTO `yupe_store_attribute_option` VALUES ('12', '1', '12', '174');
INSERT INTO `yupe_store_attribute_option` VALUES ('13', '1', '13', '175');
INSERT INTO `yupe_store_attribute_option` VALUES ('14', '2', '14', '70');
INSERT INTO `yupe_store_attribute_option` VALUES ('15', '2', '15', '75');
INSERT INTO `yupe_store_attribute_option` VALUES ('16', '2', '16', '80');
INSERT INTO `yupe_store_attribute_option` VALUES ('17', '2', '17', '90');
INSERT INTO `yupe_store_attribute_option` VALUES ('18', '2', '18', '100');

-- ----------------------------
-- Table structure for `yupe_store_badge`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_store_badge`;
CREATE TABLE `yupe_store_badge` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT 'Название',
  `color` varchar(255) DEFAULT NULL COMMENT 'Цвет текста',
  `background` text COMMENT 'Фон Badge',
  `status` int(11) DEFAULT NULL COMMENT 'Статус',
  `position` int(11) DEFAULT NULL COMMENT 'Сортировка',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_store_badge
-- ----------------------------
INSERT INTO `yupe_store_badge` VALUES ('1', 'Новинка', '#A500F3', '', '1', '1');
INSERT INTO `yupe_store_badge` VALUES ('2', 'Хит продаж', '#CA0000', '', '1', '2');

-- ----------------------------
-- Table structure for `yupe_store_category`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_store_category`;
CREATE TABLE `yupe_store_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `slug` varchar(150) NOT NULL,
  `name` varchar(250) NOT NULL,
  `image` varchar(250) DEFAULT NULL,
  `short_description` text,
  `description` text,
  `meta_title` varchar(250) DEFAULT NULL,
  `meta_description` varchar(250) DEFAULT NULL,
  `meta_keywords` varchar(250) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `sort` int(11) NOT NULL DEFAULT '1',
  `external_id` varchar(100) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `meta_canonical` varchar(255) DEFAULT NULL,
  `image_alt` varchar(255) DEFAULT NULL,
  `image_title` varchar(255) DEFAULT NULL,
  `view` varchar(100) DEFAULT NULL,
  `name_short` varchar(255) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `is_home` tinyint(1) NOT NULL DEFAULT '0',
  `name_desc` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_store_category_alias` (`slug`),
  KEY `ix_yupe_store_category_parent_id` (`parent_id`),
  KEY `ix_yupe_store_category_status` (`status`),
  KEY `yupe_store_category_external_id_ix` (`external_id`),
  CONSTRAINT `fk_yupe_store_category_parent` FOREIGN KEY (`parent_id`) REFERENCES `yupe_store_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_store_category
-- ----------------------------
INSERT INTO `yupe_store_category` VALUES ('1', null, 'santehnika', 'Сантехника', '3a3ac4f644ee752a22a4ee2b908a20e8.png', '', '', '', '', '', '1', '1', null, '', '', '', '', '', 'Сантехника', null, '1', '');
INSERT INTO `yupe_store_category` VALUES ('2', null, 'dushevaya', 'Душевая', '51d76a1f1e0d0392713b858cd325545c.png', '', '', '', '', '', '1', '2', null, '', '', '', '', '', 'Душевая', null, '1', '');
INSERT INTO `yupe_store_category` VALUES ('3', null, 'sanfayans', 'Санфаянс', 'edd9837df9f66d6436921ab8e81b7656.png', '', '', '', '', '', '1', '3', null, '', '', '', '', '', 'Санфаянс', null, '1', '');
INSERT INTO `yupe_store_category` VALUES ('4', null, 'mebel', 'Мебель', 'cb602a5880106278b1b90b3ef0dfd098.png', '', '', '', '', '', '1', '4', null, '', '', '', '', '', 'Мебель', null, '1', '');
INSERT INTO `yupe_store_category` VALUES ('5', null, 'smesiteli', 'Смесители', '5f96a2dfc2ccffbab58c9284ee1bd959.png', '', '', '', '', '', '1', '5', null, '', '', '', '', '', 'Смесители', null, '1', '');
INSERT INTO `yupe_store_category` VALUES ('6', null, 'polotencesushiteli', 'Полотенцесушители', 'a5ea395b065a10c3ddc4f8952236b4fd.png', '', '', '', '', '', '1', '6', null, '', '', '', '', '', 'Полотенцесушители', null, '1', '');
INSERT INTO `yupe_store_category` VALUES ('7', null, 'inzhenernaya-santehnika', 'Инженерная сантехника', '2437de7a78e95b1a0d35604437e6ca2e.png', '', '', '', '', '', '1', '7', null, '', '', '', '', '', 'Инженерная сантехника', null, '1', '');
INSERT INTO `yupe_store_category` VALUES ('8', null, 'vodonagrevateli', 'Водонагреватели', '74279b1a127904ca4ce2bf89f9eaab42.png', '', '', '', '', '', '1', '8', null, '', '', '', '', '', 'Водонагреватели', null, '1', '');
INSERT INTO `yupe_store_category` VALUES ('9', null, 'radiatory-otopleniya', 'Радиаторы отопления', '8aea98a38b041557f1ea32934b1e37f8.png', '', '', '', '', '', '1', '9', null, '', '', '', '', '', 'Радиаторы отопления', null, '1', '');
INSERT INTO `yupe_store_category` VALUES ('10', null, 'stroitelnye-materialy', 'Строительные материалы', '061f7355fa1447446e091e080c71cfa6.png', '', '', '', '', '', '1', '10', null, '', '', '', '', '', 'Строительные материалы', null, '1', '');
INSERT INTO `yupe_store_category` VALUES ('11', '1', 'akrilovye-vanny', 'Акриловые ванны', null, '', '', '', '', '', '1', '11', null, '', '', '', '', '', 'Акриловые ванны', null, '0', '');
INSERT INTO `yupe_store_category` VALUES ('12', '1', 'stalnye-vanny', 'Стальные ванны', null, '', '', '', '', '', '1', '12', null, '', '', '', '', '', 'Стальные ванны', null, '0', '');
INSERT INTO `yupe_store_category` VALUES ('13', '1', 'shtorki-i-shtangi-dlya-vann', 'Шторки и штанги для ванн', null, '', '', '', '', '', '1', '13', null, '', '', '', '', '', 'Шторки и штанги для ванн', null, '0', '');
INSERT INTO `yupe_store_category` VALUES ('14', '1', 'ekrany', 'Экраны', null, '', '', '', '', '', '1', '14', null, '', '', '', '', '', 'Экраны', null, '0', '');
INSERT INTO `yupe_store_category` VALUES ('15', '1', 'dop-oborudovanie', 'Доп. оборудование', null, '', '', '', '', '', '1', '15', null, '', '', '', '', '', 'Доп. оборудование', null, '0', '');
INSERT INTO `yupe_store_category` VALUES ('16', '1', 'komplektuyushchie', 'Комплектующие', null, '', '', '', '', '', '1', '16', null, '', '', '', '', '', 'Комплектующие', null, '0', '');
INSERT INTO `yupe_store_category` VALUES ('17', '1', 'aksessuary', 'Аксессуары', null, '', '', '', '', '', '1', '17', null, '', '', '', '', '', 'Аксессуары', null, '0', '');
INSERT INTO `yupe_store_category` VALUES ('18', '2', 'dushevye-kabiny', 'Душевые кабины', null, '', '', '', '', '', '1', '18', null, '', '', '', '', '', 'Душевые кабины', null, '0', '');
INSERT INTO `yupe_store_category` VALUES ('19', '2', 'dushevye-ograzhdeniya', 'Душевые ограждения', null, '', '', '', '', '', '1', '19', null, '', '', '', '', '', 'Душевые ограждения', null, '0', '');
INSERT INTO `yupe_store_category` VALUES ('20', '2', 'dushevye-poddony', 'Душевые поддоны', null, '', '', '', '', '', '1', '20', null, '', '', '', '', '', 'Душевые поддоны', null, '0', '');
INSERT INTO `yupe_store_category` VALUES ('21', '2', 'trapy-i-dushevye-lotki', 'Трапы и душевые лотки', null, '', '', '', '', '', '1', '21', null, '', '', '', '', '', 'Трапы и душевые лотки', null, '0', '');
INSERT INTO `yupe_store_category` VALUES ('22', '2', 'sauny', 'Сауны', null, '', '', '', '', '', '1', '22', null, '', '', '', '', '', 'Сауны', null, '0', '');
INSERT INTO `yupe_store_category` VALUES ('23', '2', 'komplektuyushchie-dushevye', 'Комплектующие душевые', null, '', '', '', '', '', '1', '23', null, '', '', '', '', '', 'Комплектующие', null, '0', '');
INSERT INTO `yupe_store_category` VALUES ('24', '2', 'aksessuary-dushevye', 'Аксессуары душевые', null, '', '', '', '', '', '1', '24', null, '', '', '', '', '', 'Аксессуары', null, '0', '');
INSERT INTO `yupe_store_category` VALUES ('25', null, 'akcii', 'Акции', null, '', '', '', '', '', '1', '25', null, '', '', '', '', '', 'Акции', null, '0', '');
INSERT INTO `yupe_store_category` VALUES ('26', '25', 'hit-prodazh', 'Хит продаж', null, '', '', '', '', '', '1', '26', null, '', '', '', '', '', 'Хит продаж', null, '0', '');
INSERT INTO `yupe_store_category` VALUES ('27', '25', 'novoe-postuplenie', 'Новое поступление', null, '', '', '', '', '', '1', '27', null, '', '', '', '', '', 'Новое поступление', null, '0', '');
INSERT INTO `yupe_store_category` VALUES ('28', '25', 'akcii-i-skidki', 'Акции и скидки', null, '', '', '', '', '', '1', '28', null, '', '', '', '', '', 'Акции и скидки', null, '0', '');

-- ----------------------------
-- Table structure for `yupe_store_coupon`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_store_coupon`;
CREATE TABLE `yupe_store_coupon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `code` varchar(255) NOT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '0',
  `value` decimal(10,2) NOT NULL DEFAULT '0.00',
  `min_order_price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `registered_user` tinyint(4) NOT NULL DEFAULT '0',
  `free_shipping` tinyint(4) NOT NULL DEFAULT '0',
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `quantity_per_user` int(11) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_store_coupon
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_store_delivery`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_store_delivery`;
CREATE TABLE `yupe_store_delivery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text,
  `price` float(10,2) NOT NULL DEFAULT '0.00',
  `free_from` float(10,2) DEFAULT NULL,
  `available_from` float(10,2) DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '1',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `separate_payment` tinyint(4) DEFAULT '0',
  `module` varchar(100) DEFAULT NULL,
  `settings` text,
  PRIMARY KEY (`id`),
  KEY `idx_yupe_store_delivery_position` (`position`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_store_delivery
-- ----------------------------
INSERT INTO `yupe_store_delivery` VALUES ('1', 'Самовывоз', '', '0.00', null, null, '1', '1', '0', 'pickup', 'a:0:{}');
INSERT INTO `yupe_store_delivery` VALUES ('2', 'Доставка курьером', '', '400.00', null, null, '2', '1', '0', 'courier', 'a:0:{}');

-- ----------------------------
-- Table structure for `yupe_store_delivery_payment`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_store_delivery_payment`;
CREATE TABLE `yupe_store_delivery_payment` (
  `delivery_id` int(11) NOT NULL,
  `payment_id` int(11) NOT NULL,
  PRIMARY KEY (`delivery_id`,`payment_id`),
  KEY `fk_yupe_store_delivery_payment_payment` (`payment_id`),
  CONSTRAINT `fk_yupe_store_delivery_payment_delivery` FOREIGN KEY (`delivery_id`) REFERENCES `yupe_store_delivery` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_yupe_store_delivery_payment_payment` FOREIGN KEY (`payment_id`) REFERENCES `yupe_store_payment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_store_delivery_payment
-- ----------------------------
INSERT INTO `yupe_store_delivery_payment` VALUES ('2', '1');

-- ----------------------------
-- Table structure for `yupe_store_favorite`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_store_favorite`;
CREATE TABLE `yupe_store_favorite` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `favorite_data` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_yupe_store_favorite_user_id` (`user_id`),
  CONSTRAINT `fk_yupe_store_favorite_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_store_favorite
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_store_order`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_store_order`;
CREATE TABLE `yupe_store_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `delivery_id` int(11) DEFAULT NULL,
  `delivery_price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `payment_method_id` int(11) DEFAULT NULL,
  `paid` tinyint(4) DEFAULT '0',
  `payment_time` datetime DEFAULT NULL,
  `payment_details` text,
  `total_price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `discount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `coupon_discount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `separate_delivery` tinyint(4) DEFAULT '0',
  `status_id` int(11) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `street` varchar(255) NOT NULL DEFAULT '',
  `phone` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `comment` varchar(1024) NOT NULL DEFAULT '',
  `ip` varchar(15) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `note` varchar(1024) NOT NULL DEFAULT '',
  `modified` datetime DEFAULT NULL,
  `zipcode` varchar(30) DEFAULT NULL,
  `country` varchar(150) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `house` varchar(50) DEFAULT NULL,
  `apartment` varchar(10) DEFAULT NULL,
  `manager_id` int(11) DEFAULT NULL,
  `region_obj` text,
  `city_obj` text,
  `street_obj` text,
  `house_obj` text,
  `apartment_obj` text,
  `region` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `udx_yupe_store_order_url` (`url`),
  KEY `idx_yupe_store_order_user_id` (`user_id`),
  KEY `idx_yupe_store_order_date` (`date`),
  KEY `idx_yupe_store_order_status` (`status_id`),
  KEY `idx_yupe_store_order_paid` (`paid`),
  KEY `fk_yupe_store_order_delivery` (`delivery_id`),
  KEY `fk_yupe_store_order_payment` (`payment_method_id`),
  KEY `fk_yupe_store_order_manager` (`manager_id`),
  CONSTRAINT `fk_yupe_store_order_delivery` FOREIGN KEY (`delivery_id`) REFERENCES `yupe_store_delivery` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `fk_yupe_store_order_manager` FOREIGN KEY (`manager_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `fk_yupe_store_order_payment` FOREIGN KEY (`payment_method_id`) REFERENCES `yupe_store_payment` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `fk_yupe_store_order_status` FOREIGN KEY (`status_id`) REFERENCES `yupe_store_order_status` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `fk_yupe_store_order_user` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_store_order
-- ----------------------------
INSERT INTO `yupe_store_order` VALUES ('1', '2', '400.00', '1', '0', null, null, '35760.40', '0.00', '0.00', '0', '1', '2020-12-24 10:58:44', '1', 'admin', 'улица', '+7(123) 456-78-90', 'nariman-abenov@mail.ru', '', '127.0.0.1', '9c77d9f3cce139eecbd95e5557bd6b81', '', '2020-12-24 10:58:44', '', '', 'г Оренбург', '1', '2', null, '{\"postal_code\":\"460000\",\"country\":\"Россия\",\"country_iso_code\":\"RU\",\"federal_district\":\"Приволжский\",\"region_fias_id\":\"8bcec9d6-05bc-4e53-b45c-ba0c6f3a5c44\",\"region_kladr_id\":\"5600000000000\",\"region_iso_code\":\"RU-ORE\",\"region_with_type\":\"Оренбургская обл\",\"region_type\":\"обл\",\"region_type_full\":\"область\",\"region\":\"Оренбургская\",\"area_fias_id\":null,\"area_kladr_id\":null,\"area_with_type\":null,\"area_type\":null,\"area_type_full\":null,\"area\":null,\"city_fias_id\":null,\"city_kladr_id\":null,\"city_with_type\":null,\"city_type\":null,\"city_type_full\":null,\"city\":null,\"city_area\":null,\"city_district_fias_id\":null,\"city_district_kladr_id\":null,\"city_district_with_type\":null,\"city_district_type\":null,\"city_district_type_full\":null,\"city_district\":null,\"settlement_fias_id\":null,\"settlement_kladr_id\":null,\"settlement_with_type\":null,\"settlement_type\":null,\"settlement_type_full\":null,\"settlement\":null,\"street_fias_id\":null,\"street_kladr_id\":null,\"street_with_type\":null,\"street_type\":null,\"street_type_full\":null,\"street\":null,\"house_fias_id\":null,\"house_kladr_id\":null,\"house_type\":null,\"house_type_full\":null,\"house\":null,\"block_type\":null,\"block_type_full\":null,\"block\":null,\"flat_fias_id\":null,\"flat_type\":null,\"flat_type_full\":null,\"flat\":null,\"flat_area\":null,\"square_meter_price\":null,\"flat_price\":null,\"postal_box\":null,\"fias_id\":\"8bcec9d6-05bc-4e53-b45c-ba0c6f3a5c44\",\"fias_code\":\"56000000000000000000000\",\"fias_level\":\"1\",\"fias_actuality_state\":\"0\",\"kladr_id\":\"5600000000000\",\"geoname_id\":\"515001\",\"capital_marker\":\"0\",\"okato\":\"53000000000\",\"oktmo\":\"53000000\",\"tax_office\":\"5600\",\"tax_office_legal\":\"5600\",\"timezone\":null,\"geo_lat\":null,\"geo_lon\":null,\"beltway_hit\":null,\"beltway_distance\":null,\"metro\":null,\"qc_geo\":\"5\",\"qc_complete\":null,\"qc_house\":null,\"history_values\":null,\"unparsed_parts\":null,\"source\":null,\"qc\":null,\"text\":\"Оренбургская обл\",\"id\":\"5600000000000\"}', '{\"postal_code\":null,\"country\":\"Россия\",\"country_iso_code\":\"RU\",\"federal_district\":null,\"region_fias_id\":\"8bcec9d6-05bc-4e53-b45c-ba0c6f3a5c44\",\"region_kladr_id\":\"5600000000000\",\"region_iso_code\":\"RU-ORE\",\"region_with_type\":\"Оренбургская обл\",\"region_type\":\"обл\",\"region_type_full\":\"область\",\"region\":\"Оренбургская\",\"area_fias_id\":null,\"area_kladr_id\":null,\"area_with_type\":null,\"area_type\":null,\"area_type_full\":null,\"area\":null,\"city_fias_id\":\"dce97bff-deb2-4fd9-9aec-4f4327bbf89b\",\"city_kladr_id\":\"5600000100000\",\"city_with_type\":\"г Оренбург\",\"city_type\":\"г\",\"city_type_full\":\"город\",\"city\":\"Оренбург\",\"city_area\":null,\"city_district_fias_id\":null,\"city_district_kladr_id\":null,\"city_district_with_type\":null,\"city_district_type\":null,\"city_district_type_full\":null,\"city_district\":null,\"settlement_fias_id\":null,\"settlement_kladr_id\":null,\"settlement_with_type\":null,\"settlement_type\":null,\"settlement_type_full\":null,\"settlement\":null,\"street_fias_id\":null,\"street_kladr_id\":null,\"street_with_type\":null,\"street_type\":null,\"street_type_full\":null,\"street\":null,\"house_fias_id\":null,\"house_kladr_id\":null,\"house_type\":null,\"house_type_full\":null,\"house\":null,\"block_type\":null,\"block_type_full\":null,\"block\":null,\"flat_fias_id\":null,\"flat_type\":null,\"flat_type_full\":null,\"flat\":null,\"flat_area\":null,\"square_meter_price\":null,\"flat_price\":null,\"postal_box\":null,\"fias_id\":\"dce97bff-deb2-4fd9-9aec-4f4327bbf89b\",\"fias_code\":\"5600000100000000000\",\"fias_level\":\"4\",\"fias_actuality_state\":\"0\",\"kladr_id\":\"5600000100000\",\"geoname_id\":\"515003\",\"capital_marker\":\"2\",\"okato\":\"53401000000\",\"oktmo\":\"53701000\",\"tax_office\":null,\"tax_office_legal\":null,\"timezone\":null,\"geo_lat\":\"51.787535\",\"geo_lon\":\"55.101806\",\"beltway_hit\":null,\"beltway_distance\":null,\"metro\":null,\"qc_geo\":\"4\",\"qc_complete\":null,\"qc_house\":null,\"history_values\":null,\"unparsed_parts\":null,\"source\":null,\"qc\":null,\"text\":\"г Оренбург\",\"id\":\"5600000100000\"}', '', '', '', 'Оренбургская обл');

-- ----------------------------
-- Table structure for `yupe_store_order_coupon`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_store_order_coupon`;
CREATE TABLE `yupe_store_order_coupon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `coupon_id` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_yupe_store_order_coupon_order` (`order_id`),
  KEY `fk_yupe_store_order_coupon_coupon` (`coupon_id`),
  CONSTRAINT `fk_yupe_store_order_coupon_coupon` FOREIGN KEY (`coupon_id`) REFERENCES `yupe_store_coupon` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_yupe_store_order_coupon_order` FOREIGN KEY (`order_id`) REFERENCES `yupe_store_order` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of yupe_store_order_coupon
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_store_order_product`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_store_order_product`;
CREATE TABLE `yupe_store_order_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `product_name` varchar(255) NOT NULL,
  `variants` text,
  `variants_text` varchar(1024) DEFAULT NULL,
  `price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `quantity` int(11) NOT NULL DEFAULT '0',
  `sku` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_yupe_store_order_product_order_id` (`order_id`),
  KEY `idx_yupe_store_order_product_product_id` (`product_id`),
  CONSTRAINT `fk_yupe_store_order_product_order` FOREIGN KEY (`order_id`) REFERENCES `yupe_store_order` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_yupe_store_order_product_product` FOREIGN KEY (`product_id`) REFERENCES `yupe_store_product` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_store_order_product
-- ----------------------------
INSERT INTO `yupe_store_order_product` VALUES ('1', '1', '1', 'Кухонная мойка Mira Sink Light MR-3838 0,6/160 (380х380х160 мм.), врезная', 'a:0:{}', null, '14169.40', '1', 'У10976');
INSERT INTO `yupe_store_order_product` VALUES ('2', '1', '8', 'Ванна Marka One Convey 150x75 [1] [1]', 'a:0:{}', null, '21591.00', '1', 'У10977');

-- ----------------------------
-- Table structure for `yupe_store_order_status`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_store_order_status`;
CREATE TABLE `yupe_store_order_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `is_system` tinyint(1) NOT NULL DEFAULT '0',
  `color` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_store_order_status
-- ----------------------------
INSERT INTO `yupe_store_order_status` VALUES ('1', 'Новый', '1', 'default');
INSERT INTO `yupe_store_order_status` VALUES ('2', 'Принят', '1', 'info');
INSERT INTO `yupe_store_order_status` VALUES ('3', 'Выполнен', '1', 'success');
INSERT INTO `yupe_store_order_status` VALUES ('4', 'Удален', '1', 'danger');

-- ----------------------------
-- Table structure for `yupe_store_payment`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_store_payment`;
CREATE TABLE `yupe_store_payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module` varchar(100) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text,
  `settings` text,
  `currency_id` int(11) DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '1',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_yupe_store_payment_position` (`position`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_store_payment
-- ----------------------------
INSERT INTO `yupe_store_payment` VALUES ('1', 'manual', 'Оплата при получении', '', 'a:0:{}', null, '1', '1');

-- ----------------------------
-- Table structure for `yupe_store_producer`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_store_producer`;
CREATE TABLE `yupe_store_producer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_short` varchar(150) NOT NULL,
  `name` varchar(250) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `image` varchar(250) DEFAULT NULL,
  `short_description` text,
  `description` text,
  `meta_title` varchar(250) DEFAULT NULL,
  `meta_keywords` varchar(250) DEFAULT NULL,
  `meta_description` varchar(250) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `sort` int(11) NOT NULL DEFAULT '0',
  `view` varchar(100) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `is_home` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ix_yupe_store_producer_slug` (`slug`),
  KEY `ix_yupe_store_producer_sort` (`sort`),
  KEY `fk_yupe_store_producer_city` (`city_id`),
  CONSTRAINT `fk_yupe_store_producer_city` FOREIGN KEY (`city_id`) REFERENCES `yupe_store_producer_city` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_store_producer
-- ----------------------------
INSERT INTO `yupe_store_producer` VALUES ('1', 'Grohe', 'Grohe', 'grohe', 'a08e528af9b667dca19ed9b022e84047.jpg', '', '', '', '', '', '1', '1', '', '2', '1');
INSERT INTO `yupe_store_producer` VALUES ('2', 'Aquatek', 'Aquatek', 'aquatek', '325b7bcb31e230afbef39a45d51e8a68.jpg', '', '', '', '', '', '1', '2', '', null, '1');
INSERT INTO `yupe_store_producer` VALUES ('3', 'Sanita', 'Sanita', 'sanita', '83eaf94818257545849a0a98a8028425.jpg', '', '', '', '', '', '1', '3', '', null, '1');
INSERT INTO `yupe_store_producer` VALUES ('4', 'Cersanit', 'Cersanit', 'cersanit', 'fd8f4328403f5146da6fa3ea1713bf2d.jpg', '', '', '', '', '', '1', '4', '', null, '1');
INSERT INTO `yupe_store_producer` VALUES ('5', 'Gappo', 'Gappo', 'gappo', 'bfc037c0b9d9fc1a46a6fcdb3f3d7419.jpg', '', '', '', '', '', '1', '5', '', null, '1');
INSERT INTO `yupe_store_producer` VALUES ('6', 'Santeri', 'Santeri', 'santeri', '3b9753cb87b6bf64c2ff4fe9bcdf243a.jpg', '', '', '', '', '', '1', '6', '', null, '1');
INSERT INTO `yupe_store_producer` VALUES ('7', 'Sensea', 'Sensea', 'sensea', '0177c42fcb9b4a719036d46b9dce81e0.jpg', '', '', '', '', '', '1', '7', '', null, '1');
INSERT INTO `yupe_store_producer` VALUES ('8', 'Jacob delafon', 'Jacob delafon', 'jacob-delafon', '88eb03fd761a59c1c738bd32b9c5c476.jpg', '', '', '', '', '', '1', '8', '', null, '1');
INSERT INTO `yupe_store_producer` VALUES ('9', 'Gappo', 'Gappo', 'gappo-2', '315026245d9e756e0d470bab0986e398.jpg', '', '', '', '', '', '1', '9', '', null, '1');
INSERT INTO `yupe_store_producer` VALUES ('10', 'Marka One', 'Marka One', 'marka-one', 'f9ed3490ef0137ee11f7d2f3964ec92e.jpg', '', '', '', '', '', '1', '10', '', '1', '1');
INSERT INTO `yupe_store_producer` VALUES ('11', '1 МАРКА', '1 МАРКА', '1-marka', '822a7df05cd3215fcacb94c5ecd4682c.jpg', '', '', '', '', '', '1', '11', '', '1', '1');

-- ----------------------------
-- Table structure for `yupe_store_producer_city`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_store_producer_city`;
CREATE TABLE `yupe_store_producer_city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_short` varchar(150) NOT NULL,
  `name` varchar(250) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `image` varchar(250) DEFAULT NULL,
  `description` text,
  `meta_title` varchar(250) DEFAULT NULL,
  `meta_keywords` varchar(250) DEFAULT NULL,
  `meta_description` varchar(250) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_store_producer_city
-- ----------------------------
INSERT INTO `yupe_store_producer_city` VALUES ('1', 'Россия', 'Россия', 'rossiya', null, '', '', '', '', '1', '1');
INSERT INTO `yupe_store_producer_city` VALUES ('2', 'Германия', 'Германия', 'germaniya', null, '', '', '', '', '1', '2');
INSERT INTO `yupe_store_producer_city` VALUES ('3', 'Италия', 'Италия', 'italiya', null, '', '', '', '', '1', '3');

-- ----------------------------
-- Table structure for `yupe_store_product`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_store_product`;
CREATE TABLE `yupe_store_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_id` int(11) DEFAULT NULL,
  `producer_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `sku` varchar(100) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `price` decimal(19,3) NOT NULL DEFAULT '0.000',
  `discount_price` decimal(19,3) DEFAULT NULL,
  `discount` decimal(19,3) DEFAULT NULL,
  `description` text,
  `short_description` text,
  `data` text,
  `is_special` tinyint(1) NOT NULL DEFAULT '0',
  `length` decimal(19,3) DEFAULT NULL,
  `width` decimal(19,3) DEFAULT NULL,
  `height` decimal(19,3) DEFAULT NULL,
  `weight` decimal(19,3) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `in_stock` tinyint(4) NOT NULL DEFAULT '1',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `meta_title` varchar(250) DEFAULT NULL,
  `meta_keywords` varchar(250) DEFAULT NULL,
  `meta_description` varchar(250) DEFAULT NULL,
  `image` varchar(250) DEFAULT NULL,
  `average_price` decimal(19,3) DEFAULT NULL,
  `purchase_price` decimal(19,3) DEFAULT NULL,
  `recommended_price` decimal(19,3) DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '1',
  `external_id` varchar(100) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `meta_canonical` varchar(255) DEFAULT NULL,
  `image_alt` varchar(255) DEFAULT NULL,
  `image_title` varchar(255) DEFAULT NULL,
  `view` varchar(100) DEFAULT NULL,
  `is_home` tinyint(1) NOT NULL DEFAULT '0',
  `price_result` decimal(19,3) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_store_product_alias` (`slug`),
  KEY `ix_yupe_store_product_status` (`status`),
  KEY `ix_yupe_store_product_type_id` (`type_id`),
  KEY `ix_yupe_store_product_producer_id` (`producer_id`),
  KEY `ix_yupe_store_product_price` (`price`),
  KEY `ix_yupe_store_product_discount_price` (`discount_price`),
  KEY `ix_yupe_store_product_create_time` (`create_time`),
  KEY `ix_yupe_store_product_update_time` (`update_time`),
  KEY `fk_yupe_store_product_category` (`category_id`),
  KEY `yupe_store_product_external_id_ix` (`external_id`),
  KEY `ix_yupe_store_product_sku` (`sku`),
  KEY `ix_yupe_store_product_name` (`name`),
  KEY `ix_yupe_store_product_position` (`position`),
  CONSTRAINT `fk_yupe_store_product_category` FOREIGN KEY (`category_id`) REFERENCES `yupe_store_category` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `fk_yupe_store_product_producer` FOREIGN KEY (`producer_id`) REFERENCES `yupe_store_producer` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `fk_yupe_store_product_type` FOREIGN KEY (`type_id`) REFERENCES `yupe_store_type` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_store_product
-- ----------------------------
INSERT INTO `yupe_store_product` VALUES ('1', null, '3', '4', 'У10976', 'Кухонная мойка Mira Sink Light MR-3838 0,6/160 (380х380х160 мм.), врезная', 'kuhonnaya-moyka-mira-sink-light-mr-3838-06160-380h380h160-mm-vreznaya', '20242.000', null, '30.000', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', '', '0', null, null, null, null, '0', '0', '1', '2020-09-01 12:38:33', '2020-10-01 10:36:48', '', '', '', 'b92ae4b3edefc0dbac10ce8bc503ed6e.jpg', null, null, null, '3', null, '', '', '', '', '', '0', '14169.400');
INSERT INTO `yupe_store_product` VALUES ('2', null, '3', '3', '78274', 'Кухонная мойка Ulgran U-104-308 черный', 'kuhonnaya-moyka-ulgran-u-104-308-chernyy', '17490.000', null, null, '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', '', '0', null, null, null, null, '0', '0', '1', '2020-09-01 14:06:54', '2020-09-25 15:05:19', '', '', '', '9429ecd3cb1c9476eb9a7ab09fc54dad.jpg', null, null, null, '4', null, '', '', '', '', '', '0', '17490.000');
INSERT INTO `yupe_store_product` VALUES ('3', null, '5', '5', '86029', 'Однорычажный смеситель для кухни под фильтр с краном для питьевой воды Ledeme L4055-3 хром', 'odnorychazhnyy-smesitel-dlya-kuhni-pod-filtr-s-kranom-dlya-pitevoy-vody-ledeme-l4055-3-hrom', '3599.000', null, '20.000', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', '', '0', null, null, null, null, '0', '0', '1', '2020-09-01 14:08:40', '2020-09-25 15:05:19', '', '', '', 'c7d030011955d62fe3d09644e5869bc4.jpg', null, null, null, '5', null, '', '', '', '', '', '0', '2879.200');
INSERT INTO `yupe_store_product` VALUES ('4', null, '3', '4', 'У10976', 'Кухонная мойка Mira Sink Light MR-3838 0,6/160 (380х380х160 мм.), врезная [1]', 'kuhonnaya-moyka-mira-sink-light-mr-3838-06160-380h380h160-mm-vreznaya-1', '20242.000', null, '30.000', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', '', '0', null, null, null, null, null, '0', '1', '2020-09-01 14:09:30', '2020-09-25 15:05:19', '', '', '', 'dfa7f90cdee9f4eb55f1216dab828996.jpg', null, null, null, '6', null, '', '', '', '', '', '0', '14169.400');
INSERT INTO `yupe_store_product` VALUES ('5', null, '3', '3', '78274', 'Кухонная мойка Ulgran U-104-308 черный [1]', 'kuhonnaya-moyka-ulgran-u-104-308-chernyy-1', '17490.000', null, null, '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', '', '0', null, null, null, null, null, '0', '1', '2020-09-01 14:09:36', '2020-09-25 15:05:19', '', '', '', '5f6cb52a2a0c945ba310d9cd88652329.jpg', null, null, null, '7', null, '', '', '', '', '', '0', '17490.000');
INSERT INTO `yupe_store_product` VALUES ('6', '1', '11', '11', 'У10977', 'Ванна Marka One Convey 150x75', 'vanna-marka-one-convey-150x75', '23990.000', null, '10.000', '<p>Специально для владельцев малогабаритных ванных комнат \"1МарКа\" разработала ванну Piccolo. Размеры и форма ванны рассчитаны так, чтобы раковину можно было разместить над узкой частью ванны Piccolo. При этом она не будет мешать принимать водные процедуры, а изгибы ванны освобождают доступ к раковине. Приобрести акриловую ванну Пикколо можно в одном из фирменных магазинов «Центр Ванн» и «MARKA» по весьма привлекательной цене. Чтобы купить ванну 1Marka PICCOLO 150x75 в городе Оренбург с доставкой или самовывозом достаточно положить товар в корзину или связаться любым удобным способом.</p>', '<p>Специально для владельцев малогабаритных ванных комнат \"1МарКа\" разработала ванну Piccolo. Размеры и форма ванны рассчитаны так, чтобы раковину можно было разместить над узкой частью ванны Piccolo.</p>', '', '0', null, null, null, null, null, '0', '1', '2020-09-01 14:20:19', '2020-10-05 12:51:10', '', '', '', '8cf8c275af05b866501ed0fbbc88b3d2.jpg', null, null, null, '1', null, '', '', '', '', '', '0', '21591.000');
INSERT INTO `yupe_store_product` VALUES ('7', '1', '11', '11', 'У10977', 'Ванна Marka One Convey 160x75', 'vanna-marka-one-convey-160x75', '21990.000', null, '10.000', '<p>Специально для владельцев малогабаритных ванных комнат \"1МарКа\" разработала ванну Piccolo. Размеры и форма ванны рассчитаны так, чтобы раковину можно было разместить над узкой частью ванны Piccolo. При этом она не будет мешать принимать водные процедуры, а изгибы ванны освобождают доступ к раковине. Приобрести акриловую ванну Пикколо можно в одном из фирменных магазинов «Центр Ванн» и «MARKA» по весьма привлекательной цене. Чтобы купить ванну 1Marka PICCOLO 150x75 в городе Оренбург с доставкой или самовывозом достаточно положить товар в корзину или связаться любым удобным способом.</p>', '<p>Специально для владельцев малогабаритных ванных комнат \"1МарКа\" разработала ванну Piccolo. Размеры и форма ванны рассчитаны так, чтобы раковину можно было разместить над узкой частью ванны Piccolo.</p>', '', '0', null, null, null, null, null, '0', '1', '2020-09-01 14:58:24', '2020-10-01 10:36:42', '', '', '', 'd9203928af4776cff8266ba93f40f090.jpg', null, null, null, '2', null, '', '', '', '', '', '0', '19791.000');
INSERT INTO `yupe_store_product` VALUES ('8', null, '11', '11', 'У10977', 'Ванна Marka One Convey 150x75 [1] [1]', 'vanna-marka-one-convey-150x75-1-1', '23990.000', null, '10.000', '<p>Специально для владельцев малогабаритных ванных комнат \"1МарКа\" разработала ванну Piccolo. Размеры и форма ванны рассчитаны так, чтобы раковину можно было разместить над узкой частью ванны Piccolo. При этом она не будет мешать принимать водные процедуры, а изгибы ванны освобождают доступ к раковине. Приобрести акриловую ванну Пикколо можно в одном из фирменных магазинов «Центр Ванн» и «MARKA» по весьма привлекательной цене. Чтобы купить ванну 1Marka PICCOLO 150x75 в городе Оренбург с доставкой или самовывозом достаточно положить товар в корзину или связаться любым удобным способом.</p>', '<p>Специально для владельцев малогабаритных ванных комнат \"1МарКа\" разработала ванну Piccolo. Размеры и форма ванны рассчитаны так, чтобы раковину можно было разместить над узкой частью ванны Piccolo.</p>', '', '0', null, null, null, null, null, '0', '1', '2020-09-01 14:58:31', '2020-09-25 15:05:19', '', '', '', 'eaf4a9bb81ca3de3a9ca7abb16a8770f.jpg', null, null, null, '8', null, '', '', '', '', '', '0', '21591.000');
INSERT INTO `yupe_store_product` VALUES ('9', null, '11', '11', 'У10977', 'Ванна Marka One Convey 150x75 [1] [1] [1]', 'vanna-marka-one-convey-150x75-1-1-1', '23990.000', null, '10.000', '<p>Специально для владельцев малогабаритных ванных комнат \"1МарКа\" разработала ванну Piccolo. Размеры и форма ванны рассчитаны так, чтобы раковину можно было разместить над узкой частью ванны Piccolo. При этом она не будет мешать принимать водные процедуры, а изгибы ванны освобождают доступ к раковине. Приобрести акриловую ванну Пикколо можно в одном из фирменных магазинов «Центр Ванн» и «MARKA» по весьма привлекательной цене. Чтобы купить ванну 1Marka PICCOLO 150x75 в городе Оренбург с доставкой или самовывозом достаточно положить товар в корзину или связаться любым удобным способом.</p>', '<p>Специально для владельцев малогабаритных ванных комнат \"1МарКа\" разработала ванну Piccolo. Размеры и форма ванны рассчитаны так, чтобы раковину можно было разместить над узкой частью ванны Piccolo.</p>', '', '0', null, null, null, null, null, '0', '1', '2020-09-01 14:58:37', '2020-09-25 15:05:19', '', '', '', 'ecac9b935154bed28c9e8db93dec174a.jpg', null, null, null, '9', null, '', '', '', '', '', '0', '21591.000');
INSERT INTO `yupe_store_product` VALUES ('10', '1', '11', '11', 'У10977', 'Ванна Marka One Convey 160x75 [1]', 'vanna-marka-one-convey-160x75-1', '21990.000', null, '10.000', '<p>Специально для владельцев малогабаритных ванных комнат \"1МарКа\" разработала ванну Piccolo. Размеры и форма ванны рассчитаны так, чтобы раковину можно было разместить над узкой частью ванны Piccolo. При этом она не будет мешать принимать водные процедуры, а изгибы ванны освобождают доступ к раковине. Приобрести акриловую ванну Пикколо можно в одном из фирменных магазинов «Центр Ванн» и «MARKA» по весьма привлекательной цене. Чтобы купить ванну 1Marka PICCOLO 150x75 в городе Оренбург с доставкой или самовывозом достаточно положить товар в корзину или связаться любым удобным способом.</p>', '<p>Специально для владельцев малогабаритных ванных комнат \"1МарКа\" разработала ванну Piccolo. Размеры и форма ванны рассчитаны так, чтобы раковину можно было разместить над узкой частью ванны Piccolo.</p>', '', '0', null, null, null, null, null, '0', '1', '2020-09-22 11:19:14', '2020-09-25 15:05:19', '', '', '', '632a7dbea2ddd222fc9b4b56229a4a06.jpg', null, null, null, '10', null, '', '', '', '', '', '0', '19791.000');
INSERT INTO `yupe_store_product` VALUES ('11', null, '11', '11', 'У10977', 'Ванна Marka One Convey 150x75 [1] [1] [2]', 'vanna-marka-one-convey-150x75-1-1-2', '23990.000', null, '10.000', '<p>Специально для владельцев малогабаритных ванных комнат \"1МарКа\" разработала ванну Piccolo. Размеры и форма ванны рассчитаны так, чтобы раковину можно было разместить над узкой частью ванны Piccolo. При этом она не будет мешать принимать водные процедуры, а изгибы ванны освобождают доступ к раковине. Приобрести акриловую ванну Пикколо можно в одном из фирменных магазинов «Центр Ванн» и «MARKA» по весьма привлекательной цене. Чтобы купить ванну 1Marka PICCOLO 150x75 в городе Оренбург с доставкой или самовывозом достаточно положить товар в корзину или связаться любым удобным способом.</p>', '<p>Специально для владельцев малогабаритных ванных комнат \"1МарКа\" разработала ванну Piccolo. Размеры и форма ванны рассчитаны так, чтобы раковину можно было разместить над узкой частью ванны Piccolo.</p>', '', '0', null, null, null, null, null, '0', '1', '2020-09-22 11:19:14', '2020-09-22 11:19:14', '', '', '', 'f57753cf48de9efaa1741f8e58963451.jpg', null, null, null, '11', null, '', '', '', '', '', '0', '21591.000');
INSERT INTO `yupe_store_product` VALUES ('12', null, '11', '11', 'У10977', 'Ванна Marka One Convey 150x75 [1] [1] [1] [1]', 'vanna-marka-one-convey-150x75-1-1-1-1', '23990.000', null, '10.000', '<p>Специально для владельцев малогабаритных ванных комнат \"1МарКа\" разработала ванну Piccolo. Размеры и форма ванны рассчитаны так, чтобы раковину можно было разместить над узкой частью ванны Piccolo. При этом она не будет мешать принимать водные процедуры, а изгибы ванны освобождают доступ к раковине. Приобрести акриловую ванну Пикколо можно в одном из фирменных магазинов «Центр Ванн» и «MARKA» по весьма привлекательной цене. Чтобы купить ванну 1Marka PICCOLO 150x75 в городе Оренбург с доставкой или самовывозом достаточно положить товар в корзину или связаться любым удобным способом.</p>', '<p>Специально для владельцев малогабаритных ванных комнат \"1МарКа\" разработала ванну Piccolo. Размеры и форма ванны рассчитаны так, чтобы раковину можно было разместить над узкой частью ванны Piccolo.</p>', '', '0', null, null, null, null, null, '0', '1', '2020-09-22 11:19:14', '2020-09-22 11:19:14', '', '', '', '097d5a3165032f3280bdc015668a2f49.jpg', null, null, null, '12', null, '', '', '', '', '', '0', '21591.000');

-- ----------------------------
-- Table structure for `yupe_store_product_attribute_value`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_store_product_attribute_value`;
CREATE TABLE `yupe_store_product_attribute_value` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `number_value` double DEFAULT NULL,
  `string_value` varchar(250) DEFAULT NULL,
  `text_value` text,
  `option_value` int(11) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `yupe_fk_product_attribute_product` (`product_id`),
  KEY `yupe_fk_product_attribute_attribute` (`attribute_id`),
  KEY `yupe_fk_product_attribute_option` (`option_value`),
  KEY `yupe_ix_product_attribute_number_value` (`number_value`),
  KEY `yupe_ix_product_attribute_string_value` (`string_value`),
  CONSTRAINT `yupe_fk_product_attribute_attribute` FOREIGN KEY (`attribute_id`) REFERENCES `yupe_store_attribute` (`id`) ON DELETE CASCADE,
  CONSTRAINT `yupe_fk_product_attribute_option` FOREIGN KEY (`option_value`) REFERENCES `yupe_store_attribute_option` (`id`) ON DELETE CASCADE,
  CONSTRAINT `yupe_fk_product_attribute_product` FOREIGN KEY (`product_id`) REFERENCES `yupe_store_product` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_store_product_attribute_value
-- ----------------------------
INSERT INTO `yupe_store_product_attribute_value` VALUES ('7', '6', '3', '110', null, null, null, null);
INSERT INTO `yupe_store_product_attribute_value` VALUES ('10', '10', '1', null, null, null, '7', null);
INSERT INTO `yupe_store_product_attribute_value` VALUES ('12', '7', '3', '100', null, null, null, null);
INSERT INTO `yupe_store_product_attribute_value` VALUES ('17', '7', '1', null, null, null, '7', null);
INSERT INTO `yupe_store_product_attribute_value` VALUES ('22', '6', '1', null, null, null, '5', null);
INSERT INTO `yupe_store_product_attribute_value` VALUES ('23', '6', '2', null, null, null, '15', null);

-- ----------------------------
-- Table structure for `yupe_store_product_badge`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_store_product_badge`;
CREATE TABLE `yupe_store_product_badge` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `badge_id` int(11) DEFAULT NULL COMMENT 'Id Badge',
  `product_id` int(11) DEFAULT NULL COMMENT 'Id продукта',
  PRIMARY KEY (`id`),
  KEY `fk_yupe_store_product_badge_badge_id` (`badge_id`),
  KEY `fk_yupe_store_product_badge_product_id` (`product_id`),
  CONSTRAINT `fk_yupe_store_product_badge_badge_id` FOREIGN KEY (`badge_id`) REFERENCES `yupe_store_badge` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_yupe_store_product_badge_product_id` FOREIGN KEY (`product_id`) REFERENCES `yupe_store_product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_store_product_badge
-- ----------------------------
INSERT INTO `yupe_store_product_badge` VALUES ('33', '1', '2');
INSERT INTO `yupe_store_product_badge` VALUES ('34', '2', '3');
INSERT INTO `yupe_store_product_badge` VALUES ('38', '1', '1');
INSERT INTO `yupe_store_product_badge` VALUES ('41', '2', '6');

-- ----------------------------
-- Table structure for `yupe_store_product_category`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_store_product_category`;
CREATE TABLE `yupe_store_product_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_yupe_store_product_category_product_id` (`product_id`),
  KEY `ix_yupe_store_product_category_category_id` (`category_id`),
  CONSTRAINT `fk_yupe_store_product_category_category` FOREIGN KEY (`category_id`) REFERENCES `yupe_store_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_yupe_store_product_category_product` FOREIGN KEY (`product_id`) REFERENCES `yupe_store_product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_store_product_category
-- ----------------------------
INSERT INTO `yupe_store_product_category` VALUES ('1', '6', '26');
INSERT INTO `yupe_store_product_category` VALUES ('2', '6', '28');
INSERT INTO `yupe_store_product_category` VALUES ('3', '6', '26');
INSERT INTO `yupe_store_product_category` VALUES ('4', '6', '28');
INSERT INTO `yupe_store_product_category` VALUES ('5', '7', '28');
INSERT INTO `yupe_store_product_category` VALUES ('6', '7', '28');
INSERT INTO `yupe_store_product_category` VALUES ('7', '1', '27');
INSERT INTO `yupe_store_product_category` VALUES ('8', '1', '27');

-- ----------------------------
-- Table structure for `yupe_store_product_image`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_store_product_image`;
CREATE TABLE `yupe_store_product_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `title` varchar(250) DEFAULT NULL,
  `alt` varchar(255) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_yupe_store_product_image_product` (`product_id`),
  KEY `fk_yupe_store_product_image_group` (`group_id`),
  CONSTRAINT `fk_yupe_store_product_image_group` FOREIGN KEY (`group_id`) REFERENCES `yupe_store_product_image_group` (`id`) ON DELETE NO ACTION ON UPDATE SET NULL,
  CONSTRAINT `fk_yupe_store_product_image_product` FOREIGN KEY (`product_id`) REFERENCES `yupe_store_product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_store_product_image
-- ----------------------------
INSERT INTO `yupe_store_product_image` VALUES ('1', '1', '87a860ba2d49769215240b3184495ed5.jpg', '', '', null);
INSERT INTO `yupe_store_product_image` VALUES ('2', '1', 'e464a5d0d72e9e2d53063d6243f0426e.jpg', '', '', null);
INSERT INTO `yupe_store_product_image` VALUES ('3', '6', 'fc563a771e8d4726e0b7c6144bddea71.jpg', '', '', null);
INSERT INTO `yupe_store_product_image` VALUES ('4', '6', '46b6d305fe5b04f64232b0eb47a5b0f8.jpg', '', '', null);
INSERT INTO `yupe_store_product_image` VALUES ('5', '6', '82086a5f8f338b5fb37593f8e90e8735.jpg', '', '', null);
INSERT INTO `yupe_store_product_image` VALUES ('6', '6', '0356388ed70be2bfc8af08b6a7e26202.jpg', '', '', null);
INSERT INTO `yupe_store_product_image` VALUES ('7', '6', '55c59e05a50b3aa14a1f7f53c41c3cb1.jpg', '', '', null);

-- ----------------------------
-- Table structure for `yupe_store_product_image_group`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_store_product_image_group`;
CREATE TABLE `yupe_store_product_image_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_store_product_image_group
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_store_product_link`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_store_product_link`;
CREATE TABLE `yupe_store_product_link` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_id` int(11) DEFAULT NULL,
  `product_id` int(11) NOT NULL,
  `linked_product_id` int(11) NOT NULL,
  `position` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_store_product_link_product` (`product_id`,`linked_product_id`),
  KEY `fk_yupe_store_product_link_linked_product` (`linked_product_id`),
  KEY `fk_yupe_store_product_link_type` (`type_id`),
  CONSTRAINT `fk_yupe_store_product_link_linked_product` FOREIGN KEY (`linked_product_id`) REFERENCES `yupe_store_product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_yupe_store_product_link_product` FOREIGN KEY (`product_id`) REFERENCES `yupe_store_product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_yupe_store_product_link_type` FOREIGN KEY (`type_id`) REFERENCES `yupe_store_product_link_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_store_product_link
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_store_product_link_type`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_store_product_link_type`;
CREATE TABLE `yupe_store_product_link_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_store_product_link_type_code` (`code`),
  UNIQUE KEY `ux_yupe_store_product_link_type_title` (`title`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_store_product_link_type
-- ----------------------------
INSERT INTO `yupe_store_product_link_type` VALUES ('1', 'similar', 'Похожие');
INSERT INTO `yupe_store_product_link_type` VALUES ('2', 'related', 'Сопутствующие');

-- ----------------------------
-- Table structure for `yupe_store_product_variant`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_store_product_variant`;
CREATE TABLE `yupe_store_product_variant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `attribute_value` varchar(255) DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `type` tinyint(4) NOT NULL,
  `sku` varchar(50) DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '1',
  `quantity` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_yupe_store_product_variant_product` (`product_id`),
  KEY `idx_yupe_store_product_variant_attribute` (`attribute_id`),
  KEY `idx_yupe_store_product_variant_value` (`attribute_value`),
  CONSTRAINT `fk_yupe_store_product_variant_attribute` FOREIGN KEY (`attribute_id`) REFERENCES `yupe_store_attribute` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_yupe_store_product_variant_product` FOREIGN KEY (`product_id`) REFERENCES `yupe_store_product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_store_product_variant
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_store_type`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_store_type`;
CREATE TABLE `yupe_store_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_store_type_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_store_type
-- ----------------------------
INSERT INTO `yupe_store_type` VALUES ('1', 'Ванны');

-- ----------------------------
-- Table structure for `yupe_store_type_attribute`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_store_type_attribute`;
CREATE TABLE `yupe_store_type_attribute` (
  `type_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  PRIMARY KEY (`type_id`,`attribute_id`),
  KEY `fk_yupe_store_type_attribute_attribute` (`attribute_id`),
  CONSTRAINT `fk_yupe_store_type_attribute_attribute` FOREIGN KEY (`attribute_id`) REFERENCES `yupe_store_attribute` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_yupe_store_type_attribute_type` FOREIGN KEY (`type_id`) REFERENCES `yupe_store_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_store_type_attribute
-- ----------------------------
INSERT INTO `yupe_store_type_attribute` VALUES ('1', '1');
INSERT INTO `yupe_store_type_attribute` VALUES ('1', '2');
INSERT INTO `yupe_store_type_attribute` VALUES ('1', '3');

-- ----------------------------
-- Table structure for `yupe_user_tokens`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_user_tokens`;
CREATE TABLE `yupe_user_tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `token` varchar(255) DEFAULT NULL,
  `type` int(11) NOT NULL,
  `status` int(11) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `expire_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_yupe_user_tokens_user_id` (`user_id`),
  CONSTRAINT `fk_yupe_user_tokens_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_user_tokens
-- ----------------------------
INSERT INTO `yupe_user_tokens` VALUES ('30', '1', 'adP4TPERGHqXAF6micWv4eTSwtrQoGfc', '4', '0', '2021-01-21 10:05:41', '2021-01-21 10:05:41', '127.0.0.1', '2021-01-28 10:05:41');

-- ----------------------------
-- Table structure for `yupe_user_user`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_user_user`;
CREATE TABLE `yupe_user_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `update_time` datetime NOT NULL,
  `first_name` varchar(250) DEFAULT NULL,
  `middle_name` varchar(250) DEFAULT NULL,
  `last_name` varchar(250) DEFAULT NULL,
  `nick_name` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `gender` tinyint(1) NOT NULL DEFAULT '0',
  `birth_date` date DEFAULT NULL,
  `site` varchar(250) NOT NULL DEFAULT '',
  `about` varchar(250) NOT NULL DEFAULT '',
  `location` varchar(250) NOT NULL DEFAULT '',
  `status` int(11) NOT NULL DEFAULT '2',
  `access_level` int(11) NOT NULL DEFAULT '0',
  `visit_time` datetime DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `avatar` varchar(150) DEFAULT NULL,
  `hash` varchar(255) NOT NULL DEFAULT '50443789b000cb912ae2c026e7e117600.90330600 1545824979',
  `email_confirm` tinyint(1) NOT NULL DEFAULT '0',
  `phone` char(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_user_user_nick_name` (`nick_name`),
  UNIQUE KEY `ux_yupe_user_user_email` (`email`),
  KEY `ix_yupe_user_user_status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_user_user
-- ----------------------------
INSERT INTO `yupe_user_user` VALUES ('1', '2018-12-26 17:50:44', '', '', '', 'admin', 'nariman-abenov@mail.ru', '0', null, '', '', '', '1', '1', '2021-01-21 10:05:40', '2018-12-26 17:50:44', null, '$2y$13$n8zKhyL8z0DR9yUE1HmNlu.QUR6GwqIuebNFXyeHzb2pBqF3DcNci', '1', null);

-- ----------------------------
-- Table structure for `yupe_user_user_auth_assignment`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_user_user_auth_assignment`;
CREATE TABLE `yupe_user_user_auth_assignment` (
  `itemname` char(64) NOT NULL,
  `userid` int(11) NOT NULL,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`itemname`,`userid`),
  KEY `fk_yupe_user_user_auth_assignment_user` (`userid`),
  CONSTRAINT `fk_yupe_user_user_auth_assignment_item` FOREIGN KEY (`itemname`) REFERENCES `yupe_user_user_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_yupe_user_user_auth_assignment_user` FOREIGN KEY (`userid`) REFERENCES `yupe_user_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_user_user_auth_assignment
-- ----------------------------
INSERT INTO `yupe_user_user_auth_assignment` VALUES ('admin', '1', null, null);

-- ----------------------------
-- Table structure for `yupe_user_user_auth_item`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_user_user_auth_item`;
CREATE TABLE `yupe_user_user_auth_item` (
  `name` char(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`name`),
  KEY `ix_yupe_user_user_auth_item_type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_user_user_auth_item
-- ----------------------------
INSERT INTO `yupe_user_user_auth_item` VALUES ('admin', '2', 'Admin', null, null);

-- ----------------------------
-- Table structure for `yupe_user_user_auth_item_child`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_user_user_auth_item_child`;
CREATE TABLE `yupe_user_user_auth_item_child` (
  `parent` char(64) NOT NULL,
  `child` char(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `fk_yupe_user_user_auth_item_child_child` (`child`),
  CONSTRAINT `fk_yupe_user_user_auth_item_child_child` FOREIGN KEY (`child`) REFERENCES `yupe_user_user_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_yupe_user_user_auth_itemchild_parent` FOREIGN KEY (`parent`) REFERENCES `yupe_user_user_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_user_user_auth_item_child
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_yupe_settings`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_yupe_settings`;
CREATE TABLE `yupe_yupe_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_id` varchar(100) NOT NULL,
  `param_name` varchar(100) NOT NULL,
  `param_value` varchar(500) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `type` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_yupe_settings_module_id_param_name_user_id` (`module_id`,`param_name`,`user_id`),
  KEY `ix_yupe_yupe_settings_module_id` (`module_id`),
  KEY `ix_yupe_yupe_settings_param_name` (`param_name`),
  KEY `fk_yupe_yupe_settings_user_id` (`user_id`),
  CONSTRAINT `fk_yupe_yupe_settings_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=98 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_yupe_settings
-- ----------------------------
INSERT INTO `yupe_yupe_settings` VALUES ('1', 'yupe', 'siteDescription', '', '2018-12-26 17:51:10', '2020-08-26 10:12:15', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('2', 'yupe', 'siteName', '', '2018-12-26 17:51:10', '2020-08-26 10:12:15', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('3', 'yupe', 'siteKeyWords', '', '2018-12-26 17:51:10', '2020-08-26 10:12:15', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('4', 'yupe', 'email', 'nariman-abenov@mail.ru', '2018-12-26 17:51:10', '2018-12-26 17:51:10', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('5', 'yupe', 'theme', 'default', '2018-12-26 17:51:10', '2018-12-26 17:51:10', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('6', 'yupe', 'backendTheme', '', '2018-12-26 17:51:10', '2018-12-26 17:51:10', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('7', 'yupe', 'defaultLanguage', 'ru', '2018-12-26 17:51:10', '2018-12-26 17:51:10', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('8', 'yupe', 'defaultBackendLanguage', 'ru', '2018-12-26 17:51:10', '2018-12-26 17:51:10', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('9', 'homepage', 'mode', '2', '2019-01-14 09:39:38', '2019-01-14 09:39:38', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('10', 'homepage', 'target', '1', '2019-01-14 09:39:38', '2019-01-14 09:39:41', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('11', 'homepage', 'limit', '', '2019-01-14 09:39:38', '2019-01-14 09:39:38', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('12', 'page', 'editor', 'tinymce5', '2020-05-22 09:50:05', '2020-08-26 11:23:26', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('13', 'page', 'mainCategory', '', '2020-05-22 09:50:05', '2020-05-22 09:50:05', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('14', 'yupe', 'coreCacheTime', '3600', '2020-08-26 10:12:15', '2020-08-26 10:12:15', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('15', 'yupe', 'uploadPath', 'uploads', '2020-08-26 10:12:15', '2020-08-26 10:12:15', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('16', 'yupe', 'editor', 'tinymce5', '2020-08-26 10:12:15', '2020-08-26 13:23:47', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('17', 'yupe', 'availableLanguages', 'ru,uk,en,zh', '2020-08-26 10:12:15', '2020-08-26 10:12:15', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('18', 'yupe', 'allowedIp', '', '2020-08-26 10:12:15', '2020-08-26 10:12:15', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('19', 'yupe', 'hidePanelUrls', '0', '2020-08-26 10:12:15', '2020-08-26 10:12:15', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('20', 'yupe', 'logo', 'images/logo.png', '2020-08-26 10:12:15', '2020-08-26 10:12:15', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('21', 'yupe', 'allowedExtensions', 'gif, jpeg, png, jpg, zip, rar, doc, docx, xls, xlsx, pdf, svg', '2020-08-26 10:12:15', '2020-09-01 10:18:15', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('22', 'yupe', 'mimeTypes', 'image/gif,image/jpeg,image/png,application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document, application/zip,application/x-rar,application/x-rar-compressed, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, image/svg+xml, application/xhtml+xml, application/xml', '2020-08-26 10:12:15', '2020-09-01 10:21:39', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('23', 'yupe', 'maxSize', '5242880', '2020-08-26 10:12:15', '2020-08-26 10:12:15', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('24', 'yupe', 'defaultImage', '/images/nophoto.jpg', '2020-08-26 10:12:15', '2020-08-26 10:12:15', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('25', 'user', 'avatarMaxSize', '5242880', '2020-08-26 10:13:59', '2020-08-26 10:13:59', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('26', 'user', 'avatarExtensions', 'jpg,png,gif,jpeg', '2020-08-26 10:13:59', '2020-08-26 10:13:59', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('27', 'user', 'defaultAvatarPath', 'images/avatar.png', '2020-08-26 10:13:59', '2020-08-26 10:13:59', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('28', 'user', 'avatarsDir', 'avatars', '2020-08-26 10:13:59', '2020-08-26 10:13:59', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('29', 'user', 'showCaptcha', '0', '2020-08-26 10:13:59', '2020-08-26 10:13:59', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('30', 'user', 'minCaptchaLength', '3', '2020-08-26 10:13:59', '2020-08-26 10:13:59', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('31', 'user', 'maxCaptchaLength', '6', '2020-08-26 10:13:59', '2020-08-26 10:13:59', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('32', 'user', 'minPasswordLength', '8', '2020-08-26 10:13:59', '2020-08-26 10:13:59', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('33', 'user', 'autoRecoveryPassword', '0', '2020-08-26 10:13:59', '2020-08-26 10:13:59', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('34', 'user', 'recoveryDisabled', '0', '2020-08-26 10:13:59', '2020-08-26 10:13:59', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('35', 'user', 'registrationDisabled', '0', '2020-08-26 10:13:59', '2020-08-26 10:13:59', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('36', 'user', 'notifyEmailFrom', 'no-reply@dcmr.ru', '2020-08-26 10:13:59', '2020-08-26 10:13:59', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('37', 'user', 'logoutSuccess', '/', '2020-08-26 10:13:59', '2020-08-26 10:13:59', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('38', 'user', 'loginSuccess', '/', '2020-08-26 10:13:59', '2020-08-26 10:13:59', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('39', 'user', 'accountActivationSuccess', '/user/account/login', '2020-08-26 10:13:59', '2020-08-26 10:13:59', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('40', 'user', 'accountActivationFailure', '/user/account/registration', '2020-08-26 10:14:00', '2020-08-26 10:14:00', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('41', 'user', 'loginAdminSuccess', '/yupe/backend/index', '2020-08-26 10:14:00', '2020-08-26 10:14:00', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('42', 'user', 'registrationSuccess', '/user/account/login', '2020-08-26 10:14:00', '2020-08-26 10:14:00', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('43', 'user', 'sessionLifeTime', '7', '2020-08-26 10:14:00', '2020-08-26 10:14:00', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('44', 'user', 'usersPerPage', '20', '2020-08-26 10:14:00', '2020-08-26 10:14:00', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('45', 'user', 'emailAccountVerification', '1', '2020-08-26 10:14:00', '2020-08-26 10:14:00', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('46', 'user', 'badLoginCount', '3', '2020-08-26 10:14:00', '2020-08-26 10:14:00', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('47', 'user', 'phoneMask', '+7(999) 999-99-99', '2020-08-26 10:14:00', '2020-08-26 10:14:00', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('48', 'user', 'phonePattern', '/\\+7\\(\\d{3}\\) \\d{3}\\-\\d{2}\\-\\d{2}$/', '2020-08-26 10:14:00', '2020-08-26 10:14:00', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('49', 'user', 'generateNickName', '1', '2020-08-26 10:14:00', '2020-08-26 10:14:10', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('50', 'image', 'uploadPath', 'image', '2020-08-27 09:24:04', '2020-08-27 09:24:04', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('51', 'image', 'allowedExtensions', 'jpg,jpeg,png,gif,svg', '2020-08-27 09:24:04', '2020-08-31 17:34:05', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('52', 'image', 'minSize', '0', '2020-08-27 09:24:04', '2020-08-27 09:24:04', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('53', 'image', 'maxSize', '5242880', '2020-08-27 09:24:04', '2020-08-27 09:24:04', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('54', 'image', 'mainCategory', '', '2020-08-27 09:24:04', '2020-08-27 09:24:04', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('55', 'image', 'mimeTypes', 'image/gif, image/jpeg, image/png, image/svg+xml', '2020-08-27 09:24:04', '2020-08-31 17:34:06', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('56', 'image', 'width', '1950', '2020-08-27 09:24:04', '2020-08-27 09:24:04', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('57', 'image', 'height', '1950', '2020-08-27 09:24:04', '2020-08-27 09:24:04', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('58', 'storecategory', 'pageSize', '100', '2020-08-27 10:31:57', '2020-08-27 10:31:57', '1', '2');
INSERT INTO `yupe_yupe_settings` VALUES ('59', 'news', 'editor', '', '2020-08-31 15:14:28', '2020-08-31 15:14:28', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('60', 'news', 'mainCategory', '', '2020-08-31 15:14:28', '2020-08-31 15:14:28', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('61', 'news', 'uploadPath', 'news', '2020-08-31 15:14:28', '2020-08-31 15:14:28', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('62', 'news', 'allowedExtensions', 'jpg,jpeg,png,gif', '2020-08-31 15:14:28', '2020-08-31 15:14:28', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('63', 'news', 'minSize', '0', '2020-08-31 15:14:28', '2020-08-31 15:14:28', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('64', 'news', 'maxSize', '5368709120', '2020-08-31 15:14:28', '2020-08-31 15:14:28', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('65', 'news', 'rssCount', '10', '2020-08-31 15:14:28', '2020-08-31 15:14:28', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('66', 'news', 'perPage', '10', '2020-08-31 15:14:28', '2020-08-31 15:14:28', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('67', 'news', 'metaTitle', '', '2020-08-31 15:14:28', '2020-08-31 15:14:28', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('68', 'news', 'metaDescription', '', '2020-08-31 15:14:28', '2020-08-31 15:14:28', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('69', 'news', 'metaKeyWords', '', '2020-08-31 15:14:28', '2020-08-31 15:14:28', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('70', 'news', 'newsId', '1', '2020-08-31 15:14:28', '2020-08-31 15:14:28', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('71', 'news', 'stockId', '', '2020-08-31 15:14:28', '2020-08-31 15:14:28', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('72', 'order', 'notifyEmailFrom', 'no-reply@dcmr.ru', '2020-10-01 09:58:51', '2020-10-01 09:58:51', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('73', 'order', 'notifyEmailsTo', 'nariman-abenov@mail.ru', '2020-10-01 09:58:51', '2020-10-01 09:58:51', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('74', 'order', 'showOrder', '1', '2020-10-01 09:58:51', '2020-10-01 09:58:51', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('75', 'order', 'enableCheck', '1', '2020-10-01 09:58:51', '2020-10-01 09:58:51', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('76', 'order', 'defaultStatus', '1', '2020-10-01 09:58:51', '2020-10-01 09:58:51', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('77', 'order', 'enableComments', '1', '2020-10-01 09:58:51', '2020-10-01 09:58:51', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('78', 'store', 'uploadPath', 'store', '2020-10-01 10:29:49', '2020-10-01 10:29:49', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('79', 'store', 'defaultImage', '/images/nophoto.jpg', '2020-10-01 10:29:49', '2020-10-01 10:29:49', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('80', 'store', 'editor', '', '2020-10-01 10:29:49', '2020-10-01 10:29:49', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('81', 'store', 'itemsPerPage', '20', '2020-10-01 10:29:50', '2020-10-01 10:29:50', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('82', 'store', 'phone', '', '2020-10-01 10:29:50', '2020-10-01 10:29:50', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('83', 'store', 'email', '', '2020-10-01 10:29:50', '2020-10-01 10:29:50', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('84', 'store', 'currency', 'RUB', '2020-10-01 10:29:50', '2020-10-01 10:29:50', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('85', 'store', 'title', '', '2020-10-01 10:29:50', '2020-10-01 10:29:50', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('86', 'store', 'address', '', '2020-10-01 10:29:50', '2020-10-01 10:29:50', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('87', 'store', 'city', '', '2020-10-01 10:29:50', '2020-10-01 10:29:50', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('88', 'store', 'zipcode', '', '2020-10-01 10:29:50', '2020-10-01 10:29:50', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('89', 'store', 'defaultSort', 'position', '2020-10-01 10:29:50', '2020-10-01 10:29:50', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('90', 'store', 'defaultSortDirection', 'ASC', '2020-10-01 10:29:50', '2020-10-01 10:29:50', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('91', 'store', 'metaTitle', '', '2020-10-01 10:29:50', '2020-10-01 10:29:50', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('92', 'store', 'metaDescription', '', '2020-10-01 10:29:50', '2020-10-01 10:29:50', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('93', 'store', 'metaKeyWords', '', '2020-10-01 10:29:50', '2020-10-01 10:29:50', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('94', 'store', 'controlStockBalances', '', '2020-10-01 10:29:50', '2020-10-01 10:29:50', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('95', 'store', 'notCategoryId', '25', '2020-10-01 10:29:50', '2020-10-01 10:29:50', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('96', 'moysklad', 'login', 'Sayt@santexnika174', '2021-01-21 10:26:11', '2021-01-21 10:26:11', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('97', 'moysklad', 'password', '12345678', '2021-01-21 10:26:11', '2021-01-21 10:26:11', '1', '1');
