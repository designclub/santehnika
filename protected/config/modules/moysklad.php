<?php

return [
    'module' => [
        'class' => 'application.modules.moysklad.MoyskladModule',
    ],
    'component' => [
        'moysklad' => [
            'class' => 'application.modules.moysklad.components.MoyskladClient'
        ],
    ],
    'commandMap' => [
        'moysklad-import' => [
            'class' => 'application.modules.moysklad.commands.ImportCommand',
        ],
    ]
];
