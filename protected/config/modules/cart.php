<?php

return [
    'module' => [
        'class' => 'application.modules.cart.CartModule',
    ],
    'import' => [
        'application.modules.cart.components.shopping-cart.*',
        'application.modules.cart.events.*',
        'application.modules.cart.models.CartProduct',
    ],
    'component' => [
        'cart' => [
            'class' => 'application.modules.cart.components.shopping-cart.EShoppingCart',
        ],
        'dadata' => [
            'class' => 'application.modules.cart.components.DaDataComponent',
            'token' => '6cbe709a0b66d86b698623e2d13434203fb13617',
        ],
    ],
    'rules' => [
        '/cart'                            => 'cart/cart/index',
        '/cart/<action:\w+>'               => 'cart/cart/<action>',
        '/cart/<action:\w+>/<id:\w+>'      => 'cart/cart/<action>',
        '/dadata/<action:\w+>'             => 'cart/dadata/<action>',
        '/dadata/<action:\w+>/<id:\w+>'    => 'cart/dadata/<action>',
        '/cart/user-ajax/<action>'         => 'cart/userAjax/<action>',
    ],
];
