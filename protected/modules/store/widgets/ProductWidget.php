<?php

Yii::import('application.modules.store.models.*');

class ProductWidget extends yupe\widgets\YWidget
{   
    /**
     * @var
     */
    public $title;
    public $product_id;
    public $category_id;
    public $is_home = false;
    /**
     * @var bool
     */
    public $limit = false;
    /**
     * @var string
     */
    public $view = 'default';

    /**
     * @return bool
     * @throws CException
     */
    public function run()
    {
        $criteria = new CDbCriteria();

        if($this->limit){
            $criteria->limit = $this->limit;
        }
        
        $criteria->order = 't.position ASC';
        
        if($this->category_id){
            $criteria->addCondition("t.category_id={$this->category_id}");
        }

        if($this->is_home){
            $criteria->addCondition("t.is_home={$this->is_home}");
        }

        if($this->product_id){
            $this->product_id = explode(',', $this->product_id);
            $criteria->addNotInCondition('id', $this->product_id);
        }
        
        $products = Product::model()->published()->findAll($criteria);

        $this->render(
            $this->view,
            [
                'products' => $products,
            ]
        );
    }
}