<?php

/**
 * Class CategoryFilterWidget
 */
class CategoryFilterWidget extends \yupe\widgets\YWidget
{
    /**
     * @var string
     */
    public $view = 'category-filter';

    /**
     * @var
     */
    public $category_id;
    public $category;

    /**
     * @throws CException
     */
    public function run()
    {
        

        if($this->category_id){
            $criteria = new CDbCriteria();
            $criteria->compare('parent_id', $this->category_id);

            $this->render($this->view, [
                'categories' => StoreCategory::model()->published()->findAll($criteria),
            ]);

        } else{
            $criteria = new CDbCriteria();
            $criteria->order = 't.sort ASC';
            $notIds = explode(',', Yii::app()->getModule('store')->notCategoryId);
            $criteria->addNotInCondition('id', $notIds);

            $categories = $this->category ? $this->category->child() : StoreCategory::model()->roots();

            $this->render($this->view, [
                'categories' => $categories->published()->findAll($criteria),
            ]);
        }
    }
} 
