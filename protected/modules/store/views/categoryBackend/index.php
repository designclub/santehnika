<?php
$this->breadcrumbs = [
    Yii::t('StoreModule.store', 'Categories') => ['/store/categoryBackend/index'],
    Yii::t('StoreModule.store', 'Manage'),
];

$this->pageTitle = Yii::t('StoreModule.store', 'Categories - manage');

$this->menu = [
    [
        'icon' => 'fa fa-fw fa-list-alt',
        'label' => Yii::t('StoreModule.store', 'Manage categories'),
        'url' => ['/store/categoryBackend/index'],
    ],
    [
        'icon' => 'fa fa-fw fa-plus-square',
        'label' => Yii::t('StoreModule.store', 'Create category'),
        'url' => ['/store/categoryBackend/create'],
    ],
];
?>
<div class="page-header">
    <h1>
        <?= Yii::t('StoreModule.store', 'Categories'); ?>
        <small><?= Yii::t('StoreModule.store', 'administration'); ?></small>
    </h1>
</div>

<?php $this->widget(
    'yupe\widgets\CustomGridView',
    [
        'id' => 'category-grid',
        'sortableRows' => true,
        'sortableAjaxSave' => true,
        'sortableAttribute' => 'sort',
        'sortableAction' => '/store/categoryBackend/sortable',
        'type' => 'condensed',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'actionsButtons' => [
            CHtml::link(
                Yii::t('YupeModule.yupe', 'Add'),
                ['/store/categoryBackend/create'],
                ['class' => 'btn btn-success pull-right btn-sm']
            ),
        ],
        'columns' => [
            [
                'name' => 'image',
                'type' => 'raw',
                'value' => function ($data) {
                    return CHtml::image(StoreImage::category($data, 40, 40), $data->name, ["width" => 40, "height" => 40, "class" => "img-thumbnail"]);
                },
                'filter' => false,
            ],
            // [
            //     'name' => 'icon',
            //     'type' => 'raw',
            //     'value' => function ($data) {
            //         return CHtml::image($data->getImageNewUrl(0, 0, true, null, 'icon'), $data->name, ["width" => 40, "height" => 40, "class" => "img-thumbnail"]);
            //     },
            //     'filter' => false,
            // ],
            [
                'name' => 'name',
                'type' => 'raw',
                'value' => function ($data) {
                    return CHtml::link($data->name, array("/store/categoryBackend/update", "id" => $data->id));
                },
            ],
            [
                'name' => 'slug',
                'type' => 'raw',
                'value' => function ($data) {
                    return CHtml::link($data->slug, array("/store/categoryBackend/update", "id" => $data->id));
                },
            ],
            [
                'name' => 'parent_id',
                'value' => function ($data) {
                    return $data->getParentName();
                },
                'filter' => CHtml::activeDropDownList(
                    $model,
                    'parent_id',
                    StoreCategoryHelper::formattedList(),
                    ['encode' => false, 'empty' => '', 'class' => 'form-control']
                ),
            ],
            [
                'name'  => 'is_home',
                'value' => function($data){
                    return ($data->is_home) ? "Да" : 'Нет';
                },
                'type' => 'raw',
                'filter' => CHtml::activeDropDownList($model, 'is_home', $model->getHomeList(), ['encode' => false, 'empty' => '', 'class' => 'form-control']),
                'htmlOptions' => ['width' => '220px'],
            ],
            [
                'class' => 'yupe\widgets\EditableStatusColumn',
                'name' => 'status',
                'url' => $this->createUrl('/store/categoryBackend/inline'),
                'source' => $model->getStatusList(),
                'options' => [
                    StoreCategory::STATUS_PUBLISHED => ['class' => 'label-success'],
                    StoreCategory::STATUS_DRAFT => ['class' => 'label-default'],
                ],
            ],
            [
                'value' => function ($data) {
                    return CHtml::link(
                        Yii::t('StoreModule.store', 'Products'),
                        ['/store/productBackend/index', "Product[category_id]" => $data->id],
                        ['class' => 'label label-default']
                    );
                },
                'type' => 'raw',
            ],
            [
                'class' => 'yupe\widgets\CustomButtonColumn',
                'frontViewButtonUrl' => function($data){
                    return Yii::app()->createUrl('/store/category/view', ['path' => $data->path]);
                },
                'buttons' => [
                    'front_view' => [
                        'visible' => function ($row, $data) {
                            return $data->status == StoreCategory::STATUS_PUBLISHED;
                        },
                    ],
                ],
            ],
        ],
    ]
); ?>
