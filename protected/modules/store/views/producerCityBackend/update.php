<?php
/**
 * Отображение для update:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     https://yupe.ru
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('StoreModule.store', 'Города') => ['/store/producerCityBackend/index'],
    $model->name => ['/store/producerCityBackend/view', 'id' => $model->id],
    Yii::t('StoreModule.store', 'Редактирование'),
];

$this->pageTitle = Yii::t('StoreModule.store', 'Города - редактирование');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('StoreModule.store', 'Управление Городами'), 'url' => ['/store/producerCityBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('StoreModule.store', 'Добавить Город'), 'url' => ['/store/producerCityBackend/create']],
    ['label' => Yii::t('StoreModule.store', 'Город') . ' «' . mb_substr($model->id, 0, 32) . '»'],
    ['icon' => 'fa fa-fw fa-pencil', 'label' => Yii::t('StoreModule.store', 'Редактирование Города'), 'url' => [
        '/store/producerCityBackend/update',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-eye', 'label' => Yii::t('StoreModule.store', 'Просмотреть Город'), 'url' => [
        '/store/producerCityBackend/view',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-trash-o', 'label' => Yii::t('StoreModule.store', 'Удалить Город'), 'url' => '#', 'linkOptions' => [
        'submit' => ['/store/producerCityBackend/delete', 'id' => $model->id],
        'confirm' => Yii::t('StoreModule.store', 'Вы уверены, что хотите удалить Город?'),
        'csrf' => true,
    ]],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('StoreModule.store', 'Редактирование') . ' ' . Yii::t('StoreModule.store', 'Города'); ?>        <br/>
        <small>&laquo;<?=  $model->name; ?>&raquo;</small>
    </h1>
</div>

<?=  $this->renderPartial('_form', ['model' => $model]); ?>