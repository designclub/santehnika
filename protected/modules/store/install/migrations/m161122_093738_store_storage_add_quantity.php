<?php

class m161122_093738_store_storage_add_quantity extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{store_storage_count}}', 'quantity', 'int(11) null');
    }

    public function safeDown()
    {
        $this->dropColumn('{{store_storage_count}}', 'quantity');
    }
}
