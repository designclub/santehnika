<?php

class m181218_121830_store_producer_add_column_is_home extends yupe\components\DbMigration
{
    public function safeUp()
    {
    	$this->addColumn('{{store_producer}}', 'is_home', "boolean not null default '0'");
    }

    public function safeDown()
    {
        $this->dropColumn('{{store_producer}}', 'is_home');
    }
}