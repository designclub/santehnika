<?php

/**
 * Class ProducerRepository
 */
class ProducerRepository extends CApplicationComponent
{

    /**
     * @param StoreCategory $category
     * @param CDbCriteria $mergeWith
     * @return array|mixed|null
     */
    /*public function getForCategory(StoreCategory $category, CDbCriteria $mergeWith)
    {
        $criteria = new CDbCriteria([
            'order' => 't.sort ASC',
            'join' => 'LEFT JOIN {{store_product}} AS products ON products.producer_id = t.id',
            'distinct' => true,
        ]);
        $criteria->addInCondition('products.category_id', [$category->id]);
        $criteria->mergeWith($mergeWith);

        return Producer::model()->findAll($criteria);
    }
    */
    public function getForCategory(StoreCategory $category, CDbCriteria $mergeWith)
    {
        $criteria = new CDbCriteria([
            'order' => 't.sort ASC',
        ]);
        $criteria->mergeWith($mergeWith);

        $categories = $category->getChildsArray();
        array_push($categories, $category->id);
        
        if (!empty($categories)) {
            $ids = array_column(Yii::app()
                ->db
                ->createCommand()
                ->select('pr.id')
                ->from('{{store_product}} p')
                ->join('{{store_producer}} pr', 'p.producer_id=pr.id')
                ->where(['in', 'category_id', $categories])
                ->group('pr.id')
                ->queryAll(), 'id');

            $criteria->addInCondition('t.id', $ids);
        }

        return Producer::model()->findAll($criteria);
    }

    public function getForCityCategory(StoreCategory $category, CDbCriteria $mergeWith)
    {
        $criteria = new CDbCriteria([
            'order' => 't.order ASC',
        ]);
        $criteria->mergeWith($mergeWith);

        $categories = $category->getChildsArray();
        array_push($categories, $category->id);

        if (!empty($categories)) {
            $categoriesIds = implode(',', $categories);
 
            $cityIds = array_column(Yii::app()
                ->db
                ->createCommand()
                ->select('pr_city.id')
                ->from('{{store_producer_city}} pr_city')
                ->join('{{store_producer}} pr', 'pr.city_id = pr_city.id')
                ->where("pr.id IN (SELECT p.producer_id FROM yupe_store_product p WHERE p.category_id IN ({$categoriesIds}) GROUP BY p.producer_id)")
                ->group('pr_city.id')
                ->queryAll(), 'id');

           $criteria->addInCondition('t.id', $cityIds);
        }


        return ProducerCity::model()->findAll($criteria);
    }


    /**
     * @return CActiveDataProvider
     */
    public function getAllDataProvider()
    {
        $criteria = new CDbCriteria();
        $criteria->scopes = ['published'];
        $criteria->order = 'sort';

        return new CActiveDataProvider(
            'Producer', [
                'criteria' => $criteria,
                'pagination' => [
                    'pageSize' => 20,
                    'pageVar' => 'page',
                ],
            ]
        );
    }

    /**
     * @param $slug
     * @return mixed
     */
    public function getBySlug($slug)
    {
        return Producer::model()->published()->find('slug = :slug', [':slug' => $slug]);
    }
}