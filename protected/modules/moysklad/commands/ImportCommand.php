<?php

use application\modules\moysklad\importers\CategoriesImporter;
use application\modules\moysklad\importers\ProductsImporter;
use application\modules\moysklad\importers\RestsImporter;
use application\modules\moysklad\importers\WarehousesImporter;
use application\modules\moysklad\importers\CounterpartyImporter;
use application\modules\moysklad\importers\BalanceImporter;

/**
 * Class ImportCommand
 */
// class ImportCommand extends CConsoleCommand
class ImportCommand extends \yupe\components\ConsoleCommand
{
    public function actionIndex()
    {
        $this->actionCategories();
        $this->actionProducts();
        $this->actionWarehouses();
        $this->actionRests();

        $this->actionCounterparty();

//       всякие обнуления
//       $this->actionСrutch();
//       $this->actionCounterparty();
//       $this->actionBalance();
    }

    public function actionCategories()
    {
        $executor = new CategoriesImporter();
        $executor->execute();
    }

    public function actionProducers()
    {
        echo PHP_EOL . 'I can import producers!' . PHP_EOL;
    }

    public function actionProducts()
    {
        $executor = new ProductsImporter();
        $executor->execute();
    }

    public function actionWarehouses()
    {
        $executor = new WarehousesImporter();
        $executor->execute();
    }

    public function actionRests()
    {
        $executor = new RestsImporter();
        $executor->execute();
    }


    public function actionCounterparty()
    {
        $executor = new CounterpartyImporter();
        $executor->execute();
    }


    public function actionBalance()
    {
        $executor = new BalanceImporter();
        $executor->execute();
    }


//    public function actionСrutch()
//    {
//        $executor = new RestsImporter();
//        $executor->execute();
//    }
}
