<?php

namespace application\modules\moysklad\importers;

use application\modules\moysklad\repositories\ExternalEntitiesRepository;
use application\modules\moysklad\repositories\ModelsRepository;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use MoySklad\Components\Fields\ImageField;
use yupe\helpers\YText;

class RestsImporter implements Command
{
    protected $modelsRepository;
    protected $externalEntitiesRepository;

    public function __construct()
    {
        $this->modelsRepository = new ModelsRepository();
        $this->externalEntitiesRepository = new ExternalEntitiesRepository();
    }


    public function execute()
    {
        $imported = 0;
        $updated = 0;
        $processedIds = [];
        $errors = [];

        $warehouses = \Yii::app()->db->createCommand('SELECT id, external_id FROM {{store_warehouse}} WHERE status = :status')
            ->bindValue(':status', \Warehouse::STATUS_ACTIVE)
            ->queryAll();

        foreach ($warehouses as $wh) {
            foreach ($this->externalEntitiesRepository->getRests($wh['external_id']) as $extRests) {

                $extRests->map(function ($extRest) use (&$imported, &$updated, &$errors, $wh, &$processedIds) {

                    if ($extRest->fields->meta->type !== 'product')
                        return;

                    /**
                     * @var \ProductRest $model
                     */
                    $productId = \Yii::app()->db->createCommand("SELECT id FROM {{store_product}} WHERE external_id = '{$extRest->id}'")->queryScalar();

                    if (!$productId)
                        return;

                    $model = $this->modelsRepository->getProductRest($productId, $wh['id']);
                    $model->product_id = $productId;
                    $model->warehouse_id = $wh['id'];
                    $model->quantity = ceil($extRest->quantity);
                    $model->price = !empty($extRest->salePrices) ? ($extRest->salePrices[0]->value / 100) : 0;

                    $model->purchase_price = !empty($extRest->buyPrice) ? ($extRest->buyPrice->value / 100) : 0;

                    if ($model->quantity > 0) {
                        $product = $model->product;
                        $product->in_stock = \Product::STATUS_IN_STOCK;
                        $product->save();
                    }

                    $isNewRecord = $model->getIsNewRecord();
                    if (!$model->save()) {
                        $errors[$extRest->id] = $model->getErrors();
                        return;
                    }

                    $processedIds[] = $model->id;

                    if ($isNewRecord)
                        $imported++;
                    else
                        $updated++;
                });
            }
        }

        $deleted = \Yii::app()->db->createCommand()
            ->delete(
                '{{store_product_rest}}',
                'id NOT IN(' . implode(',', $processedIds) . ')'
            );

        if (!empty($errors))
            echo json_encode($errors, JSON_UNESCAPED_UNICODE);

        echo PHP_EOL . "Rests updated: $updated imported: $imported deleted: $deleted" . PHP_EOL;

        return true;
    }
}