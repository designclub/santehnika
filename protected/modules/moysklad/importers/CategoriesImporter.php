<?php

namespace application\modules\moysklad\importers;

use application\modules\moysklad\repositories\ExternalEntitiesRepository;
use application\modules\moysklad\repositories\ModelsRepository;
use yupe\helpers\YText;

class CategoriesImporter implements Command
{
    protected $modelsRepository;
    protected $externalEntitiesRepository;

    public function __construct()
    {
        $this->modelsRepository = new ModelsRepository();
        $this->externalEntitiesRepository = new ExternalEntitiesRepository();
    }

    public function execute()
    {
        $imported = 0;
        $updated = 0;
        $errors = [];
        $catToCatRelations = [];
        foreach ($this->externalEntitiesRepository->getCategories() as $extCats) {
            $model = $this->modelsRepository->getCategory($extCat);
            print_r($model->attributes);
            echo "\n";
            // Yii::app()->end();
            $extCats->map(function ($extCat) use (&$imported, &$updated, &$catToCatRelations, &$errors) {

//                Пропускаем категорию с именем /cml/sync
                if (!stristr($extCat->pathName, 'Товары интернет-магазинов')) {
                    return;
                }
                if (stristr($extCat->name, 'Товары интернет-магазинов')) {
                    return;
                }

                $model = $this->modelsRepository->getCategory($extCat);

//                $model = $this->modelsRepository->getCategory($extCat->id);

                $isNewRecord = $model->getIsNewRecord();

                $model->external_id = $extCat->id;
                $model->name = $extCat->name;
                $model->status = '1';

                if (!empty($extCat->description)) {
                    $model->description = $model->description ?: $extCat->description;
                }

                if ($isNewRecord) {
                    $model->slug = YText::translit($extCat->name);
                }

                if (!$model->save()) {
                    $errors[$extCat->id] = $model->getErrors();
                    return;
                }


                if (!empty($extCat->relations->productFolder)&
                    !empty($extCat->relations->productFolder->meta)&
                    !empty($extCat->relations->productFolder->meta->href)
                ) {
                    $catToCatRelations[$model->id] = basename($extCat->relations->productFolder->meta->href);
                }

                if ($isNewRecord) {
                    $imported++;
                } else {
                    $updated++;
                }
            });
        }

        foreach ($catToCatRelations as $modelId => $relatedExtId) {
            $parent = $this->modelsRepository->getCategory($relatedExtId);

            if (!$parent->getIsNewRecord()) {
                \Yii::app()->db->createCommand()->update('{{store_category}}', ['parent_id' => $parent->id], "id = $modelId");
            }
        }


        if (!empty($errors)) {
            echo json_encode($errors, JSON_UNESCAPED_UNICODE);
        }

        echo PHP_EOL . "Categories updated: $updated imported: $imported" . PHP_EOL;

        return true;
    }
}
