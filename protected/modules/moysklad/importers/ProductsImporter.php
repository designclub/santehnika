<?php

namespace application\modules\moysklad\importers;

use application\modules\moysklad\repositories\ExternalEntitiesRepository;
use application\modules\moysklad\repositories\ModelsRepository;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use MoySklad\Components\Fields\ImageField;
use yupe\helpers\YText;

class ProductsImporter implements Command
{
    protected $modelsRepository;
    protected $externalEntitiesRepository;

    public function __construct()
    {
        $this->modelsRepository = new ModelsRepository();
        $this->externalEntitiesRepository = new ExternalEntitiesRepository();
    }


    public function execute()
    {
        $imported = 0;
        $updated = 0;
        $processedIds = [];
        $errors = [];
        foreach ($this->externalEntitiesRepository->getProducts() as $extProducts) {

            $extProducts->map(function ($extProduct) use (&$imported, &$updated, &$errors, &$processedIds) {

//                $this->saveImage($extProduct);

                /**
                 * @var \Product $model
                 */

                if (!$this->findProductCategory($extProduct))
                    return;

                $model = $this->modelsRepository->getProduct($extProduct);

                $isNewRecord = $model->getIsNewRecord();

                $model->external_id = $extProduct->id;
                $model->name = $extProduct->name;
                $model->status = \Product::STATUS_ACTIVE;
                $model->in_stock = \Product::STATUS_NOT_IN_STOCK;
                $model->weight = $extProduct->weight;

                $model->price = !empty($extProduct->salePrices) ? ($extProduct->salePrices[0]->value / 100) : 0;

                if (!empty($extProduct->article))
                    $model->sku = $extProduct->article;

                if (!empty($extProduct->description))
                    $model->description = $model->description ?: $extProduct->description;

                if ($isNewRecord)
                    $model->slug = YText::translit($extProduct->name);

                // Категория
                $model = $this->setCategory($model, $extProduct);

                // Картинка
//                $model = $this->setImage($model, $extProduct);

                if (!$model->save()) {

                    if ($model->hasErrors('slug'))
                        $model->slug = $model->slug . '-' . time();

                    if (!$model->save()) {
                        $errors[$extProduct->id] = $model->getErrors();
                        return;
                    }
                }

                $processedIds[] = $model->id;

                if ($isNewRecord)
                    $imported++;
                else
                    $updated++;
            });
        }

        $deactivated = \Yii::app()->db->createCommand()
            ->update(
                '{{store_product}}',
                ['in_stock' => \Product::STATUS_NOT_IN_STOCK]
            );

        if (!empty($errors))
            echo json_encode($errors, JSON_UNESCAPED_UNICODE);

        echo PHP_EOL . "Products updated: $updated imported: $imported deactivated: $deactivated" . PHP_EOL;

        return true;
    }

    protected function findProductCategory($extProduct)
    {
        if (empty($extProduct->relations)
            || empty($extProduct->relations->productFolder)
            || empty($extProduct->relations->productFolder->fields)
            || empty($extProduct->relations->productFolder->fields->meta))
            return false;

        $extCatId = basename($extProduct->relations->productFolder->fields->meta->href);
        if ($extCatId) {
            $catId = \Yii::app()->db->createCommand("SELECT id FROM {{store_category}} WHERE external_id = '{$extCatId}'")->queryScalar();
            if ($catId) {
               return $catId;
            }
        }

        return false;
    }


    protected function setCategory($model, $extProduct)
    {
        if (empty($extProduct->relations)
            || empty($extProduct->relations->productFolder)
            || empty($extProduct->relations->productFolder->fields)
            || empty($extProduct->relations->productFolder->fields->meta))
            return $model;

        $extCatId = basename($extProduct->relations->productFolder->fields->meta->href);
        if ($extCatId) {
            $catId = \Yii::app()->db->createCommand("SELECT id FROM {{store_category}} WHERE external_id = '{$extCatId}'")->queryScalar();
            if ($catId) {
                $model->category_id = $catId;
            }
        }

        return $model;
    }


    protected function saveImage($extProduct)
    {

        if (!empty($extProduct->image) && $extProduct->image instanceof ImageField) {
                $fp = $this->generateFileUploadPath($extProduct->externalCode, $extProduct->image->filename);

                try {
                    $res = $this->download($fp, $extProduct->image, 'normal');

                    if ($res == 200)
                    {
                        true;
                    }
                } catch (\Exception $e) {


                }
        }

        return true;
    }


    protected function setImage($model, $extProduct)
    {

        if (!empty($extProduct->image) && $extProduct->image instanceof ImageField) {
            if (!$model->image) {

                $fp = $this->generateFileUploadPath($model, $extProduct->image->filename);

                try {
                    $res = $this->download($fp, $extProduct->image, 'normal');
                    if ($res == 200)
                        $model->image = basename($fp, PATHINFO_FILENAME);
                } catch (\Exception $e) {


                }
            }
        } else {
            $model->image = null;
        }

        return $model;
    }

    /**
     * @return string generated file name.
     */
    public function generateFileUploadPath($modelId, $fileName)
    {
        $path = \Yii::getPathOfAlias('webroot') . '/uploads/do1c';

        $name = $path . '/' .$modelId;

        $name .= '.' . pathinfo($fileName, PATHINFO_EXTENSION);

        return $name;
    }

    /**
     * @param $saveFile
     * @param string $size
     * @return string
     * @throws \Exception
     */
    public function download($saveFile, $imageField, $size = 'normal')
    {
        if ($link = $imageField->getDownloadLink($size)) {
            $filePath = fopen($saveFile, 'w+');

            $module = \Yii::app()->getModule('moysklad');
            $client = new Client([
                'auth' => [$module->login, $module->password],
            ]);
//            $response = $client->requestAsync('GET', $link, ['debug' => true, RequestOptions::SINK => $filePath])->;
            $response = $client->get($link, [RequestOptions::SINK => $filePath]);
            return $response->getStatusCode();
        }
        throw new \Exception("Image does not have a $size size link. Try to refresh hosting entity");
    }
}