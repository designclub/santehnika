<?php

namespace application\modules\moysklad\importers;

use application\modules\moysklad\repositories\ExternalEntitiesRepository;
use application\modules\moysklad\repositories\ModelsRepository;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use MoySklad\Components\Fields\ImageField;
use yupe\helpers\YText;

class WarehousesImporter implements Command
{
    protected $modelsRepository;
    protected $externalEntitiesRepository;

    public function __construct()
    {
        $this->modelsRepository = new ModelsRepository();
        $this->externalEntitiesRepository = new ExternalEntitiesRepository();
    }

    public function execute()
    {
        $imported = 0;
        $updated = 0;
        $processedIds = [];
        $errors = [];
        foreach ($this->externalEntitiesRepository->getWarehouses() as $extWarehouses) {
            $extWarehouses->map(function ($extWH) use (&$imported, &$updated, &$errors, &$processedIds) {
                /**
                 * @var \Warehouse $model
                 */
                $model = $this->modelsRepository->getWarehouse($extWH->id);
                $model->external_id = $extWH->id;
                $model->name = $extWH->name;
                $model->address = $extWH->address;
                $model->status = \Warehouse::STATUS_ACTIVE;

                $isNewRecord = $model->getIsNewRecord();
                if (!$model->save()) {
                    $errors[$extWH->id] = $model->getErrors();
                    return;
                }

                $processedIds[] = $model->id;

                if ($isNewRecord)
                    $imported++;
                else
                    $updated++;
            });
        }

        $deactivated = \Yii::app()->db->createCommand()
            ->update(
                '{{store_warehouse}}',
                ['status' => \Warehouse::STATUS_NOT_ACTIVE],
                'id NOT IN('. implode(',', $processedIds) .')'
            );

        if (!empty($errors))
            echo json_encode($errors, JSON_UNESCAPED_UNICODE);

        echo PHP_EOL."Warehouses updated: $updated imported: $imported deactivated: $deactivated".PHP_EOL;

        return true;
    }
}