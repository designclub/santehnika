<?php

namespace application\modules\moysklad\importers;

use application\modules\moysklad\repositories\ExternalEntitiesRepository;
use application\modules\moysklad\repositories\ModelsRepository;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use MoySklad\Components\Fields\ImageField;
use yupe\helpers\YText;

class CounterpartyImporter implements Command
{
    protected $modelsRepository;
    protected $externalEntitiesRepository;

    public function __construct()
    {
        $this->modelsRepository = new ModelsRepository();
        $this->externalEntitiesRepository = new ExternalEntitiesRepository();
    }


    public function execute()
    {
        $imported = 0;
        $updated = 0;
        $processedIds = [];
        $errors = [];
        $counterparties = [];

        foreach ($this->externalEntitiesRepository->getCounterparty() as $exCounterparties) {
            $exCounterparties->map(function ($exCounterparty) use (&$imported, &$updated, &$errors, &$processedIds, &$counterparties) {


                print_r($exCounterparty->fields->name);
                echo PHP_EOL;

                $counterparties[] = [
                    'external_id' => $exCounterparty->id,
                    'name' => $exCounterparty->fields->name,
                    'legalTitle' => $exCounterparty->fields->legalTitle ?? null,
                    'legalAddress' => $exCounterparty->fields->legalAddress ?? null,
                    'email' => $exCounterparty->fields->email ?? null,
                    'phone' => $exCounterparty->fields->phone ?? null,
                    'inn' => $exCounterparty->fields->inn ?? null,
                    'balance' => $this->externalEntitiesRepository->getBalance($exCounterparty)/100,
                ];

            });
        }

        $cacheRepository = \Yii::app()->getCache();
        $cacheRepository->delete('importedCounterparties');
        $cacheRepository->set('importedCounterparties', $counterparties);

        print_r( $cacheRepository->get('importedCounterparties') );

        if (!empty($errors))
            echo json_encode($errors, JSON_UNESCAPED_UNICODE);


        return true;
    }

}