<?php


namespace application\modules\moysklad\importers;


interface Command
{
    public function execute();
}