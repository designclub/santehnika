<?php

namespace application\modules\moysklad\importers;

use application\modules\moysklad\repositories\ExternalEntitiesRepository;
use application\modules\moysklad\repositories\ModelsRepository;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use MoySklad\Components\Fields\ImageField;
use yupe\helpers\YText;

class BalanceImporter implements Command
{
    protected $modelsRepository;
    protected $externalEntitiesRepository;

    public function __construct()
    {
        $this->modelsRepository = new ModelsRepository();
        $this->externalEntitiesRepository = new ExternalEntitiesRepository();
    }


    public function execute()
    {
        $imported = 0;
        $updated = 0;
        $processedIds = [];
        $errors = [];


        foreach ($this->externalEntitiesRepository->getBalance() as $exBalances) {

            $exBalances->map(function ($exBalance) use (&$imported, &$updated, &$errors, &$processedIds) {

//                print_r($exCounterparty->fields->name.' '.$exCounterparty->fields->inn);
                print_r($exBalance);

                echo PHP_EOL;

            });

        }


        if (!empty($errors))
            echo json_encode($errors, JSON_UNESCAPED_UNICODE);


        return true;
    }



}