<?php


namespace application\modules\moysklad;


class QuerySpecs extends \MoySklad\Components\Specs\QuerySpecs\QuerySpecs
{
    /**
     * Get possible variables for spec, will be sent as query string
     *  limit: max results per request
     *  offset: get results with offset
     *  maxResults: get only this amount of results. If 0 - unlimited
     *  expand: use expand parameter to get chosen relations
     *  updatedFrom: entity should be updated from date
     *  updatedTo: entity should be updated up to date
     *  updatedBy: entity should be updated by employee
     * @return array
     */
    public function getDefaults()
    {
        return [
            "limit" => static::MAX_LIST_LIMIT,
            "offset" => 0,
            "maxResults" => 0,
            "expand" => null,
            "updatedFrom" => null,
            "updatedTo" => null,
            "updatedBy" => null,
            "order" => null,
            "scope" => null,
            "stockmode" => null,
            "stockstore" => null,
            'pathName' => null
        ];
    }
}