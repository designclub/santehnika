<?php

/**
 * MoyskladModule основной класс модуля install
 *
 * @author yupe team <team@yupe.ru>
 * @link https://yupe.ru
 * @copyright 2009-2013 amyLabs && Yupe! team
 * @package yupe.modules.mail
 * @since 0.1
 *
 */
class MoyskladModule extends yupe\components\WebModule
{
    /**
     *
     */
    const VERSION = '0.1';

    public $login;

    public $password;

    /**
     * @return array
     */
    public function getParamsLabels()
    {
        return [
            'login' => 'Login',
            'password' => 'Password',
        ];
    }

    /**
     * @return array
     */
    public function getEditableParams()
    {
        return [
            'login',
            'password',
        ];
    }


    /**
     * Метод получения версии:
     *
     * @return string version
     **/
    public function getVersion()
    {
        return self::VERSION;
    }

    /**
     * Метод получения категории:
     *
     * @return string category
     **/
    public function getCategory()
    {
        return Yii::t('YupeModule.yupe', 'Services');
    }

    /**
     * Метод получения названия модуля:
     *
     * @return string name
     **/
    public function getName()
    {
        return 'Moysklad';
    }

    /**
     * Метод получения описвния модуля:
     *
     * @return string description
     **/
    public function getDescription()
    {
        return 'Обмен с CRM Мойсклад';
    }

    /**
     * Метод получения иконки:
     *
     * @return string icon
     **/
    public function getIcon()
    {
        return 'fa fa-fw fa-refresh';
    }

    /**
     * Метод получения адреса модуля в админ панели:
     *
     * @return string admin url
     **/
    public function getAdminPageLink()
    {
        return '/backend/modulesettings?module=moysklad';
    }

    /**
     * Метод инициализации модуля:
     *
     * @return nothing
     **/
    public function init()
    {
        $this->checkDependencies();

        $this->setImport(
            [
                'moysklad.components.*',
            ]
        );

        parent::init();
    }

    private function checkDependencies()
    {
        if (!class_exists(\MoySklad\MoySklad::class)) {
            throw new Exception('Установите пакет "tooyz/moysklad"');
        }
    }

    /**
     * @return array
     */
    public function getDependencies()
    {
        return ['store'];
    }
}
