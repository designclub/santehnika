<?php

use MoySklad\MoySklad;

/**
 * Class MoyskladClient
 */
class MoyskladClient extends CApplicationComponent
{
    public $client;

    /**
     *
     */
    public function init()
    {
        $module = Yii::app()->getModule('moysklad');
        $login = $module->login;
        $password = $module->password;

        if (empty($login) || empty($password))
            throw new InvalidArgumentException('Заполните логин, пароль в настройках модуля "Мойсклад"');

        $this->client = MoySklad::getInstance($login, $password);
    }
}