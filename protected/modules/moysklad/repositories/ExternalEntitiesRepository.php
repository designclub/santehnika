<?php

namespace application\modules\moysklad\repositories;

use MoySklad\Components\Specs\QuerySpecs\QuerySpecs;
use MoySklad\Components\Specs\QuerySpecs\Reports\SalesReportQuerySpecs;
use MoySklad\Entities\Assortment;
use MoySklad\Entities\Counterparty;
use MoySklad\Entities\Folders\ProductFolder;
use MoySklad\Entities\Products\Product;
use MoySklad\Entities\Reports\SalesReport;
use MoySklad\Entities\Reports\CounterpartyReport;
use MoySklad\Entities\Store;

/**
 * Class ModelsRepository
 */
class ExternalEntitiesRepository
{
    public $client;

    public function __construct()
    {
        $this->client = \Yii::app()->moysklad->client;
    }

    public function getCategories($pathName = null)
    {
        $perRequest = 50;
        $params = [
            "maxResults" => $perRequest
        ];

        if ($pathName)
            $params['pathName'] = $pathName;

        for ($i = 0; $i <= 1000000; ++$i) {
            $params['offset'] = $perRequest * $i;
            $result = ProductFolder::query($this->client,
                \application\modules\moysklad\QuerySpecs::create($params)
            )->getList();

            if ($result->count() == 0)
                break;

            yield $result;
        }
    }


    public function getProducts()
    {
        $perRequest = 100;
        for ($i = 0; $i <= 100000; ++$i) {

            $result = Product::query($this->client, QuerySpecs::create([
                "offset" => $perRequest * $i,
                "maxResults" => $perRequest,
            ]))->getList();

            if ($result->count() == 0)
                break;

            yield $result;
        }
    }

    public function getWarehouses()
    {
        $perRequest = 100;
        for ($i = 0; $i <= 100000; ++$i) {

            $result = Store::query($this->client, QuerySpecs::create([
                "offset" => $perRequest * $i,
                "maxResults" => $perRequest,
            ]))->getList();

            if ($result->count() == 0)
                break;

            yield $result;
        }
    }

    public function getRests($warehouseId)
    {
        $perRequest = 50;

        $params = [
            'stockmode' => 'positiveOnly',
            'maxResults' => $perRequest,
        ];

        if ($warehouseId)
            $params['stockstore'] = "https://online.moysklad.ru/api/remap/1.1/entity/store/$warehouseId";

        for ($i = 0; $i <= 1000000; ++$i) {
            $params['offset'] = $perRequest * $i;
            $result = Assortment::query(
                $this->client,
                \application\modules\moysklad\QuerySpecs::create($params)
            )->getList();

            if ($result->count() == 0)
                break;

            yield $result;
        }
    }


    public function getCounterparty()
    {
        $perRequest = 10;
        for ($i = 0; $i <= 100000; ++$i) {

            $result = Counterparty::query($this->client, QuerySpecs::create([
                "offset" => $perRequest * $i,
                "maxResults" => $perRequest,
            ]))->getList();

            if ($result->count() == 0)
                break;

            yield $result;
        }
    }


    public function getBalance($cpId)
    {

        $report = CounterpartyReport::byCounterparty($this->client,$cpId);

       return $report->balance;

    }

}