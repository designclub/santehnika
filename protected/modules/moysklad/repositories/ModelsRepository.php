<?php

namespace application\modules\moysklad\repositories;

use yupe\helpers\YText;

/**
 * Class ModelsRepository
 */
class ModelsRepository
{
    public function getCategory($extProduct)
    {
        $model = \StoreCategory::model()->findByAttributes(['external_id' => $extProduct->id], ['order' => 'id ASC']);

        if ((null == $model))
            $model = \StoreCategory::model()->findByAttributes(['name' => $extProduct->externalCode], ['order' => 'id ASC']);

        if ((null == $model))
            $model = \StoreCategory::model()->findByAttributes(['name' => $extProduct->name], ['order' => 'id ASC']);

        if ((null == $model))
            $model = \StoreCategory::model()->findByAttributes(['slug' => YText::translit($extProduct->name)],['order' => 'id ASC']);

        return $model ?: new \StoreCategory();
    }


    //для нового id

    public function getProduct($extProduct)
    {
        $model = \Product::model()->findByAttributes(['external_id' => $extProduct->externalCode]);

        if ((null == $model))
            $model = \Product::model()->findByAttributes(['external_id' => $extProduct->id]);

        if ((null == $model) && !empty($extProduct->article))
            $model = \Product::model()->findByAttributes(['sku' => $extProduct->article]);

        if ((null == $model))
            $model = \Product::model()->findByAttributes(['slug' => YText::translit($extProduct->name)]);

        if ((null == $model))
            $model = \Product::model()->findByAttributes(['name' => $extProduct->name]);


        return $model ?: new \Product();
    }


    public function getWarehouse($extId)
    {
        $model = \Warehouse::model()->findByAttributes(['external_id' => $extId]);
        return $model ?: new \Warehouse();
    }

    public function getProductRest($productId, $warehouseId)
    {
        $model = \ProductRest::model()->findByAttributes([
            'product_id' => $productId,
            'warehouse_id' => $warehouseId,
        ]);
        return $model ?: new \ProductRest();
    }
}