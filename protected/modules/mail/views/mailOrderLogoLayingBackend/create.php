<?php
/**
 * Отображение для create:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     https://yupe.ru
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('MailModule.mail', 'Заказы логотипов') => ['/mail/mailOrderLogoLayingBackend/index'],
    Yii::t('MailModule.mail', 'Добавление'),
];

$this->pageTitle = Yii::t('MailModule.mail', 'Заказы логотипов - добавление');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('MailModule.mail', 'Управление Заказами логотипов'), 'url' => ['/mail/mailOrderLogoLayingBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('MailModule.mail', 'Добавить Заказ логотипа'), 'url' => ['/mail/mailOrderLogoLayingBackend/create']],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('MailModule.mail', 'Заказы логотипов'); ?>
        <small><?=  Yii::t('MailModule.mail', 'добавление'); ?></small>
    </h1>
</div>

<?=  $this->renderPartial('_form', ['model' => $model]); ?>