<?php
/**
 * Отображение для index:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     https://yupe.ru
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('MailModule.mail', 'Заказы логотипов') => ['/mail/mailOrderLogoLayingBackend/index'],
    Yii::t('MailModule.mail', 'Управление'),
];

$this->pageTitle = Yii::t('MailModule.mail', 'Заказы логотипов - управление');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('MailModule.mail', 'Управление Заказами логотипов'), 'url' => ['/mail/mailOrderLogoLayingBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('MailModule.mail', 'Добавить Заказ логотипа'), 'url' => ['/mail/mailOrderLogoLayingBackend/create']],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('MailModule.mail', 'Заказы логотипов'); ?>
        <small><?=  Yii::t('MailModule.mail', 'управление'); ?></small>
    </h1>
</div>

<p>
    <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="collapse" data-target="#search-toggle">
        <i class="fa fa-search">&nbsp;</i>
        <?=  Yii::t('MailModule.mail', 'Поиск Заказов логотипов');?>
        <span class="caret">&nbsp;</span>
    </a>
</p>

<div id="search-toggle" class="collapse out search-form">
        <?php Yii::app()->clientScript->registerScript('search', "
        $('.search-form form').submit(function () {
            $.fn.yiiGridView.update('mail-order-logo-laying-grid', {
                data: $(this).serialize()
            });

            return false;
        });
    ");
    $this->renderPartial('_search', ['model' => $model]);
?>
</div>

<br/>

<p> <?=  Yii::t('MailModule.mail', 'В данном разделе представлены средства управления Заказами логотипов'); ?>
</p>

<?php
 $this->widget(
    'yupe\widgets\CustomGridView',
    [
        'id'           => 'mail-order-logo-laying-grid',
        'type'         => 'striped condensed',
        'dataProvider' => $model->search(),
        'filter'       => $model,
        'columns'      => [
            'id',
            [
                'name'  => 'product_id',
                'value' => function($data){
                    $categoryList = '<span class="label label-primary">'. (isset($data->product_id) ? $data->product->name : '---') . '</span>';

                    return $categoryList;
                },
                'type' => 'raw',
                'filter' => CHtml::activeDropDownList($model, 'product_id', $model->getProductList(), ['encode' => false, 'empty' => '', 'class' => 'form-control']),
                'htmlOptions' => ['width' => '220px'],
            ],
            'name',
            'phone',
            'email',
            // 'city',
            [
                'class' => 'yupe\widgets\EditableStatusColumn',
                'name' => 'status',
                'url' => $this->createUrl('/mail/mailOrderLogoLayingBackend/inline'),
                'source' => $model->getStatusList(),
                'options' => [
                    MailOrderLogoLaying::STATUS_PUBLIC => ['class' => 'label-success'],
                    MailOrderLogoLaying::STATUS_MODERATE => ['class' => 'label-default'],
                ],
            ],
//            'company_name',
//            'sku',
//            'product_name',
//            'tirazh',
//            'type_application',
//            'image',
//            'body',
//            'status',
            [
                'class' => 'yupe\widgets\CustomButtonColumn',
            ],
        ],
    ]
); ?>
