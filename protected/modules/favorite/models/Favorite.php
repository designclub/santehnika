<?php

/**
 * This is the model class for table "{{store_favorite}}".
 *
 * The followings are the available columns in table '{{store_favorite}}':
 * @property integer $id
 * @property integer $user_id
 * @property string $favorite_data
 *
 * The followings are the available model relations:
 * @property UserUser $user
 */
class Favorite extends yupe\models\YModel
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{store_favorite}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('favorite_data', 'required'),
			array('user_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user_id, favorite_data', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}
    
    public function beforeValidate(){
        if (Yii::app()->getUser()->isAuthenticated())
            $this->delFavoriteUser();
        
        return parent::beforeSave();
    }
    
    public function beforeSave()
    {
        if (Yii::app()->getUser()->isAuthenticated()) {
            $this->delFavoriteUser();
            $this->user_id = Yii::app()->getUser()->getId();
        }
        $this->favorite_data = json_encode($this->favorite_data);
        return parent::beforeSave();
    }
    
    public function afterFind()
    {
        $this->favorite_data = json_decode($this->favorite_data, true);
        parent::afterFind();
    }
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'favorite_data' => 'Favorite Data',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('favorite_data',$this->favorite_data,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function delFavoriteUser(){
        
        if (Yii::app()->getUser()->isAuthenticated()) {
            
            $transaction = Yii::app()->getDb()->beginTransaction();
            
            try {

                Yii::app()->getDb()->createCommand()
                    ->delete('{{store_favorite}}', 'user_id = :id', [':id' => Yii::app()->getUser()->getId()]);

                $transaction->commit();

                return true;
            } catch (Exception $e) {
                $transaction->rollback();

                return false;
            }
        }
    }
    
    public function getData(){
        if (Yii::app()->getUser()->isAuthenticated()) {
            $favorite = Favorite::model()->findByAttributes(['user_id' => Yii::app()->getUser()->getId()]);
            return $favorite->favorite_data;
        }
        return false;
    }
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Favorite the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
