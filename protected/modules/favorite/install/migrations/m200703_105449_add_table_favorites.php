<?php

class m200703_105449_add_table_favorites extends yupe\components\DbMigration
{
	public function safeUp()
	{
        $this->createTable(
            "{{store_favorite}}",
            [
                "id" => "pk",
                "user_id" => "integer null",
                "favorite_data" => "text not null",
            ],
            $this->getOptions()
        );
                
        $this->addForeignKey(
            "fk_{{store_favorite}}_user_id",
            '{{store_favorite}}',
            'user_id',
            '{{user_user}}',
            'id',
            'NO ACTION',
            'NO ACTION'
        );
        
	}

	public function safeDown()
	{
        $this->dropTable("{{store_favorite}}");
	}
}