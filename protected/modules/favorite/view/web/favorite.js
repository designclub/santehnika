/*добавление/удаление товаров в избранное*/
$(document).ready(function () {
    var svg_add = '<svg viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M8.95355 17.4465L10 18.4929L11.0464 17.4465L11.0573 17.4357L11.0687 17.4256C13.6771 15.1181 15.794 13.2207 17.2637 11.4326C18.7277 9.65139 19.5 8.03644 19.5 6.30005C19.5 3.47619 17.3239 1.30005 14.5 1.30005C12.9494 1.30005 11.3882 2.03347 10.3817 3.22302L10 3.67411L9.61831 3.22302C8.61176 2.03347 7.05055 1.30005 5.5 1.30005C2.67614 1.30005 0.5 3.47619 0.5 6.30005C0.5 8.03644 1.27229 9.65139 2.73627 11.4326C4.20595 13.2207 6.32289 15.1181 8.93129 17.4256L8.94274 17.4357L8.95355 17.4465Z"/></svg>';
    var svg_remove = '<svg viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M8.95355 17.4465L10 18.4929L11.0464 17.4465L11.0573 17.4357L11.0687 17.4256C13.6771 15.1181 15.794 13.2207 17.2637 11.4326C18.7277 9.65139 19.5 8.03644 19.5 6.30005C19.5 3.47619 17.3239 1.30005 14.5 1.30005C12.9494 1.30005 11.3882 2.03347 10.3817 3.22302L10 3.67411L9.61831 3.22302C8.61176 2.03347 7.05055 1.30005 5.5 1.30005C2.67614 1.30005 0.5 3.47619 0.5 6.30005C0.5 8.03644 1.27229 9.65139 2.73627 11.4326C4.20595 13.2207 6.32289 15.1181 8.93129 17.4256L8.94274 17.4357L8.95355 17.4465Z"/></svg>';
    // var svg_remove = '<svg viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M8.95355 17.4465L10 18.4929L11.0464 17.4465L11.0573 17.4357L11.0687 17.4255C13.6771 15.1181 15.794 13.2207 17.2637 11.4326C18.7277 9.65139 19.5 8.03644 19.5 6.30005C19.5 3.47619 17.3239 1.30005 14.5 1.30005C12.9494 1.30005 11.3882 2.03347 10.3817 3.22302L10 3.67411L9.61831 3.22302C8.61176 2.03347 7.05055 1.30005 5.5 1.30005C2.67614 1.30005 0.5 3.47619 0.5 6.30005C0.5 8.03644 1.27229 9.65139 2.73627 11.4326C4.20596 13.2207 6.32289 15.1181 8.93129 17.4255L8.94274 17.4357L8.95355 17.4465Z" /></svg>';
    $(document).on('click', '.yupe-store-favorite-add', function (event) {
        event.preventDefault();
        var $this = $(this);
        var product = parseInt($this.data('id'));
        var data = {'id': product};
        data[yupeTokenName] = yupeToken;
        $.post(yupeStoreAddFavoriteUrl, data, function (data) {
            if (data.result) {
                $('.js-yupe-store-favorite-total').html(data.count).addClass('active');
                $this.removeClass('yupe-store-favorite-add').addClass('yupe-store-favorite-remove').parent().addClass('active');
                // $this.html(svg_remove);
                // moveFavoriteProduct($this);
            }
            showNotify($this, data.result ? 'success' : 'danger', data.data);
        }, 'json');
    });

    $(document).on('click', '.yupe-store-favorite-remove', function (event) {
        event.preventDefault();
        var $this = $(this);
        var product = parseInt($this.data('id'));
        var data = {'id': product};
        data[yupeTokenName] = yupeToken;
        $.post(yupeStoreRemoveFavoriteUrl, data, function (data) {
            if (data.result) {
                $('.js-yupe-store-favorite-total').html(data.count);
                $this.removeClass('yupe-store-favorite-remove').addClass('yupe-store-favorite-add').parent().removeClass('active');
                // $this.html(svg_add);
                if(data.count == 0){
                    $('.js-yupe-store-favorite-total').removeClass('active');
                }
            }
            showNotify($this, data.result ? 'success' : 'danger', data.data);
        }, 'json');
    });

    $(document).on('click', '.yupe-product-favorite-add', function (event) {
        event.preventDefault();
        var $this = $(this);
        
        var span = $(this).find('span');
        var text = $(this).data('text');

        var product = parseInt($this.data('id'));
        var data = {'id': product};
        data[yupeTokenName] = yupeToken;
        $.post(yupeStoreAddFavoriteUrl, data, function (data) {
            if (data.result) {
                $('.js-yupe-store-favorite-total').html(data.count).addClass('active');
                $this.removeClass('yupe-product-favorite-add').addClass('yupe-product-favorite-remove').parent().addClass('active');
                // $this.html(svg_remove);
                $this.data('text', span.text());
                span.text(text);
                // moveFavoriteProduct($this);
            }
            showNotify($this, data.result ? 'success' : 'danger', data.data);
        }, 'json');
    });

    $(document).on('click', '.yupe-product-favorite-remove', function (event) {
        event.preventDefault();
        var $this = $(this);

        var span = $(this).find('span');
        var text = $(this).data('text');
        
        var product = parseInt($this.data('id'));
        var data = {'id': product};
        data[yupeTokenName] = yupeToken;
        $.post(yupeStoreRemoveFavoriteUrl, data, function (data) {
            if (data.result) {
                $('.js-yupe-store-favorite-total').html(data.count);
                $this.removeClass('yupe-product-favorite-remove').addClass('yupe-product-favorite-add').parent().removeClass('active');
                // $this.html(svg_add);

                $this.data('text', span.text());
                span.text(text);
                if(data.count == 0){
                    $('.js-yupe-store-favorite-total').removeClass('active');
                }
            }
            showNotify($this, data.result ? 'success' : 'danger', data.data);
        }, 'json');
    });

    $(document).on('click', '.favorite-delete', function (event) {
        event.preventDefault();
        var $this = $(this);
        var product = parseInt($this.data('id'));
        var data = {'id': product};
        data[yupeTokenName] = yupeToken;
        $.post(yupeStoreRemoveFavoriteUrl, data, function (data) {
            if (data.result) {
                $('.js-yupe-store-favorite-total').html(data.count);
                $this.parents('.product-box__item').remove();
                if(data.count == 0){
                    $('.js-yupe-store-favorite-total').removeClass('active');
                    $(".list-view .favorite-box").html('<span class="empty">Нет результатов.</span>');
                }
            }
            showNotify($this, data.result ? 'success' : 'danger', data.data);
        }, 'json');
    });

    function moveFavoriteProduct(elem){
        var product = elem.parents('.js-product-item').find(".js-product-image");
        var leftCart = $(".header .js-yupe-store-favorite-total").offset().left; 
        var topCart = $(".header .js-yupe-store-favorite-total").offset().top;

        if($(".header-fix-content").hasClass('active')){
            leftCart = $(".header-fix .js-yupe-store-favorite-total").offset().left;
            topCart = $(".header-fix .js-yupe-store-favorite-total").offset().top;
        }

        if(window.innerWidth > 640){
            $(product).clone().css({
                'position' : 'absolute', 
                'z-index' : '9999', 
                'opacity' : '0.5', 
                top: $(product).offset().top, 
                left:$(product).offset().left,
                width: 240
            }).appendTo("body").animate({
                opacity: 0.1,
                left: leftCart,
                top: topCart,
                width: 20}, 1000, function() {
                $(this).remove();
            });
        }
    }
});