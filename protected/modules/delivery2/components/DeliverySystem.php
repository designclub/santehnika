<?php

/**
 * Class DeliverySystem
 */
class DeliverySystem extends CApplicationComponent
{
    /**
     *
     */
    const LOG_CATEGORY = 'store.delivery';

    /**
     * @var string
     */
    public $parametersFile = 'parameters.json';

    /**
     * @param Delivery $delivery
     * @param Order $order
     * @param bool|false $return
     * @throws CException
     */
    public function renderCheckoutForm(Delivery $delivery, Order $order, $return = false)
    {
        return null;
    }

    /**
     * @param Delivery $delivery
     * @param CHttpRequest $request
     * @throws CException
     */
    public function processCheckout(Delivery $delivery, CHttpRequest $request)
    {
        return null;
    }

    /**
     * @return mixed
     */
    public function getParameters()
    {
        $class_info = new ReflectionClass($this);
        $params = json_decode(
            file_get_contents(dirname($class_info->getFileName()).DIRECTORY_SEPARATOR.$this->parametersFile),
            true
        );

        return $params;
    }

    /**
     * @param array $deliverySettings
     * @param bool|false $return
     * @return string
     */
    public function renderSettings($deliverySettings = [], $return = false)
    {
        $params = $this->getParameters();
        $settings = '';
        foreach ((array)$params['settings'] as $param) {
            $variable = $param['variable'];
            $settings .= CHtml::openTag('div', ['class' => 'form-group']);
            $settings .= CHtml::label($param['name'], 'Delivery_settings_'.$variable, ['class' => 'control-label']);
            $value = isset($deliverySettings[$variable]) ? $deliverySettings[$variable] : null;
            if (isset($param['options'])) {
                $settings .= CHtml::dropDownList(
                    'DeliverySettings['.$variable.']',
                    $value,
                    CHtml::listData($param['options'], 'value', 'name'),
                    ['class' => 'form-control']
                );
            } elseif (isset($param['options_list'])) {
                $settings .= CHtml::checkBoxList(
                    'DeliverySettings['.$variable.']',
                    $value,
                    CHtml::listData($param['options_list'], 'value', 'name'),
                    [
                        'class' => '',
                        'template' => '<div class="checkbox">{input} {label}</div>',
                        'separator' => '',
                    ]
                );
            } else {
                $settings .= CHtml::textField('DeliverySettings['.$variable.']', $value, ['class' => 'form-control']);
            }
            $settings .= CHtml::closeTag('div');
        }
        if ($return) {
            return $settings;
        } else {
            echo $settings;
        }
    }
}
