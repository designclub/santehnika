<?php

/**
 * PriceOverview
 */
class PriceOverview extends yupe\widgets\YWidget
{
    public $isButton = false;
    public $view = 'price-overview';
    public $order = null;

    public function run()
    {
        $cart = Yii::app()->cart;
        $positions = $cart->getPositions();
        $itemsCount = $cart->getItemsCount();

        if ($this->order) {
            $this->view = 'price-overview-last';
            $positions = $this->order->products;
            $itemsCount = $this->getItemsCount($positions);
        }

        $orderModule = Yii::app()->getModule('order');

        $coupons = [];

        if (Yii::app()->hasModule('coupon')) {
            $couponCodes = Yii::app()->cart->couponManager->coupons;

            foreach ($couponCodes as $code) {
                $coupons[] = Coupon::model()->getCouponByCode($code);
            }
        }

        $this->render($this->view, [
            'cart'        => $cart,
            'positions'   => $positions,
//            'discountSumm' => $this->getDiscountSumm($positions),
            'orderModule' => $orderModule,
            'isButton'    => $this->isButton,
            'coupons'     => $coupons,
            'itemsCount'  => $itemsCount,
            'order'  => $this->order,
        ]);
    }

    public function getDiscountSumm($positions)
    {
        $summ = 0;
        foreach ($positions as $position) {
        }

        return $summ;
    }

    public function getItemsCount($positions)
    {
        $count = 0;
        foreach ($positions as $position) {
            $count += $position->quantity;
        }

        return $count;
    }
}
