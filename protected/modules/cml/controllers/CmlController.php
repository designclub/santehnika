<?php
use yupe\components\controllers\FrontController;
use yupe\models\Settings;

class CmlController extends FrontController
{
    /** @var Cml */
    private $cml;

    public function filters()
    {
        return [
            [
                'application.modules.cml.components.filters.CmlAuthFilter',
            ]
        ];
    }
    public function actionIndex()
    {
            $module = Yii::app()->getModule('cml');
            $request = Yii::app()->getRequest();
            $repository = new CmlRepository();
            $event = Yii::app()->eventManager;
            $this->cml = new CmlCatalog($request, $module, $repository, $event);
            $this->parseFile('import.xml');
            $this->parseFile('offers.xml');
    }
    /**
     * Main action
     *
     * @param null $type
     * @param null $mode
     * @param null $filename
     */
    public function actionSync($type = null, $mode = null, $filename = null)
    {
        gc_enable();
        set_time_limit(0);

        if (is_null($type) && is_null($mode)) {
            echo CmlHelper::message(Yii::t('CmlModule.cml', 'message.wrong_request'), CmlHelper::MESSAGE_ERROR);
            Yii::app()->end();
        }

        $module = Yii::app()->getModule('cml');
        $request = Yii::app()->getRequest();
        $repository = new CmlRepository();
        $event = Yii::app()->eventManager;
        $parseAfterSave = false;

        switch ($type) {
            case 'catalog':
                $this->cml = new CmlCatalog($request, $module, $repository, $event);
                break;
            case 'sale':
                $parseAfterSave = true;
                $this->cml = new CmlSale($request, $module, $repository, $event);
                break;
            default:
                echo CmlHelper::message(Yii::t('CmlModule.cml', 'message.empty'), CmlHelper::MESSAGE_SUCCESS);
                Yii::app()->end();
        }

        switch ($mode) {
            case 'checkauth':
                $this->checkAuth();
                break;
            case 'init':
                $this->syncInit();
                break;
            case 'file':
                $this->saveFile($filename, $parseAfterSave);
                break;
            case 'import':
                $this->parseFile($filename);
                break;
            case 'query':
                $this->saleQuery($repository);
                break;
            case 'success':
                $this->saleSuccess();
                break;
            default:
                echo CmlHelper::message(Yii::t('CmlModule.cml', 'message.empty'), CmlHelper::MESSAGE_SUCCESS);
        }
    }

    /**
     * Check client IP
     */
    private function checkAuth()
    {
        if ($this->cml->checkIp()) {
            echo CmlHelper::message(implode(PHP_EOL, ['key', $this->cml->getKey()]), CmlHelper::MESSAGE_SUCCESS);
        } else {
            echo CmlHelper::message(Yii::t('CmlModule.cml', 'message.wrong_ip'), CmlHelper::MESSAGE_ERROR);
        }
    }

    /**
     * Set sync options
     */
    private function syncInit()
    {
        $this->checkKey();

        $data = [
            'zip=no',
            'file_limit=' . $this->cml->getFileSizeLimit(),
        ];

        $this->cml->deleteAll();

        echo CmlHelper::message(implode(PHP_EOL, $data));
    }

    /**
     * Save submitted file
     *
     * @param $filename
     * @param bool $parseAfterSave
     */
    private function saveFile($filename, $parseAfterSave = false)
    {
        $this->checkKey();

        $result = $this->cml->save($filename);

        if ($result === false || (isset($_SERVER['CONTENT_LENGTH']) && $result != $_SERVER['CONTENT_LENGTH'])) {
            echo CmlHelper::message(Yii::t('CmlModule.cml', 'message.wrong_file_size'), CmlHelper::MESSAGE_ERROR);
            Yii::app()->end();
        }

        if ($parseAfterSave) {
            $this->cml->parse(new XMLReader(), $filename);
        }

        echo CmlHelper::message(Yii::t('CmlModule.cml', 'message.empty'), CmlHelper::MESSAGE_SUCCESS);
    }

    /**
     * Parse submitted file
     *
     * @param $filename
     */
    private function parseFile($filename)
    {
//        echo CmlHelper::message(Yii::t('CmlModule.cml', 'message.empty'), CmlHelper::MESSAGE_SUCCESS);


        $this->checkKey();

        if (!$this->cml->isExist($filename)) {
            echo CmlHelper::message(Yii::t('CmlModule.cml', 'message.file_doesnt_exist'), CmlHelper::MESSAGE_ERROR);
            Yii::app()->end();
        }

        $this->cml->parse(new XMLReader(), $filename);
        
        echo CmlHelper::message(Yii::t('CmlModule.cml', 'message.empty'), CmlHelper::MESSAGE_SUCCESS);
    }

    /**
     * Checks the secret key is valid
     *
     * @return bool
     */
    private function checkKey()
    {

        return true;
//
//        if (isset($_COOKIE['key']) && $this->cml->isKeyValid($_COOKIE['key'])) return true;
//
//        echo CmlHelper::message(Yii::t('CmlModule.cml', 'message.wrong_key'), CmlHelper::MESSAGE_ERROR);
//        Yii::app()->end();
    }

    /**
     * Shows orders xml
     *
     * @param CmlRepository $repository
     */
    private function saleQuery(CmlRepository $repository)
    {
        $this->checkKey();

        Settings::model()->saveModuleSettings('cml', ['queryTime' => date('Y-m-d H:i:s', time())]);

        $this->renderPartial('sale', [
            'orders' => $repository->getOrderList(),
        ]);
    }

    /**
     * Save last successful sync time
     *
     * @return bool
     */
    private function saleSuccess()
    {
        $this->checkKey();

        return Settings::model()->saveModuleSettings('cml', ['successTime' => date('Y-m-d H:i:s', time())]);
    }
}