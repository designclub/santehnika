<?php
use yupe\components\WebModule;

class CmlModule extends WebModule
{
    const VERSION = '1.0-pro';

    const ACTION_NOTHING = 0;
    const ACTION_DEACTIVATE = 1;
    const ACTION_DELETE = 2;

    const NO = 0;
    const YES = 1;

    public $allowedUserName;
    public $allowedUserPass;
    public $allowedIP;
    public $fileSizeLimit = 1024000;
    public $secretKey = 'buEXZFKaqw64zPrD';
    public $uploadPath = 'cml';
    public $categorySync = self::YES;
    public $categoryNotInList = self::ACTION_DEACTIVATE;
    public $productSync = self::YES;
    public $productNotInList = self::ACTION_DEACTIVATE;
    public $offerSync = self::YES;
    public $updateQuantity = self::YES;
    public $updatePrice = self::YES;
    public $priceId;
    public $discountPriceId;
    public $quantityChangeStatus = self::YES;
    public $queryTime = null;
    public $successTime = null;
    public $markDeleted = self::ACTION_DEACTIVATE;
    public $defaultOrderStatus = OrderStatus::STATUS_NEW;

    public $storageSync = self::YES;

    /**
     * @return array
     */
    public function getDependencies()
    {
        return [
            'store',
            'order',
        ];
    }

    /**
     * @return array
     */
    public function getParamsLabels()
    {
        return [
            'allowedUserName' => Yii::t('CmlModule.cml', 'label.username'),
            'allowedUserPass' => Yii::t('CmlModule.cml', 'label.password'),
            'allowedIP' => Yii::t('CmlModule.cml', 'label.ip'),
            'fileSizeLimit' => Yii::t('CmlModule.cml', 'label.file_size'),
            'secretKey' => Yii::t('CmlModule.cml', 'label.key'),
            'categorySync' => Yii::t('CmlModule.cml', 'label.category_sync'),
            'storageSync' => Yii::t('CmlModule.cml', 'Синхронизировать склады'),
            'categoryNotInList' => Yii::t('CmlModule.cml', 'label.category_not_in_list'),
            'productSync' => Yii::t('CmlModule.cml', 'label.product_sync'),
            'productNotInList' => Yii::t('CmlModule.cml', 'label.product_not_in_list'),
            'offerSync' => Yii::t('CmlModule.cml', 'label.offer_sync'),
            'updateQuantity' => Yii::t('CmlModule.cml', 'label.update_quantity'),
            'updatePrice' => Yii::t('CmlModule.cml', 'label.update_price'),
            'priceId' => Yii::t('CmlModule.cml', 'label.price_id'),
            'discountPriceId' => Yii::t('CmlModule.cml', 'label.discount_price_id'),
            'quantityChangeStatus' => Yii::t('CmlModule.cml', 'label.quantity_change_status'),
            'markDeleted' => Yii::t('CmlModule.cml', 'label.mark_deleted'),
            'defaultOrderStatus' => Yii::t('CmlModule.cml', 'label.default_order_status'),
        ];
    }

    /**
     * @return array
     */
    public function getEditableParams()
    {
        return [
            'allowedUserName',
            'allowedUserPass',
            'allowedIP',
            'fileSizeLimit',
            'secretKey',
            'storageSync' => $this->getYNList(),
            'categorySync' => $this->getYNList(),
            'categoryNotInList' => $this->getNotInList(),
            'productSync' => $this->getYNList(),
            'productNotInList' => $this->getNotInList(),
            'offerSync' => $this->getYNList(),
            'updateQuantity' => $this->getYNList(),
            'updatePrice' => $this->getYNList(),
            'priceId',
            'discountPriceId',
            'quantityChangeStatus' => $this->getYNList(),
            'markDeleted' => $this->getNotInList(),
            'defaultOrderStatus' => $this->getStatusList(),
        ];
    }

    /**
     * @return array
     */
    public function getEditableParamsGroups()
    {
        return [
            '0.common' => [
                'label' => Yii::t('CmlModule.cml', 'title.settings'),
                'items' => [
                    'fileSizeLimit',
                ]
            ],
            '1.main' => [
                'label' => Yii::t('CmlModule.cml', 'title.user_data'),
                'items' => [
                    'allowedUserName',
                    'allowedUserPass',
                    'secretKey',
                    'allowedIP',
                ]
            ],
            '2.offers' => [
                'label' => Yii::t('CmlModule.cml', 'title.offers'),
                'items' => [
                    'storageSync',
                    'categorySync',
                    'categoryNotInList',
                    'productSync',
                    'productNotInList',
                    'offerSync',
                    'updateQuantity',
                    'quantityChangeStatus',
                    'updatePrice',
                    'priceId',
                    'discountPriceId',
                ]
            ],
            '3.orders' => [
                'label' => Yii::t('CmlModule.cml', 'title.orders'),
                'items' => [
                    'markDeleted',
                    'defaultOrderStatus',
                ]
            ],
        ];
    }

    /**
     * @return bool
     */
    public function getInstall()
    {
        if (parent::getInstall()) {
            @mkdir($this->getUploadPath(), 0755);
        }

        return true;
    }

    /**
     * @return string
     */
    public function getVersion()
    {
        return self::VERSION;
    }

    /**
     * @return string
     */
    public function getCategory()
    {
        return Yii::t('CmlModule.cml', 'title.category');
    }

    /**
     * @return string
     */
    public function getName()
    {
        return Yii::t('CmlModule.cml', 'CommerceML');
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return Yii::t('CmlModule.cml', 'title.module');
    }

    /**
     * @return string
     */
    public function getAuthor()
    {
        return Yii::t('CmlModule.cml', 'Oleg Filimonov');
    }

    /**
     * @return string
     */
    public function getAuthorEmail()
    {
        return Yii::t('CmlModule.cml', 'olegsabian@gmail.com');
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return Yii::t('CmlModule.cml', 'http://yupe.ru');
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return "fa fa-fw fa-refresh";
    }

    /**
     * @return array
     */
    public function getAdminPageLink()
    {
        return ['/yupe/backend/modulesettings', 'module' => 'cml'];
    }

    /**
     * @return array|null
     */
    public function getAllowedIpList()
    {
        return empty($this->allowedIP) ? null : explode(',', $this->allowedIP);
    }

    /**
     * @return string
     */
    public function getUploadPath()
    {
        return Yii::getPathOfAlias('webroot') .
            DIRECTORY_SEPARATOR .
            Yii::app()->getModule('yupe')->uploadPath .
            DIRECTORY_SEPARATOR .
            $this->uploadPath;
    }

    /**
     * @return array
     */
    public function getYNList()
    {
        return [
            self::YES => Yii::t('CmlModule.cml', 'select.yes'),
            self::NO => Yii::t('CmlModule.cml', 'select.no'),
        ];
    }

    /**
     * @return array
     */
    public function getNotInList()
    {
        return [
            self::ACTION_NOTHING => Yii::t('CmlModule.cml', 'select.nothing'),
            self::ACTION_DEACTIVATE => Yii::t('CmlModule.cml', 'select.deactivate'),
            self::ACTION_DELETE => Yii::t('CmlModule.cml', 'select.delete'),
        ];
    }

    /**
     * @return array
     */
    public function getStatusList()
    {
        return CHtml::listData(OrderStatus::model()->findAll(), 'id', 'name');
    }
}