<?php
return [
    'module' => [
        'class' => 'application.modules.cml.CmlModule',
    ],
    'import' => [
        'application.modules.cml.components.*',
        'application.modules.cml.components.events.*',
        'application.modules.cml.components.listeners.*',
        'application.modules.cml.components.repository.*',
    ],
    'component' => [
        'request' => [
            'noCsrfValidationRoutes' => ['cml'],
        ],
        'eventManager' => [
            'class' => 'yupe\components\EventManager',
            'events' => [
                'cml.category.after.import' => [
                    ['\CmlCategoryListener', 'onAfterImport']
                ],
                'cml.product.after.import' => [
                    ['\CmlProductListener', 'onAfterImport']
                ],
                'cml.order.mark.deleted' => [
                    ['\CmlOrderListener', 'onMarkDeleted']
                ],
                'cml.order.change.status' => [
                    ['\CmlOrderListener', 'onChangeStatus']
                ],
            ]
        ],
    ],
    'rules' => [
        '/cml/manual' => 'cml/cml/index',
        '/cml/sync' => 'cml/cml/sync',
    ],
];