<?php
return [
    'label.category_not_in_list' => 'Что делать с категориями, отсутствующими в файле импорта',
    'label.category_sync' => 'Загружать категории',
    'label.default_order_status' => 'Если статус не найден, то отметить как',
    'label.discount_price_id' => '"Ид" скидочной цены продукта',
    'label.file_size' => 'Максимальный размер загружаемого файла (байт)',
    'label.ip' => 'Список доверенных IP адресов через запятую (Оставьте пустым, чтобы разрешить всем)',
    'label.key' => 'Секретный ключ для авторизации запросов',
    'label.mark_deleted' => 'Что делать с удаленными заказами',
    'label.offer_sync' => 'Загружать остатки',
    'label.password' => 'Пароль',
    'label.price_id' => '"Ид" цены продукта',
    'label.product_not_in_list' => 'Что делать с товарами, отсутствующими в файле импорта',
    'label.product_sync' => 'Загружать товары',
    'label.quantity_change_status' => 'Статус "Нет в наличии" если количество = 0',
    'label.update_price' => 'Обновлять цены',
    'label.update_quantity' => 'Обновлять количество товара',
    'label.username' => 'Имя пользователя',
    'message.empty' => '0',
    'message.file_doesnt_exist' => 'Запрашиваемый файл не найден',
    'message.find_product_no_result{extId}{sku}' => 'Товар с внешним кодом {extId} или артикулом {sku} не найден в базе данных',
    'message.order_not_found{id}' => 'Заказ №{id} не найден',
    'message.save_category_error' => 'Произошла ошибка при попытке сохранить категорию',
    'message.save_product_error' => 'Произошла ошибка при попытке сохранить товар',
    'message.wrong_file_size' => 'Произошла ошибка при передаче файлов',
    'message.wrong_ip' => 'Доступ закрыт для вашего IP адреса',
    'message.wrong_key' => 'Передан неправильный ключ',
    'message.wrong_request' => 'Неправильный запрос',
    'select.deactivate' => 'Деактивировать',
    'select.delete' => 'Удалить',
    'select.no' => 'Нет',
    'select.nothing' => 'Ничего',
    'select.yes' => 'Да',
    'title.category' => 'Магазин',
    'title.module' => 'Модуль для обмена данными в формате CommerceML с программами складского учета (1С, МойСклад и т.д.)',
    'title.offers' => 'Синхронизация товаров и остатков',
    'title.orders' => 'Синхронизация заказов',
    'title.settings' => 'Общие настройки',
    'title.user_data' => 'Данные для доступа к синхронизации',
];