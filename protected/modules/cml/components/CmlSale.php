<?php
class CmlSale extends Cml
{
    /**
     * Parse orders xml file
     *
     * @param XMLReader $reader
     * @param $filename
     * @return bool
     */
    public function parse(XMLReader $reader, $filename)
    {
        $reader->open($this->getPath($filename));

        while ($reader->read() && $reader->name !== 'Документ') ;

        while ($reader->name === 'Документ') {
            $markDeleted = false;
            $data = simplexml_load_string($reader->readOuterXml());
            $order = $this->repository->getOrder(intval($data->Ид));

            if (is_null($order)) {
                Yii::log(Yii::t('CmlModule.cml', 'message.order_not_found{id}', ['id' => intval($data->Ид)]), CLogger::LEVEL_WARNING);

                $reader->next('Документ');

                continue;
            }

            foreach ($data->ЗначенияРеквизитов->ЗначениеРеквизита as $item) {
                switch ($item->Наименование) {
                    case 'ПометкаУдаления':
                        if (strval($item->Значение) === 'false') continue;

                        $markDeleted = true;
                        $this->event->fire(CmlEvents::ORDER_MARK_DELETED, new CmlOrderEvent($order));

                        break;

                    case 'Статус заказа':
                        if ($markDeleted) continue;

                        $this->event->fire(CmlEvents::ORDER_CHANGE_STATUS, new CmlOrderEvent($order, strval($item->Значение)));

                        break;
                }
            }

            $reader->next('Документ');

            gc_collect_cycles();
        }

        $reader->close();

        $this->delete($filename);

        return true;
    }
}