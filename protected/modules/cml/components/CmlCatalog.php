<?php

use yupe\helpers\YText;

class CmlCatalog extends Cml
{
    /**
     * XML file reader
     * @var XMLReader
     */
    private $reader;

    /**
     * Current file name
     * @var string
     */
    private $filename;

    /**
     * IDs list of successfully processed categories.
     * @var array
     */
    private $categories = [];

    /**
     * IDs list of successfully processed products.
     * @var array
     */
    private $products = [];

    /**
     * Controls the import of products and categories.
     *
     * @param XMLReader $reader
     * @param $filename
     */
    public function parse(XMLReader $reader, $filename)
    {
        $this->reader = $reader;
        $this->filename = $filename;

        switch ($filename) {
            case 'import.xml':

                if ($this->module->categorySync == CmlModule::YES) {
                    $this->category();
                }

                if ($this->module->productSync == CmlModule::YES) {
                    $this->product();
                }

//                $this->delete($filename);

                break;

            case 'offers.xml':
                $this->storage();
                $this->offers();
                break;
        }
    }

    /**
     * Returns IDs list of successfully processed categories
     *
     * @return array
     */
    public function getCategoryList()
    {
        return $this->categories;
    }

    /**
     * Returns IDs list of successfully processed products
     *
     * @return array
     */
    public function getProductList()
    {
        return $this->products;
    }

    /**
     * Import categories
     *
     * @return bool
     */
    private function category()
    {
        $this->reader->open($this->getPath($this->filename));

        while ($this->reader->read() && $this->reader->name !== 'Группа') ;
        while ($this->reader->name === 'Группа') {
            $this->saveCategory(simplexml_load_string($this->reader->readOuterXml()));
            $this->reader->next('Группа');

            gc_collect_cycles();
        }

        $this->event->fire(CmlEvents::CATEGORY_IMPORT, new CmlCatalogEvent($this));

        $this->reader->close();

        return true;
    }

    /**
     * Recursively insert or update categories.
     *
     * @param SimpleXMLElement $group
     * @param null $parent
     * @return bool
     */
    private function saveCategory(SimpleXMLElement $group, $parent = null)
    {
        $array = ['Доватора', 'САНТЕХНИКА', 'САНТЕХНИКА174'];
        $id = strval($group->Ид);
        $name = CHtml::encode(strval($group->Наименование));


        $category = $this->repository->getCategory($id);

        $category->parent_id = $parent;
        $category->name_short = $name;
        $category->name = $name;
        $category->slug = YText::translit($name);
        $category->external_id = $id;
        $category->status = StoreCategory::STATUS_PUBLISHED;

        if (!$category->validate(['slug'])) {
            $category->slug = sprintf('%s_%s', $category->slug, time());
        }
        if(array_search($name, $array) === false){
            if (!$category->save()) {
                Yii::log(Yii::t('CmlModule.cml', 'message.save_category_error'), CLogger::LEVEL_WARNING);

                return false;
            }
        }

        $this->categories[$id] = $category->id;

        if (isset($group->Группы)) {
            foreach ($group->Группы->Группа as $item) {
                $this->saveCategory($item, $category->id);
            }
        }

        return true;
    }

    /**
     * Get category id by its external ID
     *
     * @param integer|null $extId
     * @return integer|null
     */
    private function getCategoryId($extId = null)
    {
        $storeCategory = StoreCategory::model()->findByAttributes(array('external_id' => $extId));

        if (!is_null($storeCategory)) {
            return $storeCategory->id;
        }

        return null;

//        return (!is_null($extId) || array_key_exists($extId, $this->categories)) ? $this->categories[$extId] : null;
    }

    /**
     * Import products
     */
    private function product()
    {
        $this->reader->open($this->getPath($this->filename));

        while ($this->reader->read() && $this->reader->name !== 'Товар') ;

        while ($this->reader->name === 'Товар') {
            $product = simplexml_load_string($this->reader->readOuterXml());
            $this->saveProduct($product);

            $this->reader->next('Товар');

            gc_collect_cycles();
        }

        $this->event->fire(CmlEvents::PRODUCT_IMPORT, new CmlCatalogEvent($this));

        $this->reader->close();

        unset($this->categories);
        unset($this->products);
    }

    /**
     * Insert or update product
     *
     * @param SimpleXMLElement $product
     * @return bool
     */
    private function saveProduct(SimpleXMLElement $product)
    {

        //Собираем константы продукта
        $externalId = strval($product->Ид);
        $name = CHtml::encode(strval($product->Наименование));
        $sku = isset($product->Артикул) ? strval($product->Артикул) : null;
        $categoryExtId = isset($product->Группы) ? strval($product->Группы->Ид) : null;

        // Ищем товар по коду $externalId
        $model = $this->repository->getProduct($externalId, $sku); // для первой выгрузки надо сделать проверку по слагу
//        $model = $this->repository->getProductBySlug(YText::translit($name)); // для первой выгрузки надо сделать проверку по слагу

        $model->name = $name;
        $model->external_id = $externalId;
        $model->status = Product::STATUS_ACTIVE;
        
        if ($sku) {
            $model->sku = $sku;
        }
        if ($categoryExtId) {
            $model->category_id = $this->getCategoryId($categoryExtId); // ищем категорию по экстернел ид
        }

        if ($model->getIsNewRecord()) {
            $model->slug = YText::translit($name);
            $model->in_stock = Product::STATUS_NOT_IN_STOCK;
        }

        if (!$model->validate(['slug'])) {
            file_put_contents(Yii::getPathOfAlias('application') . '/runtime/double_products.log', '----' . PHP_EOL . date("d.m.y H:i:s") . PHP_EOL . 'name: ' . $model->name . PHP_EOL . 'ext_id: ' . $model->external_id . PHP_EOL, FILE_APPEND | LOCK_EX);
            $model->slug = sprintf('%s_%s', $model->slug, $externalId);
        }

        if (isset($product->ЗначенияРеквизитов)) {
            $desc = '';
            foreach ($product->ЗначенияРеквизитов->ЗначениеРеквизита as $item) {
                if ($item->Наименование == 'Полное наименование') {
                    $desc = CHtml::encode(strval($item->Значение));
                }
            }

            $model->description = $desc;
        }

        if (isset($product->Картинка)) {
            $model->image = $product->Картинка;
        }

        if (!$model->meta_title) {
            // $model->meta_title = 'Купить оптом ' . $name . ' с доставкой по России';
        }
        if (!$model->meta_description) {
            // $model->meta_description = 'Оптовая продажа цветов ' . $name . '. Выгодные условия сотрудничества. ☏ 8-800-555-07-87';
        }
        if (!$model->meta_keywords) {
            // $model->meta_keywords = $name . ', оптом, купить, цветы от поставщика, доставка';
        }

        /*if (isset($height)) {
            $model->height = $height;
        }
        if (isset($multiplicity)) {
            $model->multiplicity = $multiplicity;
        }*/

        if (!$model->save()) {
            Yii::log('$externalId: ' . $externalId . ' | ' . '$sku: ' . $sku . ' | ' . Yii::t('CmlModule.cml', 'message.save_product_error') . json_encode($model->getErrors(), JSON_UNESCAPED_UNICODE), CLogger::LEVEL_WARNING);

            return false;
        }


        $this->products[] = $model->id;

        return true;
    }


    /**
     * Import storage
     *
     * @return bool
     */
    private function storage()
    {
        $this->reader->open($this->getPath($this->filename));

        while ($this->reader->read() && $this->reader->name !== 'Склад') ;

        while ($this->reader->name === 'Склад') {

            $this->saveStorage(simplexml_load_string($this->reader->readOuterXml()));

            $this->reader->next('Склад');

            gc_collect_cycles();
        }

        $this->event->fire(CmlEvents::STORAGE_IMPORT, new CmlCatalogEvent($this));

        $this->reader->close();

        return true;
    }

    private function saveStorage(SimpleXMLElement $storage)
    {
        $externalId = strval($storage->Ид);

        $name = CHtml::encode(strval($storage->Наименование));
        $model = $this->repository->getStorage($externalId);

        file_put_contents('log', var_export('name = '. $name . '|', true), FILE_APPEND);

        $model->name = $name;
        $model->external_id = $externalId;

        Yii::log('Склад | name ' . $model->name . ' | external_id ' . $model->external_id, CLogger::LEVEL_WARNING);


        if (!$model->save()) {


            return false;
        }

        return true;
    }


    private function saveProductCount(Product $model, $count)
    {

        $storageId = $this->repository->getStorage((string)$count->attributes()->ИдСклада)->id; //получаем id склада

        $storageCount = $this->repository->getStorageRecord($storageId, $model->id);

        $storageCount->storage_id = $storageId;
        $storageCount->product_id = $model->id;
        $storageCount->quantity = intval((string)$count->attributes()->КоличествоНаСкладе);

        if (!$storageCount->save()) {
            return true;
        }

        return true;

    }


    /**
     * Import product offers
     */
    private function offers()
    {
        $this->reader->open($this->getPath($this->filename));

        while ($this->reader->read() && $this->reader->name !== 'Предложение') ;

        while ($this->reader->name === 'Предложение') {
            $offer = simplexml_load_string($this->reader->readOuterXml());

            $externalId = strval($offer->Ид);
            $sku = isset($offer->Артикул) ? strval($offer->Артикул) : null;
            $quantity = intval($offer->Количество);

            $product = $this->repository->getProduct($externalId, $sku, false);

            if (is_null($product)) {
                Yii::log(Yii::t('CmlModule.cml', 'message.find_product_no_result{extId}{sku}', [
                    'extId' => $externalId,
                    'sku' => $sku,
                ]));

                $this->reader->next('Предложение');

                continue;
            }


            if ($sku) {
                $product->sku = $sku;
            }
            $product->external_id = $externalId;


            if ($this->module->updateQuantity == 1) {
                $product->quantity = $quantity;
                $product->in_stock = Product::STATUS_IN_STOCK;

                if ($quantity <= 0 && $this->module->quantityChangeStatus == 1) {
                    $product->in_stock = Product::STATUS_NOT_IN_STOCK;
                }

                if (isset($offer->Склад)) {

                    foreach ($offer->Склад as $productCount) {


                        $this->saveProductCount($product, $productCount);
                    }
                }

            }


            if ($this->module->updatePrice == 1) {
                if ($offer->Цены->Цена) {
                    foreach ($offer->Цены->Цена as $item) {

                        $priceId = strval($item->ИдТипаЦены);
                        $price = floatval($item->ЦенаЗаЕдиницу);

                        if (empty($this->module->priceId) || $this->module->priceId == $priceId) {
                            $product->price = $price;
                        }

                        if (!empty($this->module->discountPriceId) && $this->module->priceId == $priceId) {
                            $product->discount_price = $price;
                        }
                    }

                } else {
                    $product->price = 0;
                    $product->discount_price = 0;
                    Yii::log('Hет цены | $product->id ' . $product->id . ' | external_id ' . $externalId, CLogger::LEVEL_WARNING);
                }

            }

            if (!$product->save()) {
                Yii::log('| $product->id ' . $product->id . ' | external_id ' . $externalId, CLogger::LEVEL_WARNING);
            }

            $this->reader->next('Предложение');

            gc_collect_cycles();
        }

        $this->reader->close();
    }


}