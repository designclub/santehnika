<?php

use yupe\models\Settings;

class CmlRepository
{
    /**
     * Find the category by external ID or return a new one
     *
     * @param $extId
     * @return StoreCategory
     */
    public function getCategory($extId)
    {
        $category = StoreCategory::model()->findByAttributes(['external_id' => $extId]);

        return $category ?: new StoreCategory();
    }

    /**
     * Find product by external ID or SKU.
     *
     * @param string $extId
     * @param string $sku
     * @param bool $returnNew
     * @return Product
     */
    public function getProduct($extId, $sku = null, $returnNew = true)
    {
        $criteria = new CDbCriteria();
        $criteria->params = [];

        $criteria->addCondition('external_id = :ext_id', 'OR');
        $criteria->params[':ext_id'] = $extId;

        if (!is_null($sku)) {
            $criteria->addCondition('sku = :sku', 'OR');
            $criteria->params[':sku'] = $sku;
        }

        $product = Product::model()->find($criteria);

        return $product ?: ($returnNew ? new Product() : null);
    }


    public function getStorage($extId, $returnNew = true)
    {
        $criteria = new CDbCriteria();
        $criteria->params = [];

        $criteria->addCondition('external_id = :ext_id', 'OR');
        $criteria->params[':ext_id'] = $extId;

        $storage = Storage::model()->find($criteria);

        return $storage ?: ($returnNew ? new Storage() : null);
    }


    public function getStorageRecord($storage_id, $product_id)
    {
        $criteria = new CDbCriteria();
        $criteria->params = [];

        $criteria->addCondition('storage_id = :storage_id');
        $criteria->params[':storage_id'] = $storage_id;


        $criteria->addCondition('product_id = :product_id');
        $criteria->params[':product_id'] = $product_id;

        $storage = StorageCount::model()->find($criteria);

        return $storage ?: new StorageCount();
    }


    // Метод для обновлнеия external_id
    public function getProductBySlug($name)
    {
        $criteria = new CDbCriteria();
        $criteria->params = [];

        $criteria->addCondition('slug = :slug');
        $criteria->params[':slug'] = $name;

        $product = Product::model()->find($criteria);

        return $product ? $product : new Product();
    }


    /**
     * Find order by id
     *
     * @param $id
     * @return Order|null
     */
    public function getOrder($id)
    {
        return Order::model()->findByPk($id);
    }

    /**
     * Find orders
     *
     * @return array|null
     */
    public function getOrderList()
    {
        $syncTime = $this->getOrderSyncTime();

        $criteria = new CDbCriteria();

        if (!is_null($syncTime)) {
            $criteria->condition = 'modified >= :sync_time';
            $criteria->params = [
                ':sync_time' => $syncTime,
            ];
        }

        return Order::model()->findAll($criteria);
    }

    /**
     * Returns last sync time
     *
     * @return string|null
     */
    public function getOrderSyncTime()
    {
        $queryTime = Settings::model()->findByAttributes(['module_id' => 'cml', 'param_name' => 'queryTime']);
        $successTime = Settings::model()->findByAttributes(['module_id' => 'cml', 'param_name' => 'successTime']);

        if (is_null($queryTime) || is_null($successTime)) return null;

        return ($successTime->param_value < $queryTime->param_value) ? $successTime->param_value : $queryTime->param_value;
    }


}