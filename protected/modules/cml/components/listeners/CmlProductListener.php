<?php
use yupe\components\Event;

class CmlProductListener
{
    /**
     * On after product import event.
     * Deactivate or remove unlisted products
     *
     * @param Event $event
     */
    public static function onAfterImport(Event $event)
    {
        /** @var CmlCatalog $catalog */
        $catalog = $event->getCatalog();
        $module = $catalog->getModule();
        $criteria = new CDbCriteria();

        $criteria->addNotInCondition('id', $catalog->getProductList());

        if ($module->productNotInList == CmlModule::ACTION_DEACTIVATE) {
            Product::model()->updateAll(['in_stock' => Product::STATUS_NOT_IN_STOCK], $criteria);
        }

        if ($module->productNotInList == CmlModule::ACTION_DELETE) {
            Product::model()->deleteAll($criteria);
        }
    }
}