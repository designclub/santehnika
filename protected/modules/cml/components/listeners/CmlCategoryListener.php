<?php
use yupe\components\Event;

class CmlCategoryListener
{
    /**
     * On after category import event.
     * Deactivate or remove unlisted categories
     *
     * @param Event $event
     */
    public static function onAfterImport(Event $event)
    {
        /** @var CmlCatalog $catalog */
        $catalog = $event->getCatalog();
        $module = $catalog->getModule();
        $criteria = new CDbCriteria();

        $criteria->addNotInCondition('id', array_values($catalog->getCategoryList()));

        if ($module->categoryNotInList == CmlModule::ACTION_DEACTIVATE) {
            StoreCategory::model()->updateAll(['status' => StoreCategory::STATUS_DRAFT], $criteria);
        }

        if ($module->categoryNotInList == CmlModule::ACTION_DELETE) {
            StoreCategory::model()->deleteAll($criteria);
        }
    }
}