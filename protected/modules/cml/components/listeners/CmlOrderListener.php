<?php

class CmlOrderListener
{
    /**
     * On order mark deleted event.
     * Change order status or delete it
     *
     * @param CmlOrderEvent $event
     */
    public static function onMarkDeleted(CmlOrderEvent $event)
    {
        $order = $event->getOrder();
        $action = Yii::app()->getModule('cml')->markDeleted;

        switch ($action) {
            case CmlModule::ACTION_DEACTIVATE:
                $order->status_id = OrderStatus::STATUS_DELETED;
                $order->save();
                break;

            case CmlModule::ACTION_DELETE:
                $order->delete();
                break;
        }

    }

    /**
     * On order change status
     *
     * @param CmlOrderEvent $event
     */
    public static function onChangeStatus(CmlOrderEvent $event)
    {
        $order = $event->getOrder();
        $defaultStatus = Yii::app()->getModule('cml')->defaultOrderStatus;
        $status = OrderStatus::model()->findByAttributes(['name' => $event->getStatusName()]);

        $order->status_id =  is_null($status) ? $defaultStatus : $status->id;
        $order->save();
    }
}