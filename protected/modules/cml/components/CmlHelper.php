<?php
class CmlHelper
{
    const MESSAGE_ERROR = 'failure';
    const MESSAGE_SUCCESS = 'success';

    /**
     * Create system message
     *
     * @param $message
     * @param null $type
     * @return string
     */
    public static function message($message, $type = null)
    {
        $data = [
            $type,
            $message,
        ];

        return implode(PHP_EOL, array_filter($data));
    }
}