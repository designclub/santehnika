<?php

use yupe\components\EventManager;

abstract class Cml
{
    /** @var CHttpRequest */
    protected $request;

    /** @var CmlModule */
    protected $module;

    /** @var CmlRepository */
    protected $repository;

    /** @var EventManager */
    protected $event;

    public function __construct(CHttpRequest $request, CmlModule $module, CmlRepository $repository, EventManager $eventManager)
    {
        $this->request = $request;
        $this->module = $module;
        $this->repository = $repository;
        $this->event = $eventManager;
    }

    /**
     * Check client IP address
     *
     * @return bool
     */
    public function checkIp()
    {
        if (is_null($this->module->getAllowedIpList())) return true;

        return in_array($this->request->getUserHostAddress(), $this->module->getAllowedIpList());
    }

    /**
     * Returns max upload file size
     *
     * @return int
     */
    public function getFileSizeLimit()
    {
        return $this->module->fileSizeLimit;
    }

    /**
     * Returns secret key
     *
     * @return string
     */
    public function getKey()
    {
        return $this->module->secretKey;
    }

    /**
     * Compare keys
     *
     * @param string $key
     * @return bool
     */
    public function isKeyValid($key)
    {
        return $key === $this->getKey();
    }

    /**
     * Add content to the file
     *
     * @param string $filename
     * @return int|bool
     */
    public function save($filename)
    {
        return file_put_contents($this->getPath($filename), fopen('php://input', 'r'), FILE_APPEND|LOCK_EX);
    }

    /**
     * Delete imported file by their name
     *
     * @param string $filename
     * @return bool
     */
    public function delete($filename)
    {
        return unlink($this->getPath($filename));
    }

    /**
     * Delete all xml files from upload dir
     *
     * @return bool
     */
    public function deleteAll()
    {
        $files = glob(sprintf('%s/*.xml', $this->module->getUploadPath()));

        foreach ($files as $file) {
            unlink($file);
        }

        return true;
    }

    /**
     * Is file exist
     *
     * @param string $filename
     * @return bool
     */
    public function isExist($filename)
    {
        return file_exists($this->getPath($filename));
    }

    /**
     * Returns path to the file
     *
     * @param string $file
     * @return string
     */
    public function getPath($file)
    {
        // file_put_contents('log', var_export('type = '. $file, true), FILE_APPEND);
        
        $info = new SplFileInfo($file);
        if(in_array(strtolower($info->getExtension()), ['jpg', 'png', 'jpeg', 'bmp'])){
            $path = Yii::getPathOfAlias('webroot') . DIRECTORY_SEPARATOR . Yii::app()->getModule('yupe')->uploadPath . DIRECTORY_SEPARATOR . Yii::app()->getModule('store')->uploadPath . DIRECTORY_SEPARATOR . 'product';
            return $path . DIRECTORY_SEPARATOR . $file;
        } else {
            return $this->module->getUploadPath() . DIRECTORY_SEPARATOR . $file;
        }
    }

    /**
     * @return CmlModule
     */
    public function getModule()
    {
        return $this->module;
    }

    abstract public function parse(XMLReader $reader, $filename);
}