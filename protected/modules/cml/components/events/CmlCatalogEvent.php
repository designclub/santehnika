<?php
use yupe\components\Event;

class CmlCatalogEvent extends Event
{
    /**
     * @var CmlCatalog
     */
    protected $catalog;

    /**
     * CmlCatalogEvent constructor.
     *
     * @param CmlCatalog $catalog
     */
    public function __construct(CmlCatalog $catalog)
    {
        $this->catalog = $catalog;
    }

    /**
     * Returns catalog instance
     *
     * @return CmlCatalog
     */
    public function getCatalog()
    {
        return $this->catalog;
    }
}