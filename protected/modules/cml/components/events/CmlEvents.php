<?php
class CmlEvents
{
    const CATEGORY_IMPORT = 'cml.category.after.import';

    const PRODUCT_IMPORT = 'cml.product.after.import';

    const STORAGE_IMPORT = 'cml.storage.after.import';

    const ORDER_MARK_DELETED = 'cml.order.mark.deleted';

    const ORDER_CHANGE_STATUS = 'cml.order.change.status';
}