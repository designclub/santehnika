<?php
use yupe\components\Event;

class CmlOrderEvent extends Event
{
    private $order;
    private $statusName;

    /**
     * CmlOrderEvent constructor.
     *
     * @param Order $order
     * @param null $statusName
     */
    public function __construct(Order $order, $statusName = null)
    {
        $this->order = $order;
        $this->statusName = $statusName;
    }

    /**
     * Returns order
     *
     * @return Order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @return string|null
     */
    public function getStatusName()
    {
        return $this->statusName;
    }
}