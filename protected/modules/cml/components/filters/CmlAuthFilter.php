<?php
class CmlAuthFilter extends CFilter
{
    private $allowedUserName;
    private $allowedUserPass;

    public function init()
    {
        $module = Yii::app()->getModule('cml');
        $this->allowedUserName = $module->allowedUserName;
        $this->allowedUserPass = $module->allowedUserPass;
    }

    public function preFilter($filterChain)
    {
        return true;
        if (
            isset($_SERVER['PHP_AUTH_USER']) &&
            isset($_SERVER['PHP_AUTH_PW']) &&
            $this->allowedUserName == $_SERVER['PHP_AUTH_USER'] &&
            $this->allowedUserPass == $_SERVER['PHP_AUTH_PW']
        ) {
            return true;
        }

        header('WWW-Authenticate: Basic realm="Authentication needed"');
        throw new CHttpException(401, Yii::t('yii','You are not authorized to perform this action.'));
    }
}