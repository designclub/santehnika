<?php

/**
 * CourierFormWidget
 */
class CourierFormWidget extends CWidget
{
    public $system;

    public function run()
    {
        $model = new CourierForm;
        
        $model->setAttributes([
            'street'    => $this->system->getPostStreet(),
            'house'     => $this->system->getPostHouse(),
            'apartment' => $this->system->getPostFlat(),
        ]);

        if (isset($_POST['CourierForm'])) {
            $model->attributes = $_POST['CourierForm'];
            $model->validate();
        }

        $this->render('courier-form-widget', [
            'model' => $model,
        ]);
    }
}
