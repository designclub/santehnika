<?php

/**
 * Class CdekPiclupSystem
 */

Yii::import('application.modules.courier.CourierModule');
Yii::import('application.modules.courier.models.Courier');
Yii::import('application.modules.delivery.components.DeliverySystem');

/**
 * Class CdekPiclupSystem
 */
class CourierSystem extends DeliverySystem
{
    public function renderCheckoutForm(Delivery $delivery, Order $order, $return = false)
    {
        if (Yii::app()->request->isAjaxRequest) {
            Yii::app()->clientScript->scriptMap = [
                // В карте отключаем загрузку core-скриптов, УЖЕ подключенных до ajax загрузки
                'jquery.js' => false,
                'jquery.ui.js' => false,
                // На моей странице была ещё одна форма, поэтому jquery.yiiactiveform.js я исключил
                'jquery.yiiactiveform.js' => false,
            ];
        }

        return Yii::app()->getController()->renderPartial(
            'application.modules.courier.views.form',
            [
                'order'    => $order,
                'delivery' => $delivery,
                'system' => $this,
            ],
            $return,
            true
        );
    }

    public function getCost(Order $order, Delivery $delivery)
    {
        if (null === $delivery->free_from) {
            return $delivery->price;
        }

        return $delivery->free_from <= $order->total_price ? 0 : $delivery->price;
    }
}
