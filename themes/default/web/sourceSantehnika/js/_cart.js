/*
	******************************************************
	******************************************************

	======================================================
	// Все, что между комментарий
	// ["START NEW CART" и "END NEW CART"]
	// НЕОБХОДИМО УДАЛИТЬ (ЗАКОММЕНТИРОВАТЬ) из store.js
	======================================================
	
	======================================================
	// Необходимо удалить закрывающие скобки в store.js, для того, 
	чтобы текущий код вставился $(document).ready(function() store.js
	======================================================

	======================================================
	// Закоментировать вызов функции сверху
	   checkFirstAvailableDeliveryType();
	======================================================

	// Заменить переменные вначале в store.js 
	   var shippingCostElement = $('#cart-shipping-cost'); // Сумма доставки
	   var cartFullCostElement = $('.js-cart-full-cost');  // сумма заказа без доставки со скидкой
	   var cartFullCostWithShippingElement = $('.js-cart-full-cost-with-shipping'); //Итого сумма с доставкой

	   var cartFullCostElement2 = $('#cart-full-cost2');
       var cartDiscountCost = $('.js-cart-discount-cost');

	******************************************************
	******************************************************
*/

// $(document).ready(function() {
	
	updatePositionListDiscount();

	/*============ START NEW CART========================*/
	$(document).delegate('.cart-delete-product', 'click', function (e) {
        e.preventDefault();
        var el = $(this);
        var data = {'id': el.data('position-id')};
        data[yupeTokenName] = yupeToken;
        $.ajax({
            url: yupeCartDeleteProductUrl,
            type: 'post',
            data: data,
            dataType: 'json',
            success: function (data) {
                if (data.result) {
                    el.closest('.js-cart-item').remove();

                    if ($('.cart-list .js-cart-item').length == 0) {
                        $('.cart-section').remove();
                        $('.empty-cart').css({'opacity': 1, 'display': 'block'});
                    }
                    // $('#cart-total-product-count').text($('.cart-list .cart-item').length);
                    positionCountPr();
                    updateCartTotalCost();
                    updateCartWidget();
                }
            }
        });
    });

    $(document).delegate('.position-count', 'change', function () {
        var el = $(this).parents('.js-cart-item'),
            positionCountEl = el.find('.position-count');

        var quantity = parseInt(positionCountEl.val());
        var productId = el.find('.position-id').val();

        if (quantity <= 0 || isNaN(quantity)) {
            quantity = 1;
        }

        var quantityLimiterEl = el.find('.spinput'),
            minQuantity = parseInt(quantityLimiterEl.data('min-value')),
            maxQuantity = parseInt(quantityLimiterEl.data('max-value'));

        if (quantity < minQuantity) {
            quantity = minQuantity;
        }
        else if (quantity > maxQuantity) {
            quantity = maxQuantity;
        }

        positionCountEl.val(quantity);

        updatePositionSumPrice(el);
        changePositionQuantity(productId, quantity);
        positionCountPr();
    });

    function positionCountPr(){
        var quantityPr = 0;
        $(".cart-list .js-cart-item").each(function(){
            quantityPr = parseInt(quantityPr) + parseInt($(this).find('.position-count').val());
        });
        $('#cart-total-product-count').text(quantityPr);
    }

    function getPositionsCost() {
        var cost = 0;
        $.each($('.position-sum-price'), function (index, elem) {
            cost += parseFloat($(elem).text());
        });

        return cost;
    }

    /* Общая сумма со скидкой */
    function getCartTotalCost() {
        var cost = getPositionsCost();
        var delta = 0;
        var coupons = getCoupons();
        $.each(coupons, function (index, el) {
            if (cost >= el.min_order_price) {
                switch (el.type) {
                    case 0: // руб
                        delta += parseFloat(el.value);
                        break;
                    case 1: // %
                        delta += (parseFloat(el.value) / 100) * cost;
                        break;
                }
            }
        });

        return delta > cost ? 0 : cost - delta;
    }

    /* Общая сумма без скидки у товаров */
    function getCartTotalNoDiscountCost() {
        var cost = 0;
        $.each($('.position-full-sum-price'), function (index, elem) {
            cost += parseFloat($(elem).text());
        });
        var delta = 0;
        var coupons = getCoupons();
        $.each(coupons, function (index, el) {
            if (cost >= el.min_order_price) {
                switch (el.type) {
                    case 0: // руб
                        delta += parseFloat(el.value);
                        break;
                    case 1: // %
                        delta += (parseFloat(el.value) / 100) * cost;
                        break;
                }
            }
        });

        return delta > cost ? 0 : cost - delta;
    }
    /* Общая сумма скидки */
    function getCartDiscountCost() {
        var cost = 0;
        $.each($('.position-discount-sum-price'), function (index, elem) {
            cost += parseFloat($(elem).text());
        });

        return cost;
    }

    function updateCartTotalCost() {
        // cartFullCostElement.html(getCartTotalCost().toFixed(2));
        cartFullCostElement.html(number_format(getCartTotalNoDiscountCost(), 2, '.', ' '));
        refreshDeliveryTypes();
        updateShippingCost();
        updateFullCostWithShipping();
        checkMinAmountFunc();
        var discountTotal = getCartDiscountCost();
        if (discountTotal > 0) {
            cartDiscountCost.html('- ' + number_format(getCartDiscountCost(), 2, '.', ' '));
            cartDiscountCost.parents('.cart-total').removeClass('hidden');
        } else{
            cartDiscountCost.parents('.cart-total').addClass('hidden');
        }
    }

    function updatePositionSumPrice(tr) {
        /*var count = parseInt(tr.find('.position-count').val());
        var price = parseFloat(tr.data('price'));
        var possum = (price * count).toFixed(2);
        tr.find('.position-sum-price').html(possum);*/

        updatePositionDiscount(tr);

        updateCartTotalCost();
    }

    function updatePositionListDiscount() {
        $(".cart-list .js-cart-item").each(function(){
            updatePositionDiscount($(this));
        });
        updateCartTotalCost();
    }
    function updatePositionDiscount(elem) {
        var count = parseInt(elem.find('.position-count').val());
        var price = parseFloat(elem.data('price'));
        var baseprice = parseFloat(elem.data('base-price'));
        var discount = parseFloat(elem.data('discount-price'));

        price = (price * count).toFixed(2);
        baseprice = (baseprice * count).toFixed(2);
        discount = (discount * count).toFixed(2);

        elem.find('.position-sum-price').html(price);
        elem.find('.position-full-sum-price').html(baseprice);
        elem.find('.position-discount-sum-price').html(discount);

        elem.find('.js-cartPrice-with-discount').html(number_format(price, 2, '.', ' '));
        elem.find('.js-cartPrice-without-discount').html(number_format(baseprice, 2, '.', ' '));
        elem.find('.js-cartPrice-benefit').html(number_format(discount, 2, '.', ' '));
    }

    /*============ END NEW CART========================*/

    // Ограничения на покупку
    function checkMinAmountFunc() {
        if (typeof minAmount === 'undefined') {
            return ;
        }

        if (minAmount > getPositionsCost()) {
            $('.js-check-min-amount').removeClass('hide');
            $('.js-next-button').addClass('hide');
            $('.js-return-url').removeClass('hide');
        } else {
            $('.js-check-min-amount').addClass('hide');
            $('.js-next-button').removeClass('hide');
            $('.js-return-url').addClass('hide');
        }
    }

    /*Пказать/скрыть статичную таблицу с товарами в корзине*/
    $('.js-static-table-toggle').on('click', function() {
        var link = $(this),
        toggle = link.data('target'),
        table = $(toggle);

        link.toggleClass('active');

        table.toggle();

        return false;
    });

    /**
     * Авторизация, регистрация, модальное окно
     */
    $(document).on('submit', '#ajax-login, #ajax-registration', function(e) {
        var form = $(this),
        id = form.attr('id'),
        action = form.attr('action'),
        method = form.attr('method'),
        data = form.serialize();

        $.ajax({
            url: action,
            type: method,
            data: data,
            dataType: 'html',
            success: function(data) {
                $('#'+id).html(data);
            },
        })
        return false;
    });

    /**
     * Модуль доставка - новыый
     */

    // Вывод тарифов и спопосов оплаты
    $(document).on('change', 'input[name=\'Order[delivery_id]\']', function() {
        var input = $(this);
        $('.js-items-delivery_id').removeClass('active');
        input.parents('.js-items-delivery_id').addClass('active');
        $(".preloader").addClass("active");

        $.ajax({
            url: deliveryMethodUrl,
            type: 'post',
            data: input.parents('form').serialize(),
            success: function(data) {
                $('.js-delivery-method')
                    .fadeOut({
                        done: function() {
                            $(this)
                                .html(data)
                                .fadeIn();
                        }
                    });
            },
            complete: function () {
                $(".preloader").removeClass("active");
            }
        });

        return false;
    });

    /**
     * Самовывоз
     */
    var modalMap = null;
    /*$('a[href="#pickup-modal"]')
        .fancybox({
            'animationEffect': 'fade',
            'buttons': ['zoom','close'],
            'beforeClose': function() {
                if (modalMap) {
                    modalMap.destroy();
                }
            }
        });*/
    $(document)
        .on('click', 'a[href="#pickup-modal"]', function(e) {
            var elem = $(this);
            var modal = elem.attr('href');
            var item = elem.parents('.js-pickup-item');
            var data = item.data();
            $.each(data, function(key, value) {
                var selector = '.js-pickup-modal-'+key;
                if ($(selector).length > 0 ) {
                    $(selector).html(value);
                }
            });

            if (data.latitude && data.longitude) {
                ymaps.ready(function() {
                    modalMap = new ymaps.Map("map", {
                        center: [data.latitude, data.longitude],
                        controls: [],
                        zoom: 17
                    });
                    var point = new ymaps.GeoObject({
                        geometry: {
                            type: "Point",
                            coordinates: [data.latitude, data.longitude],
                        },
                    }, {
                        preset: "islands#blackDotIconWithCaption",
                        iconColor: "#0074FF",
                    });
                    modalMap.geoObjects.add(point);
                });
                $(modal).modal('show');
            }
            return false
        });

    var pickupItemMap;
    $(document).on('change', 'input[name="Order[pickup][]"], input[name="Order[cdek_pvz][]"]', function() {
        var elem = $(this);
        var itemContainer = elem.parents('.js-pickup-item');
        var mapContainer = itemContainer.find('.js-pickup-item__map');
        var buttonSpan = itemContainer.find('.pickup-checkbox span');
        var data = itemContainer.data();

        if (elem.prop('checked')) {
            $('input[name="Order[pickup][]"]').each(function(i, e) {
                if(elem.val() != $(e).val()) {
                    $(e)
                        .parents('.js-pickup-item')
                        .hide()
                        .find('.pickup-checkbox span')
                        .text('Забрать отсюда');
                }
            });

            buttonSpan.text('Выбрать другой');

            if (data.latitude && data.longitude) {
                mapContainer.css({'height': '150px', 'width': '100%'}).show();
                ymaps.ready(function() {
                    pickupItemMap = new ymaps.Map(mapContainer.get(0), {
                        center: [data.latitude, data.longitude],
                        controls: ['geolocationControl'],

                        zoom: 17
                    });
                    var point = new ymaps.GeoObject({
                        geometry: {
                            type: "Point",
                            coordinates: [data.latitude, data.longitude],
                        },
                    }, {
                        preset: "islands#blackDotIconWithCaption",
                        iconColor: "#0074FF",
                    });
                    pickupItemMap.geoObjects.add(point);
                });
            }

        } else {
            pickupItemMap.destroy();
            $('.js-pickup-item').each(function(i, e) {
                $(e).show();
            });
            buttonSpan.text('Забрать отсюда');
            mapContainer.css({'height': '0px', 'width': '0'}).hide();
        }
    });
    $(document).on('click', '.js-pickupModal-checkbox', function(e) {
        var elem = $(this);
        var itemContainer = elem.parents('.js-pickupModal-item');
        var id = parseInt(itemContainer.find('.js-pickup-modal-id').text());
        $('.js-pickup-item input[type="checkbox"]').prop('checked', false);
        $('.js-pickup-item[data-id="'+id+'"]').find('input[type="checkbox"]').prop("checked",true).trigger('change');
        elem.parents('.modal').modal('hide');
        return false;
    });

    $(document).on('hidden.bs.modal','#pickup-modal', function () {
        $(".js-pickupModal-map").html('');
    });
    /**
     * Самовывоз end
     */


    /**
     * модуль СДЭК
     */
    $(document).on('change', 'input[name="Order[sub_delivery_id]"]', function() {
        var item = $(this);
        var action = item.data('action');
        var container = $('.js-sub-delivery');

        $('.js-items-sub_delivery_id').removeClass('active');
        item.parents('.js-items-sub_delivery_id').addClass('active');

        $.ajax({
            url: action,
            type: 'post',
            // dataType: 'json',
            data: item.parents('form').serialize(),
            success: function(data) {
                container.html(data);
            }
        });

        return false;
    });

    /**
     * Модуль доставка - новыый end
     */


    function number_format(number, decimals, dec_point, separator ) {
        number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
        var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof separator === 'undefined') ? ',' : separator ,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function(n, prec) {
            var k = Math.pow(10, prec);
            return '' + (Math.round(n * k) / k).toFixed(prec);
        };
        // Фиксим баг в IE parseFloat(0.55).toFixed(0) = 0;
        s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
        if (s[0].length > 3) {
            s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
        }
        if ((s[1] || '').length < prec) {
            s[1] = s[1] || '';
            s[1] += new Array(prec - s[1].length + 1).join('0');
        }
        return s.join(dec);
    }
});