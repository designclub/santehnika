
// @prepros-append "_cart.js"

function showNotify(element, result, message) {
    if(message != undefined){
        $('#notifications').html('<div class="notifications-' + result + '">' + message + '</div>').fadeIn().delay(3000).fadeOut();
    }
}

/* Функция для обновления списка */
function filterUpdate(type) {
    var form = $('#store-filter'),
        data = form.serialize(),
        action = form.attr('action');
    if (data == '') {
        data={};
    }

    var historyurl = action+'?'+data;
    if(type == false){
        data={};
        historyurl = action;
        rangeInputUpdate();
        form.find(':checked').prop('checked', false);
    }
    window.history.pushState(null, document.title, historyurl);

    filterListSelected();
    var innerWidth = window.innerWidth;
    if(innerWidth <= 1240){
        cityBrandsChecked($('.js-filter-city-block'));
    }

    // $('.ajax-loading').fadeIn(500);

    var top = $('#product-box').offset().top - 50;

    $('body,html').animate({
        scrollTop: top + 'px'
    }, 400);

    if($('.product-box .product-box__item').length == 0){
        $('.product-box .empty').addClass('hidden');
        for (var i = 3; i >= 0; i--) {
            $('.product-box').append("<div class='product-box__item'><div class='product-box__img fl fl-al-it-c fl-ju-co-c'></div><div class='product-box__info fl fl-di-c fl-ju-co-sp-b'><div><div class='product-box__price product-price product-price-new fl fl-wr-w fl-al-it-fl-e'><div class='product-price__discountPercent'><span class='discount-percent fl fl-al-it-c fl-ju-co-c'>%</span></div><span class='product-price__res'>9999</span><span class='product-price__old'><span class='strikethrough'><span class='price-old'>9999</span><span class='ruble'><i class='icon-ruble-old'></i></span></span></span></div><div class='product-box__name'><a class='product-name' href='#'>product-box__name</a></div></div><div class='product-box__bottom fl fl-wr-w'><div class='product-box__but product-box-but'><a href='#' class='but but-blue-gradient js-product-but'><i class='icon icon-load'></i><span>В корзине</span></a></div></div></div></div>");
        }
    }

    $.fn.yiiListView.update('product-box', {
        'data': data,
        'url': '',
        complete:function() {
            if($('div').hasClass('raiting-list')){
                $('.raiting-list input').rating({'starWidth':'16','readOnly':true});
            }
            imageDotsHover();
            if($('div').hasClass('js-filter-stock')){
                if($("#stock_0").prop('checked') == true){
                    $('.js-filter-stock').addClass('checked').find('input[type="checkbox"]').prop('checked', true);
                }
            }
            // $('.ajax-loading').delay(100).fadeOut(500);
        }
    });


    return false;
};

function rangeInputUpdate(){
    $(".js-range").each(function(){
        var slider = $('#'+$(this).attr('id')).data("ionRangeSlider");
        var parents = $(this).parents('.filter-block-input-range');
        slider.update({
            from: $(this).data('min'),
            to: $(this).data('max')
        });

        parents.find('.js-from-in').prop('value', '');
        parents.find('.js-to-in').prop('value', '');
    });
};

/* Вывод выбранных фильтров*/
function filterListSelected(){
    var selectedFilters = $('.selected-filters');
    var storeFilter = $('#store-filter');
    var blocks = storeFilter.find('.filter-block');
    var elem;
    var flag;
    var elems = [];
    selectedFilters.html('');
    $.each(blocks, function(i, e) {
        elem = $(this).find('input:checked, option:selected, .range-input');
        var flag = null;
        $.each(elem, function(i, e) {
            var el = $(e);
            var type = null;
            var label = null;
            if (e.tagName==='INPUT') {
                type = el.attr('type');
                if (type=='radio') {
                    var par = el.parents('.filter-block').find('.filter-block__header span');
                    label = par.data('title') + ': ';
                    label += ' ' + el.next('label').text();
                } else if(type=='checkbox') {
                    var par = el.parents('.filter-block').find('.filter-block__header span');
                    label = par.data('title') + ': ';
                    label += ' ' + el.next().text();
                } else if(type=='text') {
                    var value = el.val();
                    if (value) {
                        // label = el.prev('label').text();
                        label = el.parents('.filter-block').find('.filter-block__header span').text() + ': ';
                        label += ' '+el.val();
                    }
                }
            } else if(el.hasClass('range-input')) {
                var par = el.parents('.filter-block').find('.filter-block__header span');
                type = 'number';
                var value = '';
                var count = 0;
                el.find('input').each(function(){
                    if($(this).val()){
                        if(count != 0){
                            value += ' - ';
                        }
                        value += $(this).val();
                        count++;
                    }
                });
                if(value){
                    label = par.data('title') + ': ';
                    label += value+' '+ par.data('unit');
                }
            } else {
                type = 'select';
                label = el.text();
            }
            if (label && type) {
                el.parents('.filter-block').addClass('selected active');
                flag = true;
                elems.push({
                    el: el,
                    type: type,
                    label: label,
                });
            }
        });
        if(flag===null){
            if($(this).hasClass('selected')){
                $(this).removeClass('selected');
                if(!$(this).hasClass('visiblity')){
                    $(this).removeClass('active');
                }
            }
        }
    });

    if(elems.length > 0){
    }

    $.each(elems, function(i, e) {
        var span = $('<span data-id=#'+e.el.attr("id")+' class="label label-default"></span>');
        span
            .text(e.label);

        selectedFilters.append(span);
    });
    if(elems.length > 0){
        selectedFilters.prepend('<button type="reset" class="reset-filter">Сбросить фильтры</button>').addClass('active');
    } else{
        selectedFilters.removeClass('active');
        $('.filter-block').removeClass('selected');
        if(!$('.filter-block').hasClass('visiblity')){
            $('.filter-block').removeClass('active');
        }
    }
};

var timnotification;
function butApply() {
    var innerWidth = window.innerWidth;
    if(innerWidth > 1000){
        filterUpdate();
    } else{
        clearTimeout(timnotification);
        $('.js-notification').fadeIn();
        timnotification = setTimeout(function() {
            $('.js-notification').fadeOut();
        }, 15000);
    }
}

function cityBrandsChecked(elem) {
    var flag = null;
    $('.js-brands-checkbox').parent().addClass('filter-disabled');
    elem.find('input:checked').each(function(){
        $('.js-brands-checkbox[data-city='+$(this).val()+']').parent().removeClass('filter-disabled');
        flag = true;
    });

    if(flag == null){
        $('.js-brands-checkbox').parent().removeClass('filter-disabled');
    }
}

$(document).ready(function () {
    /*
     * Действия при изменении окна
    */
    $(window).resize(function () {
        var innerWidth = window.innerWidth;
        if($('.sidebar-box').hasClass('active')){
            if(innerWidth > 1240){
                $('body').removeClass('bodymenu');
                $(".sidebar-box").removeClass('active');
                $("#store-filter").removeClass('active');
            }
        }

        if(innerWidth < 769){
            getTemplateProduct();
        }
    });
    /*
     * Товары
    */
    /* Функция определения шаблона на вывода товаров */
    function getTemplateProduct() {
        var box = $('.template-product__item.active');
        if(box.data('view') == "_item-list"){
            $('.template-product__item').addClass('active');
            box.removeClass('active');
            setCookie("store_item", "_item", {'path' : '/'});
            setTimeout(filterUpdate(), 3000);
        }
    }
    /* Вызываем функцию при загрузке сайта  */
    var innerWidth = window.innerWidth;
    if(innerWidth < 769){
        getTemplateProduct();
    }

    /* Клик, чтобы изменить шаблон вывода товаров */
    $(document).delegate(".template-product__item", "click", function(){
        setCookie("store_item", $(this).data("view"), {'path' : '/'});
        $('.template-product__item').removeClass('active');
        $(this).addClass('active');
        filterUpdate();
        return false;
    });

    if($('div').hasClass('selected-filters')){
        filterListSelected();
        // rangeInputUpdate();
    }
    if($('div').hasClass('js-filter-city-block')){
        cityBrandsChecked($('.js-filter-city-block'));
    }

    /*
     * Сортировка
    */
    /* Открыть список сортировок */
    $(document).delegate('.box-wrapper__header', 'click', function(){
        $(this).parent().toggleClass('active');
    });
    /* Клик по любому месту, чтобы закрыть все выпадающие списки */
    $(document).delegate('body', 'click', function(e){
        if($(e.target).parents('.box-wrapper').length < 1){
            $('.box-wrapper').removeClass('active');
            // return false;
        }
    });
    /* клик по сортировке */
    $(document).delegate('.sort-box-wrapper__link', 'click', function(){
        var elem = $(this);
        $('.sort-box-wrapper__link').removeClass('active');

        // $('.ajax-loading').fadeIn(500);

        var top = $('#product-box').offset().top - 50;

        $('body,html').animate({
            scrollTop: top + 'px'
        }, 400);

        $.fn.yiiListView.update('product-box', {
            data: 'ajax=product-box&sort=' + elem.attr('data-href'),
            complete:function() {
                elem.addClass('active');
                elem.parents('.sort-box-wrapper').removeClass('active');
                $('.sort-box-wrapper__header').html(elem.html());
                imageDotsHover();
                // $('.ajax-loading').delay(100).fadeOut(500);
            }
        });


        return false;
    });

    /* Клик, чтобы изменить кол-во товаров */
    $(document).delegate(".countItem-wrapper__link", "click", function(){
        setCookie("store_count", $(this).data("count"), {'path' : '/'});
        $('.countItem-wrapper__link').removeClass('active');
        $(this).addClass('active');
        filterUpdate();
        return false;
    });

    /*
     * фильтры при адаптации
    */
    $(document).delegate('.but-menu-filter', 'click', function() {
        $('body').addClass('bodymenu');
        // $('html').addClass('htmlmenu');
        $("#store-filter").addClass('active');
        $(".sidebar-box").addClass('active');
        return false;
    });
    /* Автоматическое обновление списка при изменении */
    $(document).delegate('.js-filter-block', 'change', function(e){
        var innerWidth = window.innerWidth;
        if(innerWidth > 1240){
            filterUpdate();
            return false;
        }
    });
    $(document).delegate('.js-filter-city-block', 'change', function(e){
        var innerWidth = window.innerWidth;
        if(innerWidth > 1240){
            filterUpdate();
        } else {
            cityBrandsChecked($('.js-filter-city-block'));
        }
    });

    /* Клик по постраничной навигации */
    /*$(document).delegate('#product-box .pagination li a', 'click', function(){
        // $('.ajax-loading').fadeIn(500);
        var top = $('#product-box').offset().top - 50;
        $('body,html').animate({
            scrollTop: top + 'px'
        }, 400);
        // $('.ajax-loading').delay(100).fadeOut(500);
    });*/

    $(document).delegate('#product-box .pagination li a', 'click', function(){
        var link = $(this).attr('href');
        $.fn.yiiListView.update('product-box', {
            'url': link,
            complete:function() {
                var top = $('#product-box').offset().top - 50;
                $('body,html').animate({
                    scrollTop: top + 'px'
                }, 400);
            }
        });

    });

    // $(document).delegate('.sidebar-box', 'click', function(e) {  
    $('.sidebar-box').on('click', function(e){   
        if($(e.target).hasClass('sidebar-box')){
            $('body').removeClass('bodymenu');
            $(".sidebar-box").removeClass('active');
            $("#store-filter").removeClass('active');
            return false;
        }
    });

    /* Клик по кнопке применить фильтры */
    $(document).delegate('.but-filter', 'click', function(e){
        
        clearTimeout(timnotification);
        $('.js-notification').fadeOut();
        filterUpdate();

        // if($('.filter-block').hasClass('active')){
        //     $('.filter-block').removeClass('active');
        // }
        $('body').removeClass('bodymenu');
        $(".sidebar-box").removeClass('active');
        $("#store-filter").removeClass('active');

        return false;
    });

    /* Кнопка сбросить убираем выделенные фильтры */
    $(document).delegate('.reset-filter, #reset-filter, button[type=reset]', 'click', function(e){
        // $('#store-filter').trigger('reset');
        $('#store-filter').get(0).reset();
        // Saving it's instance to var
        // rangeInputUpdate();
        filterUpdate(false);

        var innerWidth = window.innerWidth;
        if(innerWidth <= 1240){
            $('body').removeClass('bodymenu');
            // $('html').removeClass('htmlmenu');
            $(".sidebar-box").removeClass('active');
            $("#store-filter").removeClass('active');
        }
        return false;
    });

    /* Удаление выбраных фильтров при клике на крестик (выделеный красный блок) */
    $(document).delegate('.filter-close', 'click', function(e){
        var parent = $(this).parents('.filter-block');
        var elem = parent.find('input:checked, option:selected, .range-input');
        var type;
        $.each(elem, function(i, e) {
            type = $(this).attr('type')
            if (type=='radio') {
                $(this).prop('checked', false);
            } else if(type=='checkbox') {
                $(this).prop('checked', false);
            } else if(type=='range-input') {
                $(this).find('input').val('');
                var slider = parent.find('.js-range').data("ionRangeSlider");
                slider.update({
                    from: parent.find('.js-range').data('min'),
                    to: parent.find('.js-range').data('max')
                });
            } else {
                $(this).prop("selected", false)
            }
        });
        
        parent.removeClass('selected');

        filterUpdate();

        return false;
    });

    /* Удаление фильтров из списка примененных */
    $(document).delegate('.selected-filters span', 'click', function(e){
        var id = $(this).attr('data-id');
        var type = $(id).attr('type');
        var parent = $(id).parents('.filter-block');
        var checked;

        if (type=='radio') {
            $(id).prop('checked', false);
            checked = parent.find('input:checked');
            if(checked.length == 0){
                parent.removeClass('selected');
            }
        } else if(type=='checkbox') {
            $(id).click();
            checked = parent.find('input:checked');
            if(checked.length == 0){
                parent.removeClass('selected');
            }
        } else if(type=='range-input') {
            $(id).find('input').val('');
            // rangeInputUpdate();
            var slider = parent.find('.js-range').data("ionRangeSlider");
            slider.update({
                from: parent.find('.js-range').data('min'),
                to: parent.find('.js-range').data('max')
            });
            parent.removeClass('selected');
        } else {
            $(id).prop("selected", false);
            checked = parent.find('option:selected');
            if(checked.length == 0){
                parent.removeClass('selected');
            }
        }

        filterUpdate()

        return false;
    });

    /* Скрыть/показать блок фильтры */
    $(document).delegate('.filter-block .filter-block__header', 'click', function(e){
        var parent = $(this).parents('.filter-block');
        parent.toggleClass('active');
        // if(parent.hasClass('visiblity')){
        //     parent.removeClass('visiblity');
        // }
        return false;
    });
    /* Кнопка (Показать еще) в фильтрах*/
    $(document).delegate('.filter-block__more', 'click', function(e){
        var parent = $(this).parents(".filter-block");
        var span = $(this).find("span");
        var text = span.data('text');
        if($(this).hasClass('active')){
            parent.removeClass('visibles');
            $(this).removeClass('active');
            parent.find(".filter-list.hidden-2").removeClass("hidden-2").addClass("hidden");
            parent.find('.filter-block__list').removeClass("active");
            span.data('text', span.text());
            span.text(text);

        } else {
            parent.addClass('visibles');
            $(this).addClass('active');
            parent.find(".filter-list.hidden").removeClass("hidden").addClass("hidden-2");
            parent.find('.filter-block__list').addClass("active");
            span.data('text', span.text());
            span.text(text);
        }
        return false;
    });

    $(document).delegate('.showmore', 'click', function(){
        var id = $(this).attr('href');
        $('.ajax-loading').fadeIn(500);
        
        var top = $(id).offset().top - 50;
        $('#myTab li').removeClass('active');
        $('#myTab li a[href='+id+']').parent().addClass('active');
        $('body,html').animate({
            scrollTop: top + 'px'
        }, 400);
        
        $('.ajax-loading').delay(100).fadeOut(500);
        return false;
    });

    $(document).delegate('#myTab li a', 'click', function(){
        var innerWidth = window.innerWidth;
        if(innerWidth < 641){
            var id = $(this).attr('href');
            var top = $(id).offset().top - 50;
            $('body,html').animate({
                scrollTop: top + 'px'
            }, 400);
        }
        return true;
    });
    
    /*
     *  Рейтинг
    */
    // Клик по кнопке, чтобы показать всю таблицу
    $(document).delegate('.raiting-list-form .raiting-list__item', 'click', function(){
        var $this = $(this);
        var id = $this.data('id');
        $('.raiting-list-form .raiting-list__item').removeClass('active');
        $this.addClass('active').prevAll(".raiting-list__item").addClass('active');
        $this.parent().addClass('no-hover');
        $('#Review_rating').val(id);
        return false;
    });

    if($('div').hasClass('js-filter-stock')){
        // $('.catalog-controls-instock').append($('.js-filter-stock'));
        $(document).delegate('.js-filter-stock', 'click', function(){
            if($(this).hasClass('checked')){
                $(this).removeClass('checked');
                $(this).find('input[type="checkbox"]').prop('checked', false);
                $("#stock_0").prop('checked', false);
            } else {
                $(this).addClass('checked');
                $(this).find('input[type="checkbox"]').prop('checked', true);
                $("#stock_0").prop('checked', true);
            }

            filterUpdate();

            return false;
        });
    }
});

$(document).ready(function () {
    var cartWidgetSelector = '.js-shopping-cart-widget';

    /*страница продукта*/
    var priceElement = $('#result-price'); //итоговая цена на странице продукта
    var basePrice = parseFloat($('#base-price').val()); //базовая цена на странице продукта
    var quantityElement = $('#product-quantity');
    var quantityInputElement = $('#product-quantity-input');

    /*корзина*/
    var shippingCostElement = $('#cart-shipping-cost'); // Сумма доставки
    var cartFullCostElement = $('.js-cart-full-cost');  // сумма заказа без доставки со скидкой
    var cartFullCostWithShippingElement = $('.js-cart-full-cost-with-shipping'); //Итого сумма с доставкой

    /* Сумма заказа без доставки без скидки */
    var cartFullCostElement2 = $('#cart-full-cost2');
    var cartDiscountCost = $('.js-cart-discount-cost');


    miniCartListeners();
    refreshDeliveryTypes();
    // checkFirstAvailableDeliveryType();
    updateAllCosts();

    // Галерея дополнительных изображений в карточке товара
    // $('.js-product-gallery').productGallery();

    // Табы в карточке товара
    // $('.js-tabs').tabs();

    // $(".js-select2").select2();

    $('#start-payment').on('click', function () {
        $('.payment-method-radio:checked').parents('.payment-method').find('form').submit();
    });

    $('body').on('click', '.clear-cart', function (e) {
        e.preventDefault();
        var data = {};
        data[yupeTokenName] = yupeToken;
        $.ajax({
            url: '/coupon/clear',
            type: 'post',
            data: data,
            dataType: 'json',
            success: function (data) {
                if (data.result) {
                    updateCartWidget();
                }
            }
        });
    });

    $('#add-coupon-code').click(function (e) {
        e.preventDefault();
        var code = $('#coupon-code').val();
        var button = $(this);
        if (code) {
            var data = {'code': code};
            data[yupeTokenName] = yupeToken;
            $.ajax({
                url: '/coupon/add',
                type: 'post',
                data: data,
                dataType: 'json',
                success: function (data) {
                    if (data.result) {
                        window.location.reload();
                    }
                    showNotify(button, data.result ? 'success' : 'danger', data.data.join('; '));
                }
            });
            $('#coupon-code').val('');
        }
    });

    $('.coupon .close').click(function (e) {
        e.preventDefault();
        var code = $(this).siblings('input[type="hidden"]').data('code');
        var data = {'code': code};
        var el = $(this).closest('.coupon');
        data[yupeTokenName] = yupeToken;
        $.ajax({
            url: '/coupon/remove',
            type: 'post',
            data: data,
            dataType: 'json',
            success: function (data) {
                showNotify(this, data.result ? 'success' : 'danger', data.data);
                if (data.result) {
                    el.remove();
                    updateAllCosts();
                }
            }
        });
    });

    $('#coupon-code').keypress(function (e) {
        if (e.which == 13) {
            e.preventDefault();
            $('#add-coupon-code').click();
        }
    });

    $('.order-form').submit(function () {
        $(this).find("button[type='submit']").prop('disabled', true);
    });

    $('select[name="ProductVariant[]"]').change(function () {
        updatePrice();
    });

    $('.product-quantity-increase').on('click', function () {
        quantityInputElement.val(parseInt(quantityInputElement.val()) + 1).trigger('change');
    });

    $('.product-quantity-decrease').on('click', function () {
        if (parseInt(quantityInputElement.val()) > 1) {
            quantityInputElement.val(parseInt(quantityInputElement.val()) - 1).trigger('change');
        }
    });

    $('#product-quantity-input').change(function (event) {
        var el = $(this);
        quantity = parseInt(el.val());

        if (quantity <= 0 || isNaN(quantity)) {
            quantity = 1;
        }

        var quantityLimiterEl = el.parents('.spinput'),
            minQuantity = parseInt(quantityLimiterEl.data('min-value')),
            maxQuantity = parseInt(quantityLimiterEl.data('max-value'));

        if (quantity < minQuantity) {
            quantity = minQuantity;
        }
        else if (quantity > maxQuantity) {
            quantity = maxQuantity;
        }

        el.val(quantity);
        quantityElement.text(quantity);
        $('#product-total-price').text(parseFloat($('#result-price').text()) * quantity);
    });

    $('body').on('click', '#add-product-to-cart', function (e) {
        e.preventDefault();
        var button = $(this);
        var form = $(this).parents('form');
        $.ajax({
            type: 'post',
            dataType: 'json',
            data: form.serialize(),
            url: form.attr('action'),
            success: function (data) {
                if (data.result) {
                    updateCartWidget();
                }
                showNotify(button, data.result ? 'success' : 'danger', data.data);
            }
        });
    });

    $('body').on('click', '.quick-add-product-to-cart', function (e) {
        e.preventDefault();
        var el = $(this);
        var data = {'Product[id]': el.data('product-id')};
        el.addClass('active');
        el.parents('.js-product-item').addClass('active');
        data[yupeTokenName] = yupeToken;
        $.ajax({
            url: el.data('cart-add-url'),
            type: 'post',
            data: data,
            dataType: 'html',
            success: function (data) {
                $('.js-addCartModal-body').html(data);
                $('#addCartModal').modal('show');
                // if (data.result) {
                    updateCartWidget();
                    setTimeout(function(){
                        el.removeClass('active');
                        el.parents('.js-product-item').removeClass('active');
                        el.removeClass('quick-add-product-to-cart but-svg but-svg-left but-animation').addClass('but-go-cart').attr('href', '/cart').find('span').html("В корзине");
                    }, 600);
                    // el.off('click','.quick-add-product-to-cart');
                    // el.removeClass('btn_cart')
                    //     .addClass('btn_success')
                    //     .html('Оформить заказ')
                    //     .attr('href', '/cart');
                // }
                // showNotify(el, data.result ? 'success' : 'danger', data.data);
            }
        });
    });

    /* 
     * количество у продукта
    */
    // $('.product-box-quantity-increase').on('click', function () {
    $(document).delegate('.product-box-quantity-increase', 'click', function(){
        var quantityLimiterEl = $(this).parents('.spinput'),
            maxQuantity = parseInt(quantityLimiterEl.data('max-value'))
        var input = quantityLimiterEl.find('input');
        if(parseInt(input.val()) < maxQuantity){
            input.val(parseInt(input.val()) + 1).trigger('change');
        }
        // showNotify(input, 'success', "Успешно добавлено");
    });

    // $('.product-box-quantity-decrease').on('click', function () {
    $(document).delegate('.product-box-quantity-decrease', 'click', function(){
        var quantityLimiterEl = $(this).parents('.spinput');
        var input = quantityLimiterEl.find('input');
        if (parseInt(input.val()) > 1) {
            input.val(parseInt(input.val()) - 1).trigger('change');
            // showNotify(input, 'success', "Успешно удален");
        }
    });

    $('.product-box-quantity-input').bind("keyup", function() {
        if (this.value.match(/[^0-9]/g)) {
            this.value = this.value.replace(/[^0-9]/g, '');
        }

        if (this.value <= 0 || isNaN(this.value)) {
            this.value = 1;
        }

        var quantityLimiterEl = $(this).parents('.spinput'),
            minQuantity = parseInt(quantityLimiterEl.data('min-value')),
            maxQuantity = parseInt(quantityLimiterEl.data('max-value'));

        if (this.value < minQuantity) {
            this.value = minQuantity;
        }
        else if (this.value > maxQuantity) {
            this.value = maxQuantity;
        }
    });


    $('.cart-quantity-increase').on('click', function () {
        var target = $($(this).data('target'));
        target.val(parseInt(target.val()) + 1).trigger('change');
    });

    $('.cart-quantity-decrease').on('click', function () {
        var target = $($(this).data('target'));
        if (parseInt(target.val()) > 1) {
            target.val(parseInt(target.val()) - 1).trigger('change');
        }
    });

    $(document).on('change', 'input[name="Order[delivery_id]"], input[name="Order[sub_delivery_id]"]', function () {
        updateShippingCost();
    });

    function miniCartListeners() {
        $('.mini-cart-delete-product').click(function (e) {
            e.preventDefault();
            var el = $(this);
            var data = {'id': el.data('position-id')};
            data[yupeTokenName] = yupeToken;
            $.ajax({
                url: yupeCartDeleteProductUrl,
                type: 'post',
                data: data,
                dataType: 'json',
                success: function (data) {
                    if (data.result) {
                        updateCartWidget();
                    }
                }
            });
        });

        $('#cart-toggle-link').click(function (e) {
            e.preventDefault();
            $('#cart-mini').toggle();
        });
    }

    function refreshDeliveryTypes() {
        var cartTotalCost = getCartTotalCost();
        $.each($('input[name="Order[delivery_id]"]'), function (index, el) {
            var elem = $(el);
            var availableFrom = elem.data('available-from');
            if (availableFrom.length && parseFloat(availableFrom) >= cartTotalCost) {
                if (elem.prop('checked')) {
                    checkFirstAvailableDeliveryType();
                }
                elem.prop('disabled', true);
            } else {
                elem.prop('disabled', false);
            }
        });
    }

    function checkFirstAvailableDeliveryType() {
        $('input[name="Order[delivery_id]"]:not(:disabled):first').prop('checked', true);
    }

    function getShippingCost() {
        var cartTotalCost = getCartTotalCost();
        var coupons = getCoupons();
        var freeShipping = false;
        $.each(coupons, function (index, el) {
            if (el.free_shipping && cartTotalCost >= el.min_order_price) {
                freeShipping = true;
            }
        });
        if (freeShipping) {
            return 0;
        }
        var selectedDeliveryType = $('input[name="Order[delivery_id]"]:checked');
        var selectedSubDeliveryType = $('input[name="Order[sub_delivery_id]"]:checked');
        if (!selectedDeliveryType[0] && !selectedSubDeliveryType[0]) {
            return 0;
        }

        if (selectedSubDeliveryType.length > 0) {
            if (parseInt(selectedSubDeliveryType.data('separate-payment')) || parseFloat(selectedSubDeliveryType.data('free-from')) <= cartTotalCost) {
                return 0;
            } else {
                return parseFloat(selectedSubDeliveryType.data('price'));
            }
        } else if(selectedDeliveryType.length > 0) {
            if (parseInt(selectedDeliveryType.data('separate-payment')) || parseFloat(selectedDeliveryType.data('free-from')) <= cartTotalCost) {
                return 0;
            } else {
                return parseFloat(selectedDeliveryType.data('price'));
            }
        }
    }

    function updateShippingCost() {
        if (getShippingCost() > 0) {
            shippingCostElement
                .html(getShippingCost())
                .parents('.cart-total')
                .removeClass('hidden');
        } else {
            shippingCostElement
                .parents('.cart-total')
                .addClass('hidden');
        }
        updateFullCostWithShipping();
    }

    function updateFullCostWithShipping() {
        var costRes = getShippingCost() + getCartTotalCost();
        if (costRes > 0) {
            cartFullCostWithShippingElement.html(number_format(costRes, 2, '.', ' '));
        }
    }

    function updateAllCosts() {
        updateCartTotalCost();
    }

    function updatePrice() {
        var _basePrice = basePrice;
        var variants = [];
        var varElements = $('select[name="ProductVariant[]"]');
        /* выбираем вариант, меняющий базовую цену максимально*/
        var hasBasePriceVariant = false;
        $.each(varElements, function (index, elem) {
            var varId = elem.value;
            if (varId) {
                var option = $(elem).find('option[value="' + varId + '"]');
                var variant = {amount: option.data('amount'), type: option.data('type')};
                switch (variant.type) {
                    case 2: // base price
                        // еще не было варианта
                        if (!hasBasePriceVariant) {
                            _basePrice = variant.amount;
                            hasBasePriceVariant = true;
                        }
                        else {
                            if (_basePrice < variant.amount) {
                                _basePrice = variant.amount;
                            }
                        }
                        break;
                }
            }
        });
        var newPrice = _basePrice;
        $.each(varElements, function (index, elem) {
            var varId = elem.value;
            if (varId) {
                var option = $(elem).find('option[value="' + varId + '"]');
                var variant = {amount: option.data('amount'), type: option.data('type')};
                variants.push(variant);
                switch (variant.type) {
                    case 0: // sum
                        newPrice += variant.amount;
                        break;
                    case 1: // percent
                        newPrice += _basePrice * ( variant.amount / 100);
                        break;
                }
            }
        });

        var price = parseFloat(newPrice.toFixed(2));
        priceElement.html(price);
        $('#product-result-price').text(price);
        $('#product-total-price').text(price * parseInt($('#product-quantity').text()));
    }

    function updateCartWidget() {
        $(cartWidgetSelector).load($('.js-cart-widget').data('cart-widget-url'), function () {
            miniCartListeners();
        });
    }

    function getCoupons() {
        var coupons = [];
        $.each($('.coupon-input'), function (index, elem) {
            var $elem = $(elem);
            coupons.push({
                code: $elem.data('code'),
                name: $elem.data('name'),
                value: $elem.data('value'),
                type: $elem.data('type'),
                min_order_price: $elem.data('min-order-price'),
                free_shipping: $elem.data('free-shipping')
            })
        });
        return coupons;
    }

    function changePositionQuantity(productId, quantity) {
        var data = {'quantity': quantity, 'id': productId};
        data[yupeTokenName] = yupeToken;
        $.ajax({
            url: yupeCartUpdateUrl,
            type: 'post',
            data: data,
            dataType: 'json',
            success: function (data) {
                if (data.result) {
                    updateCartWidget();
                }
                else {
                    showNotify(this, 'danger', data.data);
                }
            }
        });
    }

    /* анимация перемещения товара  в корзину */
    function moveCartProduct(elem){
        var product = elem.parents('.js-product-item').find(".js-product-image");
        var leftCart = $(".header-bot .js-shopping-cart-widget").offset().left;
        var topCart = $(".header-bot .js-shopping-cart-widget").offset().top;

        if($(".header-fix-content").hasClass('active')){
            leftCart = $(".header-fix .js-shopping-cart-widget").offset().left;
            topCart = $(".header-fix .js-shopping-cart-widget").offset().top;
        }

        $(product).clone().css({
            'position' : 'absolute',
            'z-index' : '9999',
            'opacity' : '0.5',
            top: $(product).offset().top,
            left:$(product).offset().left,
            width: 240
        }).appendTo("body").animate({
            opacity: 0.1,
            left: leftCart,
            top: topCart,
            width: 20}, 1000, function() {
            $(this).remove();
        });
    }
