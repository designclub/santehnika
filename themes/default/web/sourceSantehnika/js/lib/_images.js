/**********************************************/
/*
 * Действия с изображениями
*/

$(document).ready(function() {
    /**********************************************/
    /*
     * Pагрузка изображений в зависимости от разрешения экрана
    */
    if($('img,picture').hasClass('js-load-img')){
        $('.js-load-img').each(function(){
            lazySlideImg($(this));
        });
    }

    function lazySlideImg(img){
        var imgSrc;
        var flag = false;

        var source = false;
        var sourceSrc;
        if(img.hasClass('js-load-picture')){
            source = $(img.find('source'));
            img = $(img.find('img'));
        }

        var innerWidth = window.innerWidth;
        if(innerWidth > 1900){
            if(!img.hasClass('img-big')){
                img.addClass('img-big').removeClass('img-md img-sm img-xs');
                imgSrc = img.data('img-big');

                if(source){
                    source.addClass('img-big').removeClass('img-md img-sm img-xs');
                    sourceSrc = source.data('img-big');
                }

                flag = true;
            }
        } else if(innerWidth > 640){
            if(!img.hasClass('img-md')){
                img.addClass('img-md').removeClass('img-big img-sm img-xs');
                imgSrc = img.data('img-md');

                if(source){
                    source.addClass('img-md').removeClass('img-big img-sm img-xs');
                    sourceSrc = source.data('img-md');
                }

                flag = true;
            }
        } else if(innerWidth > 480){
            if(!img.hasClass('img-sm')){
                img.addClass('img-sm').removeClass('img-big img-md img-xs');
                imgSrc = img.data('img-sm');

                if(source){
                    source.addClass('img-sm').removeClass('img-big img-md img-xs');
                    sourceSrc = source.data('img-sm');
                }

                flag = true;
            }
        } else {
            if(!img.hasClass('img-xs')){
                img.addClass('img-xs').removeClass('img-big img-md img-sm');
                imgSrc = img.data('img-xs');

                if(source){
                    source.addClass('img-xs').removeClass('img-big img-md img-sm');
                    sourceSrc = source.data('img-xs');
                }

                flag = true;
            }
        }

        if(flag){
            img.attr('src', imgSrc);
            if(source){
                source.attr('srcset', sourceSrc);
            }
        }
    }

    /**********************************************/
    /*
     * Смена фоток при наведение на карточку
    */
    imageDotsHover();
    /**********************************************/
});
/**********************************************/

function imageDotsHover() {
    if($('div').hasClass('js-images-list')){
        $(".js-images-list").hover(function(){
            var parent = $(this).parents('.js-product-item');
            var src =  $(this).data("src");
            var srcset =  $(this).data("srcset");
            var key =  $(this).data("key");
            
            parent.find('.js-product-image source').attr('srcset', srcset);
            parent.find('.js-product-image img').attr('src', src);
            
            parent.find('.js-dots-list').removeClass('active');
            parent.find('.js-dots-list[data-key='+key+']').addClass('active');
        }, function(){
            var key =  0;
            var parent = $(this).parents('.js-product-item');
            var src = parent.find('.js-images-list[data-key='+key+']').data("src");
            var srcset = parent.find('.js-images-list[data-key='+key+']').data("srcset");

            parent.find('.js-product-image source').attr('srcset', srcset);
            parent.find('.js-product-image img').attr('src', src);
            
            parent.find('.js-dots-list').removeClass('active');
            parent.find('.js-dots-list[data-key='+key+']').addClass('active');
        });
    }
}