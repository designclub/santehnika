/**********************************************/
/*
 * Действия при изменении окна 
*/
$(document).ready(function() {
	$(window).resize(function () {
	    if($('img,picture').hasClass('js-load-img')){
	        $('.js-load-img').each(function(){
	            lazySlideImg($(this));
	        });
	    }
	    filterMore();
	});
});