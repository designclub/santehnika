/**********************************************/
/*
 * Слайдеры
*/

$(document).ready(function() {
    /* Карусель в шапке */
    if($('div').hasClass('carousel-header-box')){
        var carouselHeader = $('.carousel-header-box');

        carouselHeader.slick({
            fade: true,
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: false,
            autoplaySpeed: 5000,
            dots: true,
            arrows: true,
            responsive: [
                {
                    breakpoint: 769,
                    settings: {
                       arrows: false,
                       dots: true,
                    }
                },
            ]
        });
    }

    /* Главный слайд */
    if($('div').hasClass('slide-box-carousel')){
        var slide = $('.slide-box-carousel');

        slide.slick({
            fade: true,
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: false,
            autoplaySpeed: 5000,
            dots: true,
            arrows: true,
            responsive: [
                {
                    breakpoint: 769,
                    settings: {
                       arrows: false,
                       dots: true,
                    }
                },
            ]
        });
    }

    /* Бренды производителей */
    if($('div').hasClass('producer-carousel')){
        $('.producer-carousel').slick({
            fade: false,
            infinite: true,
            slidesToShow: 8,
            slidesToScroll: 1,
            autoplay: false,
            autoplaySpeed: 5000,
            dots: false,
            arrows: true,
            responsive: [
                {
                    breakpoint: 1241,
                    settings: {
                        dots: false,
                        arrows: true,
                        slidesToShow: 7,
                        slidesToScroll: 1,
                    }
                },
                {
                    breakpoint: 1001,
                    settings: {
                        dots: false,
                        arrows: true,
                        slidesToShow: 5,
                        slidesToScroll: 1,
                    }
                },
                {
                    breakpoint: 641,
                    settings: {
                        dots: false,
                        arrows: true,
                        slidesToShow: 4,
                        slidesToScroll: 1,
                    }
                },
                {
                    breakpoint: 481,
                    settings: {
                        dots: false,
                        arrows: true,
                        slidesToShow: 2,
                        slidesToScroll: 1,
                    }
                },
            ]
        });
    }

    /* Карусель продуктов */
    

    /* Акции и скидки */
    if($('div').hasClass('stock-box-carousel')){
        $('.stock-box-carousel').slick({
            fade: false,
            infinite: true,
            slidesToShow: 2,
            slidesToScroll: 1,
            autoplay: false,
            autoplaySpeed: 5000,
            dots: false,
            arrows: true,
            responsive: [
                {
                    breakpoint: 1001,
                    settings: {
                        dots: false,
                        arrows: true,
                        slidesToShow: 1,
                        slidesToScroll: 1,
                    }
                },
            ]
        });
    }

    /* Акции и скидки */
    if($('div').hasClass('news-box-carousel')){
        $('.news-box-carousel').slick({
            fade: false,
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 1,
            autoplay: false,
            autoplaySpeed: 5000,
            dots: false,
            arrows: true,
            responsive: [
                {
                    breakpoint: 1001,
                    settings: {
                        dots: false,
                        arrows: true,
                        slidesToShow: 2,
                        slidesToScroll: 1,
                    }
                },
                {
                    breakpoint: 641,
                    settings: {
                        dots: false,
                        arrows: true,
                        slidesToShow: 1,
                        slidesToScroll: 1,
                    }
                },
            ]
        });
    }

    slickIn();
});

function slickIn() {
    if($('div').hasClass('product-box-carousel')){
        $('.product-box-carousel').slick({
            fade: false,
            infinite: true,
            slidesToShow: 4,
            slidesToScroll: 1,
            autoplay: false,
            autoplaySpeed: 5000,
            dots: false,
            arrows: true,
            responsive: [
                {
                    breakpoint: 1241,
                    settings: {
                        dots: false,
                        arrows: true,
                        slidesToShow: 3,
                        slidesToScroll: 1,
                    }
                },
                {
                    breakpoint: 769,
                    settings: {
                        dots: false,
                        arrows: true,
                        slidesToShow: 2,
                        slidesToScroll: 1,
                    }
                },
                {
                    breakpoint: 641,
                    settings: {
                        dots: false,
                        arrows: true,
                        slidesToShow: 2,
                        slidesToScroll: 1,
                    }
                },
                /*{
                    breakpoint: 481,
                    settings: {
                        dots: false,
                        arrows: true,
                        slidesToShow: 1,
                        slidesToScroll: 1,
                    }
                },*/
            ]
        });
    }
}