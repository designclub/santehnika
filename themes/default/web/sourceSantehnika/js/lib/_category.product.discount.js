/**********************************************/
/*
 * Действия с формами
*/
$(document).ready(function() {
    if ($('div').hasClass('js-category-productDiscount')) {
		var block = $('.js-category-productDiscount');
		var event = jQuery.Event('click');
		if(!block.hasClass('active')){
			var loadFirstProduct = setTimeout(function(){
				block.addClass('active');
	            $('.js-productDiscount-item:eq(0)').trigger(event);
			}, 1000);
		}
    }

	$(document).on('scroll',function(){
        if ($('div').hasClass('js-category-productDiscount')) {
        	var block = $('.js-category-productDiscount');
	        var docScroll = $(document).scrollTop();
	        var divOffset = block.position().top;
	        var innerHeight = $(window).innerHeight();
	        var event = jQuery.Event('click');
	        if(!block.hasClass('active')){
	        	clearTimeout(loadFirstProduct);
		        if((docScroll + innerHeight) > divOffset){
		        	block.addClass('active');
		            $('.js-productDiscount-item:eq(0)').trigger(event);
		            $(document).off('scroll');
		        }
	        }
	    }
    });


    $(document).delegate('.js-productDiscount-item', 'click',function(e){
		$('.js-productDiscount-item').removeClass('active');
		$(this).addClass('active');
		$('.js-productDiscount-content').addClass('productDiscount-loading');
		$.ajax({
			url: '/productDiscount',
			type: 'post',
			data: {
                YUPE_TOKEN:  yupeToken,
                id: $(this).data('id')
            },
			success: function(html) {
			    $('.js-productDiscount-content').html(html);
			    slickIn();
			    imageDotsHover();
				$('.js-productDiscount-content').removeClass('productDiscount-loading');
			}
		});
		return false;
    });

	$(document).delegate('.js-moreProductDiscount-nav', 'click', function(){
	    var elem = $('.js-productDiscount-nav');
	    var $this = $(this).find('.js-moreProductDiscount-text');
	    var textShow = $this.attr('data-text');
	    var textHide = $this.text();
	    if(elem.hasClass('active')){
	        elem.removeClass('active');
	    }else{
	        elem.addClass('active');
	    }
	    $this.text(textShow).attr('data-text', textHide);
	    return false;
	});

	filterMore();
});

function filterMore(){
    if($('div').hasClass('js-productDiscount-nav')){
        var contWidth = $(window).innerWidth();
        if(contWidth > 1000){
            var flag = false;
            var parent = $('.js-productDiscount-nav');
            var elem = $('.js-productDiscount-list');
            var innerWidth = parent.width();
            var width = elem.innerWidth();
            
            if(flag == false){
                elem.attr('data-width', width);
                flag = true;
            }
            width = elem.data('width');

            if(innerWidth < width){
                parent.addClass('btn-active');
            }
            else{
                parent.removeClass('btn-active');
            }

            visibleItemCount(parent);
        }
    }
}

/* Подсчет кол-ва скрытых категорий */
function visibleItemCount(parent) {
	var h = parent.height();
	var hiddenElem = [];
	parent.find('.js-productDiscount-item').each(function() {
	  	if ($(this).position().top > h){
	    	hiddenElem.push($(this));
	  	}
	});
	$('.js-count-cat').html(hiddenElem.length + ' ' + declOfNum(hiddenElem.length, ['категория', 'категории', 'категорий']));
}
/* Склонение слова */
function declOfNum(number, words) {  
    return words[(number % 100 > 4 && number % 100 < 20) ? 2 : [2, 0, 1, 1, 1, 2][(number % 10 < 5) ? number % 10 : 5]];
}