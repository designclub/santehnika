$(document).ready(function() {
    $('.js-carousel-header').on('click', function() {
        $(this).parents('.carousel-header-section').hide();
    });
    /*
     * Menu при адаптации
    */
    // Событие открывания меню на телефонах
    $('.js-menu-mobileNav').on('click', function() {
        if($(this).hasClass('active')){
            $('html').removeClass('htmlmenu');
            $('body').removeClass('bodymenu');
            $(this).removeClass('active');
            $(".mobileNav, .mobileNav-box, .mobileNav__icon-close").removeClass('active');
        } else {
            $('html').addClass('htmlmenu');
            $('body').addClass('bodymenu');
            $(this).addClass('active');
            $(".mobileNav").addClass('active');
            $(".mobileNav-box").addClass('active');
            $(".mobileNav__icon-close").addClass('active');
        }
        return false;
    });
    $('.mobileNav').on('click', function(e){
        console.log($(e.target));
        if($(e.target).hasClass('mobileNav__icon-close') || $(e.target).parents('.mobileNav__icon-close').length > 0 || $(e.target).hasClass('mobileNav')){
            $('html').removeClass('htmlmenu');
            $('body').removeClass('bodymenu');
            $(".js-menu-mobileNav, .mobileNav, .mobileNav-box, .mobileNav__icon-close").removeClass('active');
        }
    });
    /**********************************************/
    /* Открывание каталога на телефоне */
    $('.js-menu-fix').on('click', function(){
        var id = $(this).data('menu');
        if($(this).hasClass('active')){
            $('#search-form-Modal').modal('hide');
            $('html').removeClass('htmlmenu');
            $('body').removeClass('bodymenu');
            $(this).removeClass('active');
            $(".menu-fix, .menu-fix__box, .menu-fix__icon-close").removeClass('active');
        } else {
            $('#search-form-Modal').modal('hide');
            $(".menu-fix, .menu-fix__box, .menu-fix__icon-close").removeClass('active');
            $('html').addClass('htmlmenu');
            $('body').addClass('bodymenu');
            $(this).addClass('active');
            $(id).addClass('active');
            $(id).find(".menu-fix__box").addClass('active');
            $(id).find(".menu-fix__icon-close").addClass('active');
        }

        return false;
    });
    /* Кнопка закрыть меню */
    $('.menu-fix').on('click', function(e){
        if($(e.target).hasClass('menu-fix__icon-close') || $(e.target).parents('.menu-fix__icon-close').length > 0 ){
            $('html').removeClass('htmlmenu');
            $('body').removeClass('bodymenu');
            $(".js-menu-fix, .menu-fix__icon-close, .menu-fix, .menu-fix__box").removeClass('active');
            return false;
        }
    });
    /**********************************************/
    /* Открываание поиска на телефоне */
    $('.js-search-modal-but').on('click', function(){
        var modal = $(this).data('modal');
        if($(this).hasClass('active')){
            $(modal).show('hide');
        } else {
            $(".js-menu-fix, .menu-fix__icon-close, .menu-fix, .menu-fix__box").removeClass('active');
            $(modal).modal('show');
        }
        return false;
    });
    /**********************************************/


    /**********************************************/
    /*
     * Кнопка Каталог в шапке
    */
    // Событие открывания каталога 
    $('.catalog-menu-icon').on('click', function(){
        if($(this).hasClass('active')){
            $(this).removeClass('active');
            $(".header-catalog").removeClass('active');
        } else {
            $(this).addClass('active');
            $(".header-catalog").addClass('active');
        }
        return false;
    });
    // Кнопка закрыть меню
    $(document).delegate('body', 'click', function(e){
        if(!$(e.target).hasClass('header-catalog') && $(e.target).parents('.header-catalog').length < 0){
            $('.catalog-menu-icon').removeClass('active');
            $('.header-catalog').removeClass('active');
        }
    });   
    /**********************************************/ 


    /**********************************************/
    /*
     *** Кнопка больше информации СЕО-ТЕКСТ ***
    */
    $('.js-seo-txt-section-link').on('click', function(){
        var parent = $('.js-seo-txt-section');
        var txt = $(this).data('text');
        var txt_hidden = $(this).html();
        var height = parent.find('.js-seo-txt-more').height();
        if($(this).hasClass('active')){
            $(this).removeClass('active');
            parent.removeClass('active').attr('style', '');
            $(this).html(txt).data('text', txt_hidden);
        } else {
            $(this).addClass('active');
            parent.addClass('active').css('max-height', height);
            $(this).html(txt).data('text', txt_hidden);
        }
        return false;
    });
    /**********************************************/
    

    /**********************************************/
    /*
     * Кнопка показать все характеристики
    */
    $(document).delegate('.js-more-attributes', 'click', function(){
        var span = $(this).find('span');
        var text = $(this).data('text');

        if($(this).hasClass('active')){
            $(this).removeClass('active');
            $('.js-product-attributes').removeClass('active').find('.js-product-attributes-item.hidden2').addClass('hidden').removeClass('hidden2');
            $(this).data('text', span.text());
            span.text(text);
        } else {
            $(this).addClass('active');
            $('.js-product-attributes').addClass('active').find('.js-product-attributes-item.hidden').addClass('hidden2').removeClass('hidden');
            $(this).data('text', span.text());
            span.text(text);
        }
        return false;
    });

    /*$(window).scroll(function(){
        fixMenu();
    });
    
    fixMenu()

    function fixMenu() {
        var $menu = $("header");
        if ($(this).scrollTop() <= 120){
            $menu.removeClass("header-fix");
        } else if($(this).scrollTop() > 120) {
            $menu.addClass("header-fix");
        }
    }
    */

    var menumain = $('.header-top .menu .menu-main').html();
    $('.mobileNav .menu-mobile').append(menumain);

    $('.mobileNav .menu-mobile li ul').wrap('<div class="menu-mobile-wrapper"></div>');
    $('.mobileNav .menu-mobile li a').click(function(){
        var parent = $(this).parent();
        if(parent.hasClass('submenuItem')){
            parent.find(".menu-mobile-wrapper:first").addClass('active').prepend('<a class="prevLink" href="#">Назад</a>');
            return false;
        }
    });

    $('.mobileNav .menu-mobile li.listItemParent').each(function(i){
        if($(this).hasClass('submenuItem')){
            var el = $(this).find('a:first');
            var link_href = el.attr('href');
            link = "<a class='listItem-nav__link' href='"+link_href+"'>"+el.html()+"</a>";
            $(this).find(".menu-mobile-wrapper ul:eq(0)").prepend("<li class='listItem listItem-nav'>" + link + "</li>");
            appendLi($(this).find(".menu-mobile-wrapper ul:eq(0) > li.submenuItem"), link);
        }
    });

    function appendLi(element, link_prev){
        var link_res;
        var count = 1;
        element.each(function(index){
            link_res = link_prev;
            if($(this).hasClass('listItemFloor')){
                link_prev = '<a class="listItem-nav__link" href="#">Магазины</a>'
            }
            if($(this).hasClass('submenuItem')){
                var el = $(this).find('a:first');
                var link_href = el.attr('href');
                var link_active = "<a class='listItem-nav__link' href='"+link_href+"'>"+el.html()+"</a>";
                link_res += link_active;
                $(this).find(".menu-mobile-wrapper ul:eq(0)").prepend("<li class='listItem listItem-nav'>" + link_prev + link_active + "</li>");
                appendLi($(this).find(".menu-mobile-wrapper ul:eq(0) > li.submenuItem"), link_res);
            }
        })
    }

    $(document).delegate('.mobileNav .menu-mobile .prevLink', 'click', function(){
        var el = $(this);
        var parent = $(this).parent();
        if(parent.hasClass('active')){
            parent.removeClass('active');
            setTimeout(function(){
                el.remove();
            }, 600);
        }
        return false;
    });
});

// @prepros-append "./lib/_carousel.js"
// @prepros-append "./lib/_images.js"
// @prepros-append "./lib/_form.js"
// @prepros-append "./lib/_category.product.discount.js"
// @prepros-append "./lib/_resize.js"
// @prepros-append "./lib/slick.js"
// @prepros-append "./lib/jquery.inputmask.js"
// @prepros-append "./lib/fancybox.js"
// @prepros-append "./lib/jquery.cookie.js"
// @prepros-append "cart.js"