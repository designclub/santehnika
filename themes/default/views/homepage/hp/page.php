<?php
/** @var Page $page */

if ($page->layout) {
    $this->layout = "//layouts/{$page->layout}";
}

$this->title = $page->meta_title ?: $page->title;
$this->breadcrumbs = [
    Yii::t('HomepageModule.homepage', 'Pages'),
    $page->title
];
$this->description = !empty($page->meta_description) ? $page->meta_description : Yii::app()->getModule('yupe')->siteDescription;
$this->keywords = !empty($page->meta_keywords) ? $page->meta_keywords : Yii::app()->getModule('yupe')->siteKeyWords;

?>

<div class="slider-home">
	<div class="content">
		<?php $this->widget('application.modules.slider.widgets.SliderWidget', [
			'category_id' => 1
		]); ?>
	</div>
</div>

<div class="producer-home">
	<div class="content">
		<?php $this->widget('application.modules.store.widgets.ProducersWidget', [
			'limit' => 20,
			'is_home' => true,
			'view' => 'producer-home'
		]); ?>
	</div>
</div>

<?php $this->widget('application.modules.store.widgets.ProductDiscountWidget'); ?>

<?php $this->widget('application.modules.store.widgets.CatalogWidget', [
	'is_home' => true,
	'view' => 'catalog-home'
]); ?>

<?php $this->widget('application.modules.news.widgets.NewsWidget', [
	'categories' => Yii::app()->getModule('news')->stockId,
	'limit' => 6,
	'view' => 'stock-home'
]); ?>


<?php $this->widget('application.modules.news.widgets.NewsWidget', [
	'categories' => Yii::app()->getModule('news')->newsId,
	'limit' => 6,
	'view' => 'news-home'
]); ?>
