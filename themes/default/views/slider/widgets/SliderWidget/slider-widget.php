<div class="slide-box slide-box-carousel slick-slider">
	<?php foreach ($models as $key => $data): ?>
	    <div class="slide-box__item">
			<div class="slide-box__img">
				<picture>
		            <source media="(min-width: 641px)" srcset="<?= $data->getImageUrlWebp(1320,400, true, null, 'image'); ?>" type="image/webp">
		            <source media="(min-width: 641px)" srcset="<?= $data->getImageNewUrl(1320,400, true, null, 'image'); ?>">

	            	<source media="(min-width: 401px)" srcset="<?= $data->getImageUrlWebp(640,400, false, null, 'image'); ?>" type="image/webp">
		            <source media="(min-width: 401px)" srcset="<?= $data->getImageNewUrl(640,400, false, null, 'image'); ?>">

		            <source media="(min-width: 1px)" srcset="<?= $data->getImageUrlWebp(480,400, false, null, 'image'); ?>" type="image/webp">
		            <source media="(min-width: 1px)" srcset="<?= $data->getImageNewUrl(480,400, false, null, 'image'); ?>">
					<!-- , Yii::app()->getModule('slider')->uploadPath . "/mobile"
					, Yii::app()->getModule('slider')->uploadPath . "/mobile" -->

		            <img src="<?= $data->getImageNewUrl(1320,400, true, null, 'image'); ?>" alt="">
		        </picture>
			</div>
	    </div>
	<?php endforeach ?>
</div>