<?php if($models) : ?>
	<div class="carousel-header-section">
		<div class="content">
			<div class="carousel-header-box slick-slider">
				<?php foreach ($models as $key => $data): ?>
					<div>
					    <div class="carousel-header-box__item fl fl-al-it-c fl-ju-co-sp-b">
					    	<?php if($data->name) : ?>
								<div class="carousel-header-box__name">
									<?= $data->name; ?>
								</div>
							<?php endif; ?>
					    	<?php if($data->image) : ?>
								<div class="carousel-header-box__img box-style-img">
									<picture>
							            <source media="(min-width: 1px)" srcset="<?= $data->getImageUrlWebp(480,400, false, null, 'image'); ?>" type="image/webp">
							            <source media="(min-width: 1px)" srcset="<?= $data->getImageNewUrl(480,400, false, null, 'image'); ?>">
							            <img src="<?= $data->getImageNewUrl(1320,400, true, null, 'image'); ?>" alt="">
							        </picture>
								</div>
							<?php endif; ?>
							<div class="fl fl-al-it-c fl-ju-co-sp-b">
								<?php if($data->button_link && $data->button_name) : ?>
									<div class="carousel-header-box__but">
										<a class="but but-orange" href="<?= $data->button_link; ?>">
											<?= $data->button_name; ?>
										</a>
									</div>
								<?php endif; ?>
								<?php if($data->description_short) : ?>
									<div class="carousel-header-box__desc">
										<?= $data->description_short; ?>
									</div>
								<?php endif; ?>
							</div>
					    </div>
				    </div>
				<?php endforeach ?>
			</div>
			<div class="carousel-header-section__close js-carousel-header"><div></div></div>
		</div>
	</div>
<?php endif; ?>