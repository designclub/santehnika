<?php
/* @var $model Page */
/* @var $this PageController */

if ($model->layout) {
    $this->layout = "//layouts/{$model->layout}";
}

$this->title = $model->meta_title ?: $model->title;
$this->breadcrumbs = $this->getBreadCrumbs();
$this->description = $model->meta_description ?: Yii::app()->getModule('yupe')->siteDescription;
$this->keywords = $model->meta_keywords ?: Yii::app()->getModule('yupe')->siteKeyWords;
?>

<div class="page-content txt-style">
    <div class="content">
    	<?php $this->widget('application.components.MyTbBreadcrumbs', [
            'links' => $this->breadcrumbs,
        ]); ?>
        <div class="delivery-page-content fl fl-wr-w">
        	<div class="delivery-page-content__info">
				<h1><?= $model->title; ?></h1>
				<?= $model->body; ?>
	        </div>
        	<div class="delivery-page-content__img"></div>
        </div>
    </div>
</div>
