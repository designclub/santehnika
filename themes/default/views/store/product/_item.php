<?php $img = $data->getImageNewUrl(290, 245,false,null,'image'); ?>
<?php $imgwebp = $data->getImageUrlWebp(290, 245,false,null,'image'); ?>


<?php $images = $data->getImages(); ?>

<div class="product-box__item js-product-item">
    <div class="product-box__favorite product-but-favorite fl fl-al-it-c fl-ju-co-c <?= (Yii::app()->getComponent('favorite')->has($data->id)) ? 'active' : ''; ?>">
        <?php $this->widget('application.modules.favorite.widgets.FavoriteControl', [
            'product' => $data,
            'view' => "favorite-item"
        ]);?>
    </div>
    <?php if(!empty($data->getProductBadgeList())): ?>
        <div class="product-box-badge">
            <?php foreach ($data->getProductBadgeList() as $key => $badge) : ?>
                <div class="product-box-badge__item fl fl-al-it-c fl-ju-co-c" style="color: <?= $badge['color']; ?>; <?= ($badge['background']) ? 'background: {$badge[\'background\']}' : ''; ?>">
                    <span><?= $badge['name']; ?></span>
                </div>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
    <div class="product-box__img fl fl-al-it-c fl-ju-co-c">
        <a class="" href="<?= ProductHelper::getUrl($data); ?>">
            <picture class="fl fl-al-it-c fl-ju-co-c js-product-image">
                <source srcset="<?= $imgwebp; ?>" type="image/webp">
                <img src="<?= $img; ?>" alt="<?= CHtml::encode($data->getImageAlt()); ?>" title="<?= CHtml::encode($data->getImageTitle()); ?>">
            </picture>
            <?php if($images) : ?>
                <div class="imagesPr-list js-imagesPr-list fl">
                    <div 
                        class="imagesPr-list__item js-images-list" 
                        data-key="0"
                        data-src="<?= $img; ?>"
                        data-srcset="<?= $imgwebp; ?>">
                    </div>
                    <?php foreach ($images as $key => $image) : ?>
                        <?php if(isset($isBig)) : ?>
                            <?php $img = $image->getImageNewUrl(620, 390,false,null,'name'); ?>
                            <?php $imgwebp = $image->getImageUrlWebp(620, 390,false,null,'name'); ?>
                        <?php elseif(isset($isSm)) : ?>
                            <?php $img = $image->getImageNewUrl(400, 280,false,null,'name'); ?>
                            <?php $imgwebp = $image->getImageUrlWebp(400, 280,false,null,'name'); ?>
                        <?php else : ?>
                            <?php $img = $image->getImageNewUrl(290, 202,false,null,'name'); ?>
                            <?php $imgwebp = $image->getImageUrlWebp(290, 202,false,null,'name'); ?>
                        <?php endif; ?>
                        <div 
                            class="imagesPr-list__item js-images-list" 
                            data-key="<?= ($key + 1); ?>"
                            data-src="<?= $img; ?>"
                            data-srcset="<?= $imgwebp; ?>">
                        </div>
                    <?php endforeach ?>
                </div>
            <?php endif; ?>
        </a>
    </div>
    <div class="product-box__info fl fl-di-c fl-ju-co-sp-b">
        <div>
            <?php if($images) : ?>
                <div class="imagesDots-list fl fl-ju-co-c">
                    <div 
                        class="imagesDots-list__item js-dots-list" 
                        data-key="0">
                    </div>
                    <?php foreach ($images as $key => $image) : ?>
                        <div 
                            class="imagesDots-list__item js-dots-list" 
                            data-key="<?= ($key + 1); ?>">
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
            <?php if($isdelete) : ?>
                <div class="favorite-box__delete fl fl-ju-co-fl-e">
                    <div class="favorite-delete fl fl-al-it-c fl-ju-co-c" data-id="<?= $data->id;?>">
                        <i class="fa fa-fw fa-trash-o"></i>
                    </div>
                </div>
            <?php endif; ?>
            <div class="product-box__price product-price <?= ($data->hasDiscount()) ? 'product-price-new' : ''; ?> fl fl-wr-w fl-al-it-fl-e">
                <?php if ($data->hasDiscount()) : ?>
                    <div class="product-price__discountPercent">
                        <?= ($data->discount) ? '<span class="discount-percent fl fl-al-it-c fl-ju-co-c">-' . str_replace('.00', '', number_format($data->discount, 2, '.', ' ')) . '%</span>' : ''; ?>
                    </div>
                <?php endif; ?>
                <span class="product-price__res">
                    <span class="price-result" id="result-price<?= $data->id?>">
                        <?= str_replace('.00', '', number_format($data->getResultPrice(), 2, '.', ' ')); ?>
                    </span>
                    <span class="ruble"><i class="icon-ruble-res"></i></span>
                </span>
                <?php if ($data->hasDiscount()) : ?>
                    <span class="product-price__old">
                        <span class="strikethrough">
                            <span class="price-old">
                                <?= str_replace('.00', '', number_format($data->getBasePrice(), 2, '.', ' ')); ?>
                            </span>
                            <span class="ruble"><i class="icon-ruble-old"></i></span>
                        </span>
                    </span>
                <?php endif; ?>
            </div>
            <div class="product-box__name">
                <a class="product-name" href="<?= ProductHelper::getUrl($data); ?>">
                    <?= CHtml::encode($data->getName()); ?>
                </a>
            </div>
        </div>
        <div class="product-box__bottom fl fl-wr-w">
            <?php if (Yii::app()->hasModule('cart')): ?>
                <div class="product-box__but product-box-but">
                    <a 
                        href="<?= ($data->getIsProductCart()) ? Yii::app()->createUrl('cart/cart/index') : '#'; ?>" 
                        class="but but-blue-gradient js-product-but <?= ($data->getIsProductCart()) ? 'but-go-cart' : 'quick-add-product-to-cart but-svg but-svg-left but-animation'; ?> product-button fl fl-al-it-c fl-ju-co-c" 
                        data-product-id="<?= $data->id; ?>" 
                        data-cart-add-url="<?= Yii::app()->createUrl('/cart/cart/add');?>">
                        <i class="icon icon-load"></i>
                        <?php if($data->getIsProductCart()) : ?>
                            <span>В корзине</span>
                        <?php else : ?>
                            <span>В корзину</span>
                        <?php endif; ?>
                    </a>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>