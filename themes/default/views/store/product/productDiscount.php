<?php 
    $this->widget(
        // 'application.components.MyListView',
        'bootstrap.widgets.TbListView',
    [
        'dataProvider' => $dataProvider,
        'id' => 'productDiscount-listView',
        'itemView' => '_item',
        'template'=>'
            {items}
        ',
        'itemsCssClass' => 'product-box product-box-carousel slick-slider',
        'htmlOptions' => [
            'class' => 'productDiscount-listView'
        ],
        'ajaxUpdate'=>true,
        'enableHistory' => false,
    ]
); ?>