<?php

/* @var $product Product */

$this->title = $product->getMetaTitle();
$this->description = $product->getMetaDescription();
$this->keywords = $product->getMetaKeywords();
$this->canonical = $product->getMetaCanonical();

$mainAssets = Yii::app()->getModule('store')->getAssetsUrl();

$this->breadcrumbs = array_merge(
    [Yii::t("StoreModule.store", 'Catalog') => ['/store/product/index']],
    $product->category ? $product->category->getBreadcrumbs(true) : [],
    [CHtml::encode($product->name)]
);
?>
<div class="page-content product-view-content" xmlns="http://www.w3.org/1999/html" itemscope itemtype="http://schema.org/Product">
    <div class="content">
        <?php $this->widget('application.components.MyTbBreadcrumbs', [
                'links' => $this->breadcrumbs,
        ]); ?>
        <div class="product-view js-product-item fl fl-wr-w fl-ju-co-sp-b">
            <div class="product-view__info">
                <div class="product-view-item">
                    <div class="product-but-favorite <?= (Yii::app()->getComponent('favorite')->has($product->id)) ? 'active' : ''; ?>">
                        <?php $this->widget('application.modules.favorite.widgets.FavoriteControl', [
                            'product' => $product,
                            'view' => "favorite"
                        ]);?>
                    </div>
                </div>
                <h1 itemprop="name"><?= CHtml::encode($product->getTitle()); ?></h1>
                <form action="<?= Yii::app()->createUrl('cart/cart/add'); ?>" method="post" data-max-value='<?= Yii::app()->getModule('store')->controlStockBalances ? $product->getAvailableQuantity() : 1000; ?>'>
                    <input type="hidden" name="Product[id]" value="<?= $product->id; ?>"/>
                    <?= CHtml::hiddenField(
                        Yii::app()->getRequest()->csrfTokenName,
                        Yii::app()->getRequest()->csrfToken
                    ); ?>
                    <div class="product-view__attr product-attr fl fl-di-c">
                        <?php if($product->sku) : ?>
                            <div class="product-attr__item product-attr__sku">
                                <div class="product-attr__inf">Артикул: <span><?= $product->sku; ?></span></div>
                            </div>
                        <?php endif; ?>
                        <?php if($product->producer_id) : ?>
                            <div class="product-attr__item product-attr__city">
                                <div class="product-attr__inf">Страна производитель: <span><?= $product->producer->city->name; ?></span></div>
                            </div>
                        <?php endif; ?>
                    </div>
                    <?php //Цена ?>
                    <div class="product-view__price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                        <input type="hidden" id="base-price" value="<?= round($product->getResultPrice(), 2); ?>"/>
                        <!-- <div class="product-view__header"><span class="product-view-header">Стоимость: </span></div> -->
                        <div class="product-price <?= ($product->hasDiscount()) ? 'product-price-new' : ''; ?> fl fl-wr-w fl-al-it-fl-e">
                            <?php if ($product->hasDiscount()) : ?>
                                <div class="product-price__discountPercent">
                                    <?= ($product->discount) ? '<span class="discount-percent fl fl-al-it-c fl-ju-co-c">-' . str_replace('.00', '', number_format($product->discount, 2, '.', ' ')) . '%</span>' : ''; ?>
                                </div>
                            <?php endif; ?>
                            <span class="product-price__res">
                                <span class="price-result" id="result-price<?= $product->id?>">
                                    <?= str_replace('.00', '', number_format($product->getResultPrice(), 2, '.', ' ')); ?>
                                </span>
                                <span class="ruble"><?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/icon-ruble.svg'); ?></span>
                            </span>
                            <?php if ($product->hasDiscount()) : ?>
                                <span class="product-price__old">
                                    <span class="strikethrough">
                                        <span class="price-old">
                                            <?= str_replace('.00', '', number_format($product->getBasePrice(), 2, '.', ' ')); ?>
                                        </span>
                                        <span class="ruble"><?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/icon-ruble.svg'); ?></span>
                                    </span>
                                </span>
                            <?php endif; ?>
                        </div>
                    </div>
                    <?php if (Yii::app()->hasModule('order')) : ?>
                        <div class="product-view__button product-button fl fl-wr-w fl-al-it-c">
                            <div class="product-button__item product-box-spinput hidden">
                                <input type="hidden" name="Product[id]" value="<?= $product->id; ?>"/>
                                <?php
                                    $minQuantity = 1;
                                    $maxQuantity = Yii::app()->getModule('store')->controlStockBalances ? $product->getAvailableQuantity() : 1000;
                                ?>
                                <span data-min-value='<?= $minQuantity; ?>' data-max-value='<?= $maxQuantity; ?>'
                                      class="spinput js-spinput">
                                    <span class="spinput__minus js-spinput__minus product-box-quantity-decrease"></span>
                                    <input name="Product[quantity]" value="<?= $product->getProductQuanityCart(); ?>" data-product-id="<?= $product->id; ?>" class="spinput__value <?= ($product->getIsProductCart()) ? 'product-box-quantity-addcart' : 'product-box-quantity-input'; ?>"/>
                                    <span class="spinput__plus js-spinput__plus product-box-quantity-increase"></span>
                                </span>
                            </div>
                            <?php if (Yii::app()->hasModule('cart')) : ?>
                                <div class="product-button__item product-box-but">
                                    <!-- <button class="btn but but-burgundy" id="add-product-to-cart">
                                        <?php //= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/cart.svg'); ?>
                                        <span>Купить</span>
                                    </button> -->
                                    <a
                                        href="<?= ($product->getIsProductCart()) ? Yii::app()->createUrl('cart/cart/index') : '#'; ?>"
                                        class="but js-product-but <?= ($product->getIsProductCart()) ? 'but-go-cart' : 'but-animation but-svg but-svg-left quick-add-product-to-cart'; ?> product-button fl fl-al-it-c fl-ju-co-c"
                                        data-product-id="<?= $product->id; ?>"
                                        data-cart-add-url="<?= Yii::app()->createUrl('/cart/cart/add');?>">
                                        <i class="icon icon-load"></i>
                                        <i class="ic-cart"></i>
                                        <?php if ($product->getIsProductCart()) : ?>
                                            <span>В корзине</span>
                                        <?php else : ?>
                                            <span>Купить</span>
                                        <?php endif; ?>
                                    </a>

                                </div>
                            <?php endif; ?>
                        </div>
                        <div class="product-view__hidden hidden">
                            <span id="product-result-price"><?= round($product->getResultPrice(), 2); ?></span> x
                            <span id="product-quantity">1</span> =
                            <span id="product-total-price"><?= round($product->getResultPrice(), 2); ?></span>
                            <span class="ruble"> <?= Yii::t("StoreModule.store", Yii::app()->getModule('store')->currency); ?></span>
                        </div>
                    <?php endif; ?>

                    <div class="product-view__text-inf text-inf">
                        <?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
                            'id' => 12
                        ]); ?>
                    </div>
                </form>
            </div>
            <?php $images = $product->getImages(); ?>
            <div class="product-view__img <?= ($images) ? 'product-view__img_2' : ''; ?> fl fl-ju-co-sp-b">
                <div class="image-preview slick-slider">
                    <div>
                        <div class="image-preview__img">
                            <a class="fl fl-al-it-c fl-ju-co-c" data-fancybox="image" href="<?= StoreImage::product($product); ?>">
                                <img
                                    class="gallery-image js-product-image"
                                    src="<?= StoreImage::product($product); ?>"
                                    itemprop="image"
                                />
                            </a>
                        </div>
                    </div>
                    <?php foreach ($images as $key => $image) : ?>
                        <div>
                            <div class="image-preview__img">
                                <a class="fl fl-al-it-c fl-ju-co-c" data-fancybox="image" href="<?= $image->getImageUrl(); ?>">
                                    <?= CHtml::image($image->getImageUrl(), '', [
                                        'class' => 'gallery-image'
                                    ])?>
                                </a>
                            </div>
                        </div>
                    <?php endforeach ?>
                </div>

                <!-- Миниатюры -->
                <?php if (count($images) > 0) : ?>
                    <div class="image-thumbnail slick-slider">
                        <div>
                            <div class="image-thumbnail__item">
                                <div class="fl fl-al-it-c fl-ju-co-c image-thumbnail__img">
                                    <img src="<?= StoreImage::product($product); ?>" />
                                </div>
                            </div>
                        </div>
                        <?php foreach ($images as $key => $image) : ?>
                            <div>
                                <div class="image-thumbnail__item">
                                    <div class="fl fl-al-it-c fl-ju-co-c image-thumbnail__img">
                                        <?= CHtml::image($image->getImageUrl(), '', ['style'=>''])?>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
        <div class="clearfix"></div>

        <?php $attributes = $product->getAttributeGroups() ?>
        <div class="product-view-section <?= ($attributes) ? '' : 'product-view-section-2'; ?> fl fl-wr-w fl-ju-co-sp-b">
            <?php if($attributes) : ?>
                <div class="product-view-section__item product-view-section__item_attributes">
                    <h2>Характеристики</h2>
                    <table class="table product-view-attributes js-product-attributes">
                        <?php $count = 0; ?>
                        <?php foreach ($attributes as $groupName => $items) : ?>
                            <?php foreach ($items as $attribute) : ?>
                                <?php
                                    $value = AttributeRender::renderValue($attribute, $product->attribute($attribute));
                                    if (empty($value)) {
                                        continue;
                                    }
                                ?>
                                <tr class="product-view-attributes__item js-product-attributes-item <?= ($count > 8) ? 'hidden' : ''; ?>">
                                    <td class="product-view-attributes__name"><span><?= CHtml::encode($attribute->title); ?></span></td>
                                    <td class="product-view-attributes__val"><?= strip_tags($value) . ' ' . $attribute->unit; ?> </td>
                                </tr>
                                <?php $count++; ?>
                            <?php endforeach; ?>
                        <?php endforeach; ?>
                    </table>
                    <?php if($count > 9) : ?>
                        <a class="product-view-section__link but-link but-link-blue but-link-svg but-link-svg-right js-more-attributes" data-text="Свернуть" href="#">
                            <span>Показать все характеристики</span>
                            <?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/icon-open.svg'); ?>
                        </a>
                    <?php endif; ?>
                </div>
            <?php endif; ?>
            <div class="product-view-section__item product-view-section__item_description">
                <?php if (!empty($product->description)) : ?>
                    <div class="product-view-description" itemprop="description">
                        <h2>Описание</h2>
                        <div class="txt-style">
                            <?= $product->description; ?>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>

        <?php $this->widget('application.modules.store.widgets.LinkedProductsWidget', [
            'product' => $product,
            'code' => null
        ]); ?>

        <?php $this->widget('application.modules.store.widgets.ProductWidget', [
            'view' => 'similar-products-category',
            'product_id' => $product->id,
            'category_id' => $product->category_id
        ]); ?>
    </div>
</div>



<?php
$fancybox = $this->widget(
    'gallery.extensions.fancybox3.AlFancybox',
    [
        'target' => '[data-fancybox]',
        'lang'   => 'ru',
        'config' => [
            'animationEffect' => "fade",
            'buttons' => [
                "zoom",
                "close",
            ]
        ],
    ]
);
?>

<?php Yii::app()->getClientScript()->registerScript("product-myTab", "
    $('.image-preview').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        fade: true,
        dots: false,
        arrows: false,
        asNavFor: '.image-thumbnail',
        responsive: [
            {
                breakpoint: 640,
                settings: {
                    adaptiveHeight: true,
                    arrows: false,
                }
            }
        ]
    });
    $('.image-thumbnail').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        asNavFor: '.image-preview',
        dots: false,
        arrows: false,
        vertical: true,
        focusOnSelect: true,
        responsive: [
            {
                breakpoint: 1241,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    vertical: false,
                }
            },
            {
                breakpoint: 641,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    vertical: false,
                }
            },
            {
                breakpoint: 481,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    vertical: false,
                }
            },
            {
                breakpoint: 361,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    vertical: false,
                }
            },
        ]
    });
    $('#myTab li').first().addClass('active');
    $('.tab-pane').first().addClass('active');
"); ?>


 <!-- Задать вопрос -->
<?php $this->widget('application.modules.mail.widgets.GeneralFeedbackWidget', [
    'id' => 'askQuestionModal',
    'formClassName' => 'StandartForm',
    'buttonModal' => false,
    'titleModal' => 'Задать вопрос',
    'showCloseButton' => false,
    'isRefresh' => true,
    'showAttributeEmail' => true,
    'showAttributeComment' => true,
    'eventCode' => 'zadat-vopros',
    'successKey' => 'zadat-vopros',
    'modalHtmlOptions' => [
        'class' => 'modal-my',
    ],
    'formOptions' => [
        'htmlOptions' => [
            'class' => 'form-my',
        ]
    ],
    'modelAttributes' => [
        'theme' => 'Задать вопрос',
        'link' => "<a href='".ProductHelper::getUrl($product, true)."'>".$product->name."</a>",
    ],
]) ?>