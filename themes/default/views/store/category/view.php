<?php
$mainAssets = Yii::app()->getTheme()->getAssetsUrl();
/* @var $category StoreCategory */

$this->title =  $category->getMetaTitle();
$this->description = $category->getMetaDescription();
$this->keywords =  $category->getMetaKeywords();
$this->canonical = $category->getMetaCanonical();

$this->breadcrumbs = [Yii::t("StoreModule.store", "Catalog") => ['/store/product/index']];

$this->breadcrumbs = array_merge(
    $this->breadcrumbs,
    $category->getBreadcrumbs(true)
);

?>

<div class="page-content category-content">
    <div class="content">
        <?php $this->widget('application.components.MyTbBreadcrumbs', [
                'links' => $this->breadcrumbs,
        ]); ?>
        <h1><?= CHtml::encode($category->getTitle()); ?></h1>
        <div class="catalog-content fl fl-wr-w fl-ju-co-sp-b">
            <div class="catalog-content__sidebar">
                <div class="sidebar-box">
                    <div class="sidebar-box__close">
                        <?= file_get_contents('.'. $this->mainAssets . '/images/svg/icon-close.svg'); ?>
                    </div>
                    <form id="store-filter" name="store-filter" data-history="category" action="<?= $category->getCategoryUrl(); ?>" method="get">
                        <?php $this->widget('application.modules.store.widgets.filters.PriceFilterWidget'); ?>
                        <div class="filter-block-stock filter-xs-visible filter-checkbox">
                            <?= CHtml::checkBoxList('stock[]', !empty($_GET['stock']) ? $_GET['stock'] : [], Product::model()->getInStockList2(),
                                array('template'=>'{input}{label}')
                            ); ?>
                        </div>
                        <?php $this->widget('application.modules.store.widgets.filters.CategoryFilterWidget', [
                            'category' => $category,
                            'view' => 'category-filter-nocheck'
                        ]); ?>
                        <?php $this->widget('application.modules.store.widgets.filters.ProducerCityFilterWidget', ['category' => $category]); ?>
                        <?php $this->widget('application.modules.store.widgets.filters.ProducerFilterWidget', ['category' => $category]); ?>
                        <?php $this->widget('application.modules.store.widgets.filters.FilterBlockWidget', [
                            'category' => $category
                        ]); ?>
                    </form>
                </div>
            </div>
            <div class="catalog-content__content">
                <div class="selected-filters"></div>
                <div class="but-menu-filter">
                    <a class="but" href="#">
                        <i class="fa fa-filter" aria-hidden="true"></i>
                        <span>Фильтры</span>
                    </a>
                </div>
                <?php 
                    $this->widget(
                        'application.components.MyListView',
                    [
                        'dataProvider' => $dataProvider,
                        'id' => 'product-box',
                        'itemView' => '//store/product/'.$this->storeItem,
                        'emptyText'=>'В данной категории нет товаров.',
                        'summaryText'=>"{count} тов.",
                        'template'=>'
                            {controls}
                            {items}
                            {pager}
                        ',
                        // <div class="product-nav fl fl-ju-co-c">
                        // {countPage}
                        // </div>
                        'sortableAttributes' => [
                            'price_result.desc' => '<i class="ic-sort-desc"></i><span>По цене</span>',
                            'price_result.asc' => '<i class="ic-sort-asc"></i><span>По цене</span>',
                            'raiting.desc' => '<i class="ic-sort-desc"></i><span>По популярности</span>',
                            'raiting.asc' => '<i class="ic-sort-asc"></i><span>По популярности</span>',
                        ],
                        'sorterHeader' => 'Сортировать по:',
                        'itemsCssClass' => 'product-box product-box-2 product-list fl fl-wr-w',
                        'htmlOptions' => [
                            'class' => 'product-box-listView'
                        ],
                        'ajaxUpdate'=>true,
                        'enableHistory' => true,
                        'pagerCssClass' => 'pagination-box',
                        'pager' => [
                            'header' => '',
                            'lastPageLabel' => '<i class="fa fa-angle-double-right" aria-hidden="true"></i>',
                            'firstPageLabel' => '<i class="fa fa-angle-double-left" aria-hidden="true"></i>',
                            'prevPageLabel' => '<i class="fa fa-angle-left" aria-hidden="true"></i>',
                            'nextPageLabel' => '<i class="fa fa-angle-right" aria-hidden="true"></i>',
                            'maxButtonCount' => 5,
                            'htmlOptions' => [
                                'class' => 'pagination'
                            ],
                        ]
                    ]
                ); ?>
            </div>
        </div>
    </div>
</div>