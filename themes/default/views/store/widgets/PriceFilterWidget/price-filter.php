<?php $filter = Yii::app()->getComponent('attributesFilter');?>
<div class="filter-block filter-block-input-range active visiblity">
    <div class="filter-block__header fl fl-ju-co-sp-b">
        <div class="filter-block__heading fl fl-ju-co-sp-b">
            <?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/icon-open.svg'); ?>
            <span data-title="<?= Yii::t('StoreModule.store', 'Цена'); ?>" data-unit="руб."><?= Yii::t('StoreModule.store', 'Цена');?></span>
        </div>
        <div class="filter-block__reset">
            <a class="filter-block-reset" href="#">
                <?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/filter-close.svg'); ?>
            </a>
        </div>
    </div>
    <div class="filter-block__body">
        <div class="filter-block__content">
            <div class="filter-block-range range-input" id="range-input-price" type="range-input" data-update="#js-range-price">
                <?= CHtml::numberField('price[from]', Yii::app()->attributesFilter->getMainSearchParamsValue('price', 'from', Yii::app()->getRequest()), [
                    'class' => "form-control js-from-in js-from-price filter-block-range__item", 
                    'placeholder' => Yii::t('StoreModule.store', 'from').' '.floor($cost['minPrice'])
                ]); ?>
                <?= CHtml::numberField('price[to]', Yii::app()->attributesFilter->getMainSearchParamsValue('price', 'to', Yii::app()->getRequest()), [
                    'class' => "form-control js-to-in js-to-price filter-block-range__item", 
                    'placeholder' => Yii::t('StoreModule.store', 'to').' '.ceil($cost['maxPrice']),
                ]); ?>
            </div>
        </div>
        <div class="filter-block__footer">
            <input type="submit" value="Применить" class="but but-filter"/>
        </div>
    </div>
</div>

<?php /*Yii::app()->getClientScript()->registerScript("js-range-price", "
    var \$rangeprice = $('#js-range-price'),
        \$fromprice = $('.js-from-price'),
        \$toprice = $('.js-to-price'),
        my_range_price,
        minprice = $('#js-range-price').data('min'),
        maxprice = $('#js-range-price').data('max'),
        fromprice,
        toprice;

    var timPrice;
    var \$time = 1500;

    var updateValuesprice = function () {
        \$fromprice.prop('value', fromprice);
        \$toprice.prop('value', toprice);
    };

    \$rangeprice.ionRangeSlider({
        onStart: function (data) {
            fromprice = data.from;
            toprice = data.to;
            
            // updateValuesprice();
        },
        onChange: function (data) {
            fromprice = data.from;
            toprice = data.to;
            
            clearTimeout(timPrice);
            timPrice = setTimeout(function() {

                updateValuesprice();
                butApply();
            }, \$time);
        },
        onFinish: function (data) {
            fromprice = data.from;
            toprice = data.to;
            
            updateValuesprice();
        }
    });

    my_range_price = \$rangeprice.data('ionRangeSlider');

    var updateRangeprice = function () {
        my_range_price.update({
            from: fromprice,
            to: toprice
        });
    };
    // \$fromprice.focusout(function (event) {
    // $(document).delegate(\$fromprice, 'keyup', function(e){
    \$fromprice.on('keyup', function () {
        fromprice = +$(this).prop('value');
        // fromprice = +\$fromprice.val();
        clearTimeout(timPrice);
        timPrice = setTimeout(function() {
            if(fromprice != 0){
                if (fromprice < minprice) {
                    fromprice = minprice;
                }
                if (fromprice > toprice) {
                    fromprice = minprice;
                }
                if(fromprice == ''){
                    fromprice = minprice;
                }
                fromprice = fromprice * 1;

            } else{
                fromprice = minprice;
            }

            updateValuesprice();    
            updateRangeprice();

            butApply();
        }, \$time);
    });
    // \$toprice.focusout(function (event) {
    // $(document).delegate(\$toprice, 'keyup', function(e){
    \$toprice.on('keyup', function () {
        toprice = +$(this).prop('value');
        // toprice = +\$toprice.val();
        clearTimeout(timPrice);
        timPrice = setTimeout(function() {
            if(toprice != 0){
                if (toprice > maxprice) {
                    toprice = maxprice;
                }
                if (toprice < fromprice) {
                    toprice = maxprice;
                }
                if(toprice == ''){
                    toprice = maxprice;
                }
                toprice = toprice * 1;
            } else{
                toprice = maxprice;
            }

            updateValuesprice();    
            updateRangeprice();
        
            butApply();
        }, \$time);
    });
");*/ ?>