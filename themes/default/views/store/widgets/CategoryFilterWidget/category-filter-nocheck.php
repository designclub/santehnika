<?php if(!empty($categories)):?>
    <div class="filter-block filter-block-category active visiblity">
        <div class="filter-block__header fl fl-ju-co-sp-b">
            <div class="filter-block__heading fl fl-ju-co-sp-b">
                <?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/icon-open.svg'); ?>
                <span data-title="<?= Yii::t('StoreModule.store', 'Categories'); ?>"><?= Yii::t('StoreModule.store', 'Categories'); ?></span>
            </div>
            <div class="filter-block__reset">
                <a class="filter-block-reset" href="#">
                    <?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/filter-close.svg'); ?>
                </a>
            </div>
        </div>
        <div class="filter-block__body filter-block__category">
            <ul class="filter-block__list category-list-filter">
                <?php $count = 1; ?>
                <?php foreach($categories as $category):?>
                    <li class="filter-block__item category-list-filter__item filter-list <?php //= ($count <= 5) ? '' : 'hidden'; ?>">
                        <a class="box-animation" href="<?= $category->getCategoryUrl(); ?>">
                            <?= strip_tags($category->name_short);?>
                        </a>
                    </li>
                    <?php $count++; ?>
                <?php endforeach; ?>
            </ul>
            <?php /*if($count > 6) : ?>
                <div class="filter-block__footer">
                    <a class="filter-block__more but-link but-link but-link-blue but-link-svg but-link-svg-right" href="#">
                        <span data-text="Свернуть">Показать еще (<?= $count - 6; ?>)</span>
                        <?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/icon-open.svg'); ?>
                    </a>
                </div>
            <?php endif;*/ ?>
        </div>
    </div>
<?php endif;?>
