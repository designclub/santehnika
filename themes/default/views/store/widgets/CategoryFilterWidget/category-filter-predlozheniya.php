<?php if(!empty($categories)):?>
    <div class="filter-block active visiblity">
        <div class="filter-block__header fl fl-ju-co-sp-b">
            <div class="filter-block__heading fl fl-ju-co-sp-b">
                <?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/icon-open.svg'); ?>
                <span data-title="<?= Yii::t('StoreModule.store', 'Акция'); ?>"><?= Yii::t('StoreModule.store', 'Акция'); ?></span>
            </div>
            <div class="filter-block__reset">
                <a class="filter-block-reset" href="#">
                    <?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/filter-close.svg'); ?>
                </a>
            </div>
        </div>
        <div class="filter-block__body filter-block__category">
            <div class="filter-block__list">
                <?php $count = 1; ?>
                <?php foreach($categories as $category):?>
                    <div class="filter-block__item filter-checkbox filter-list <?= ($count <= 5) ? '' : 'hidden'; ?>">
                        <?= CHtml::checkBox('category[]', Yii::app()->attributesFilter->isMainSearchParamChecked(AttributeFilter::MAIN_SEARCH_PARAM_CATEGORY, $category->id, Yii::app()->getRequest()), [
                            'value' => $category->id,
                            'id' => 'category_'. $category->id
                        ]) ?>
                        <?= CHtml::label(strip_tags($category->name_short), 'category_'. $category->id);?>
                    </div>
                    <?php $count++; ?>
                <?php endforeach;?>
            </div>
            <?php if($count > 6) : ?>
                <div class="filter-block__footer">
                    <a class="filter-block__more but-link but-link but-link-blue but-link-svg but-link-svg-right" href="#">
                        <span data-text="Свернуть">Показать еще (<?= $count - 6; ?>)</span>
                        <?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/icon-open.svg'); ?>
                    </a>
                </div>
            <?php endif; ?>
        </div>
    </div>
<?php endif;?>
