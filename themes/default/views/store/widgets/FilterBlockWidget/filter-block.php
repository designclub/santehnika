<?php
/* @var $attributes array */
?>

<?php $this->widget(
    'application.modules.store.widgets.filters.AttributesFilterWidget', [
        'attributes' => $attributes,
        'category' => $category,
    ]
) ?>

<?php if (!empty($attributes) || !empty($category)): ?>
    <div class="filter-button fl fl-wr-w">
        <button type="submit" class="but but-animation but-filter">
            <span>Применить</span>
        </button>

        <button type="reset" class="but but-border">
            <span>Сбросить</span>
        </button>
    </div>
<?php endif; ?>
