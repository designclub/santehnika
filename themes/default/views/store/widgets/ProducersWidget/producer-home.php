<?php 
    /**
     * @var Вывод основных производителей товаров
    **/
?>
<?php if ($brands): ?>
    <div class="producer-carousel slick-slider">
        <?php foreach ($brands as $i => $data): ?>
            <?php if($data->image) : ?>
                <div class="producer-carousel__item">
                    <div class="producer-carousel__img fl fl-al-it-c fl-ju-co-c">
                        <a class="fl fl-al-it-c fl-ju-co-c" href="<?= Yii::app()->createUrl('/store/producer/view', ['slug' => $data->slug])?>">
                            <picture>
                                <source media="(min-width: 1px)" srcset="<?= $data->getImageUrlWebp(0, 0, true, null, 'image'); ?>" type="image/webp">
                                <source media="(min-width: 1px)" srcset="<?= $data->getImageNewUrl(0, 0, true, null, 'image'); ?>">

                                <img src="<?= $data->getImageNewUrl(0, 0, true, null, 'image'); ?>" alt="">
                            </picture>
                        </a>
                    </div>
                </div>
            <?php endif; ?>
        <?php endforeach; ?>
    </div>
<?php endif; ?>