<?php $filter = Yii::app()->getComponent('attributesFilter');?>
<?php //if(!empty($minDb) && !empty($maxDb)) : ?>
<div class="filter-block filter-block-input-range">
    <div class="filter-block__header fl fl-ju-co-sp-b">
        <div class="filter-block__heading fl fl-ju-co-sp-b">
            <?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/icon-open.svg'); ?>
            <span data-title="<?= $attribute->title ?>" data-unit="<?= $attribute->unit; ?>"><?= $attribute->title ?><?= ($attribute->unit) ? ', '.$attribute->unit : ''; ?></span>
        </div>
        <div class="filter-block__reset">
            <a class="filter-block-reset" href="#">
                <?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/filter-close.svg'); ?>
            </a>
        </div>
    </div>
    <div class="filter-block__body">
        <div class="filter-block__content">
            <div class="filter-block-range range-input" id="range-input-<?= $attribute->id; ?>" type="range-input" data-update="#js-range-<?= $attribute->id; ?>">
                <?= CHtml::numberField($filter->getFieldName($attribute, 'from'), $filter->getFieldValue($attribute, 'from'), [
                    'class' => "form-control js-from-in js-from-{$attribute->id} filter-block-range__item", 
                    'placeholder' => Yii::t('StoreModule.store', 'from').' '.$minDb
                ]); ?>
                <?= CHtml::numberField($filter->getFieldName($attribute, 'to'), $filter->getFieldValue($attribute, 'to'), [
                    'class' => "form-control js-to-in js-to-{$attribute->id} filter-block-range__item", 
                    'placeholder' => Yii::t('StoreModule.store', 'to').' '.$maxDb,
                ]); ?>
            </div>
            <div class="range">
                <input type="text" class="js-range" id="js-range-<?= $attribute->id; ?>" name="my_range_<?= $attribute->id; ?>" value=""
                    data-type="double"
                    data-min  = "<?= $minDb; ?>"
                    data-max  = "<?= $maxDb; ?>"
                    data-from = "<?= $filter->getFieldValue($attribute, 'from'); ?>"
                    data-to   = "<?= $filter->getFieldValue($attribute, 'to'); ?>"
                    data-grid="true"
                    data-skin="round"
                />
            </div>
        </div>
        <div class="filter-block__but filter-block__but_noafter">
            <input type="submit" value="Применить" class="but but-filter"/>
        </div>
    </div>
</div>

<?php Yii::app()->getClientScript()->registerScript("js-range-{$attribute->id}", "
    var \$range{$attribute->id} = $('#js-range-{$attribute->id}'),
        \$from{$attribute->id} = $('.js-from-{$attribute->id}'),
        \$to{$attribute->id} = $('.js-to-{$attribute->id}'),
        my_range_{$attribute->id},
        min{$attribute->id} = $('#js-range-{$attribute->id}').data('min'),
        max{$attribute->id} = $('#js-range-{$attribute->id}').data('max'),
        from{$attribute->id},
        to{$attribute->id};

    var tim{$attribute->id};
    var \$time = 1500;
    var updateValues{$attribute->id} = function () {
        \$from{$attribute->id}.prop('value', from{$attribute->id});
        \$to{$attribute->id}.prop('value', to{$attribute->id});
    };

    \$range{$attribute->id}.ionRangeSlider({
        onStart: function (data) {
            from{$attribute->id} = data.from;
            to{$attribute->id} = data.to;
            
            // updateValues{$attribute->id}();
        },
        onChange: function (data) {
            from{$attribute->id} = data.from;
            to{$attribute->id} = data.to;
            clearTimeout(tim{$attribute->id});
            tim{$attribute->id} = setTimeout(function() {

                updateValues{$attribute->id}();
                butApply();
            }, \$time);
        },
        onFinish: function (data) {
            from{$attribute->id} = data.from;
            to{$attribute->id} = data.to;
            
            updateValues{$attribute->id}();
        }
    });

    my_range_{$attribute->id} = \$range{$attribute->id}.data('ionRangeSlider');

    var updateRange{$attribute->id} = function () {
        my_range_{$attribute->id}.update({
            from: from{$attribute->id},
            to: to{$attribute->id}
        });
    };
    
    // \$from{$attribute->id}.focusout(function (event) {
    // $(document).delegate(\$from{$attribute->id}, 'keyup', function(e){
    \$from{$attribute->id}.on('keyup', function () {
        from{$attribute->id} = +$(this).prop('value');
        // from{$attribute->id} = +\$from{$attribute->id}.val();
        clearTimeout(tim{$attribute->id});
        tim{$attribute->id} = setTimeout(function() {
            if(from{$attribute->id} != 0){
                if (from{$attribute->id} < min{$attribute->id}) {
                    from{$attribute->id} = min{$attribute->id};
                }
                if (from{$attribute->id} > to{$attribute->id}) {
                    from{$attribute->id} = min{$attribute->id};
                }
                if(from{$attribute->id} == ''){
                    from{$attribute->id} = min{$attribute->id};
                }
                from{$attribute->id} = from{$attribute->id} * 1;

            } else{
                from{$attribute->id} = min{$attribute->id};
            }

            updateValues{$attribute->id}();    
            updateRange{$attribute->id}();

            butApply();
        }, \$time);
    });

    // \$to{$attribute->id}.focusout(function (event) {
    // $(document).delegate(\$to{$attribute->id}, 'keyup', function(e){
    \$to{$attribute->id}.on('keyup', function () {
        to{$attribute->id} = +$(this).prop('value');
        // to{$attribute->id} = +\$to{$attribute->id}.val();
        clearTimeout(tim{$attribute->id});
        tim{$attribute->id} = setTimeout(function() {
            if(to{$attribute->id} != 0){
                if (to{$attribute->id} > max{$attribute->id}) {
                    to{$attribute->id} = max{$attribute->id};
                }
                if (to{$attribute->id} < from{$attribute->id}) {
                    to{$attribute->id} = max{$attribute->id};
                }
                if(to{$attribute->id} == ''){
                    to{$attribute->id} = max{$attribute->id};
                }
                to{$attribute->id} = to{$attribute->id} * 1;
            } else{
                to{$attribute->id} = max{$attribute->id};
            }

            updateValues{$attribute->id}();    
            updateRange{$attribute->id}();
        
            butApply();
        }, \$time);
    });
"); ?>

<?php //endif; ?>