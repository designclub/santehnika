<?php if ($products): ?>
    <div class="product-carousel-section section-home">
        <div class="box-style">
            <div class="box-style__header fl fl-al-it-c">
                <div class="box-style__heading">
                    Похожие товары
                </div>
            </div>
        </div>
        <div class="product-box product-box-carousel slick-slider">
            <?php foreach ($products as $key => $data) : ?>
                <div>
                    <?php Yii::app()->controller->renderPartial('//store/product/_item', ['data' => $data, 'isdelete' =>false]) ?>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
<?php endif; ?>