<?php if($category) : ?>
	<div class="category-productDiscount js-category-productDiscount">
	<div class="content">
		<div class="box-style">
			<div class="box-style__header fl fl-wr-w fl-al-it-c">
                <div class="box-style__heading">
	               	Предложения со скидкой <span class="color-blue">до 30%</span>
	            </div>
	        </div>
		</div>
		<div class="category-productDiscount-nav js-productDiscount-nav fl fl-al-it-fl-s">
			<div class="category-productDiscount-nav__list js-productDiscount-list fl">
				<div class="category-productDiscount-nav__item fl fl-al-it-c fl-ju-co-c js-productDiscount-item" data-id="all">
					<a href="#">Все категории</a>
				</div>
				<?php foreach ($category as $key => $data) : ?>
					<div class="category-productDiscount-nav__item fl fl-al-it-c fl-ju-co-c js-productDiscount-item" data-id="<?= $data->id; ?>">
						<a href="#"><?= $data->name_short; ?></a>
					</div>
				<?php endforeach; ?>
			</div>
			<div class="category-productDiscount-nav__but fl fl-al-it-c fl-ju-co-c">
				<a class="but-link but-link-blue fl fl-al-it-c js-moreProductDiscount-nav" href="#" data-text="Свернуть">
					<span class="js-moreProductDiscount-text" data-text="Свернуть">Еще <span class="js-count-cat"></span></span>
					<?= CHtml::image(Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/icon-arrow-up-link.svg', '', ['class' => 'arrow-up-link']); ?>
				</a>
			</div>
		</div>
		<div class="category-productDiscount-content js-productDiscount-content"></div>
	</div>
</div>
<?php endif; ?>