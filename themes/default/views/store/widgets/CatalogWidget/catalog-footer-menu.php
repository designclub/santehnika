<?php 
	/**
	 * @var Вывод категорий каталога в футере
	**/
?>
<?php if($category) : ?>
	<ul class="menu-footer">
		<?php foreach ($category as $key => $data) : ?>
			<li>
				<a href="<?= $data->getCategoryUrl(); ?>">
					<?= $data->name_short; ?>
				</a>
			</li>
		<?php endforeach; ?>
	</ul>
<?php endif; ?>