<?php if ($category) : ?>
    <ul>
        <?php foreach ($category as $key => $data) : ?>
            <li>
                <a href="<?= $data->getCategoryUrl(); ?>">
                    <picture>
                        <source media="(min-width: 1px)" srcset="<?= $data->getImageUrlWebp(0, 0, true, null, 'image'); ?>" type="image/webp">
                        <source media="(min-width: 1px)" srcset="<?= $data->getImageNewUrl(0, 0, true, null, 'image'); ?>">

                        <img src="<?= $data->getImageNewUrl(0, 0, true, null, 'image'); ?>" alt="">
                    </picture>
                    <span><?= $data->name; ?></span>
                </a>
            </li>
        <?php endforeach; ?>
    </ul>
<?php endif; ?>