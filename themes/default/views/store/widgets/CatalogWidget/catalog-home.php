<?php 
	/**
	 * @var Вывод основных категорий каталога
	**/
?>
<?php if($category) : ?>
	<div class="catalog-home">
		<div class="content">
			<div class="box-style box-style-but">
				<div class="box-style__header fl fl-wr-w fl-al-it-c">
	                <div class="box-style__heading">
	                    Каталог товаров
	                </div>
	                <div class="box-style__but">
	                    <a class="but but-30 but-gray but-svg but-svg-right" href="<?= Yii::app()->createUrl('/store/product/index') ?>">
	                    	<span>В каталог</span>
	                    	<?= CHtml::image(Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/icon-but-arrow.svg', '', ['class' => 'icon-but-arrow']); ?>
	                    </a>
	                </div>
	            </div>
			</div>
			<div class="catalog-box catalog-box-carousel catalog-box-home fl fl-wr-w">
				<?php foreach ($category as $key => $data) : ?>
					<?php $this->render('_item', [
						'index' => $key,
						'data' => $data,
					]) ?>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
<?php endif; ?>