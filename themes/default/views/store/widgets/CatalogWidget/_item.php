<div class="catalog-box__item catalog-box__item_<?= $index; ?> box-animation">
	<div class="catalog-box__content fl fl-di-c fl-ju-co-sp-b">
		<div class="catalog-box__info">
			<a href="<?= $data->getCategoryUrl(); ?>">
				<div class="catalog-box__name">
					<span><?= strip_tags($data->name_short, '<br>');?></span>
				</div>
			</a>
			<div class="catalog-box__child catalog-box-child">
				<?php $count = 1; ?>
				<?php $childCat = $data->children(['condition' => 'children.status = 1']); ?>
				<?php if($childCat) : ?>
					<?php foreach ($childCat as $key => $child) : ?>
						<?php if($key <= 6) : ?>
							<div class="catalog-box-child__item">
								<a href="<?= $child->getCategoryUrl(); ?>"><?= $child->name_short; ?></a>
							</div>
						<?php endif ?>
						<?php $count++; ?>
					<?php endforeach; ?>
				<?php endif; ?>
			</div>
		</div>
		<?php if($count > 8) : ?>
			<div class="catalog-box__more">
				<a href="<?= $data->getCategoryUrl() ?>"><?= Yii::t('categoryMore', 'Еще {n} категория|Еще {n} категории|Еще {n} категорий', ($count - 8)); ?></a>	
			</div>
		<?php endif; ?>		
	</div>
	<div class="catalog-box__img">
		<?php if($data->image) : ?>
			<a class="fl fl-al-it-c fl-ju-co-c" href="<?= $data->getCategoryUrl() ?>">
				<picture>
	                <source media="(min-width: 1px)" srcset="<?= $data->getImageUrlWebp(0, 0, true, null, 'image'); ?>" type="image/webp">
	                <source media="(min-width: 1px)" srcset="<?= $data->getImageNewUrl(0, 0, true, null, 'image'); ?>">

	                <img src="<?= $data->getImageNewUrl(0, 0, true, null, 'image'); ?>" alt="">
	            </picture>
	        </a>
		<?php endif; ?>
	</div>
</div>