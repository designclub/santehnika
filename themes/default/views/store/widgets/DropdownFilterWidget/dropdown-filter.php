<?php $filter = Yii::app()->getComponent('attributesFilter');?>
<?php $list = $attribute->getOptionsList($category);  ?>
<?php if(count($list) > 0) : ?>
    <div class="filter-block js-filter-block">
        <div class="filter-block__header fl fl-ju-co-sp-b">
            <div class="filter-block__heading fl fl-ju-co-sp-b">
                <?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/icon-open.svg'); ?>
                <span data-title="<?= $attribute->title; ?>"><?= $attribute->title; ?></span>
            </div>
            <div class="filter-block__reset">
                <a class="filter-block-reset" href="#">
                    <?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/filter-close.svg'); ?>
                </a>
            </div>
        </div>
        <div class="filter-block__body filter-block__attribute">
            <div class="filter-block__list">
                <?php $count = 1; ?>
                <?php foreach ($list as $option): ?>
                    <div class="filter-block__item filter-checkbox filter-list <?= ($count <= 5) ? '' : 'hidden'; ?>">
                        <?= CHtml::checkBox($filter->getDropdownOptionName($option), $filter->getIsDropdownOptionChecked($option, $option->id), [
                            'value' => $option->id,
                            'id' => $attribute->name."_".$option->id
                        ]) ?>
                        <?= CHtml::label($option->value, $attribute->name."_".$option->id);?>
                    </div>
                    <?php $count++; ?>
                <?php endforeach;?>
            </div>
            <?php if($count > 6) : ?>
                <div class="filter-block__footer">
                    <a class="filter-block__more but-link but-link but-link-blue but-link-svg but-link-svg-right" href="#">
                        <span data-text="Свернуть">Показать еще (<?= $count - 6; ?>)</span>
                        <?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/icon-open.svg'); ?>
                    </a>
                </div>
            <?php endif; ?>
        </div>
    </div>
<?php endif; ?>