<?php /* @var $dataProvider CActiveDataProvider */ ?>
<?php if ($dataProvider->getTotalItemCount()): ?>
    <div class="product-carousel-section section-home">
        <div class="box-style">
            <div class="box-style__header fl fl-al-it-c">
                <div class="box-style__heading">
                    Вместе с этим товаром покупают
                </div>
            </div>
        </div>
        <div class="product-box product-box-carousel slick-slider">
            <?php foreach ($dataProvider->getData() as $key => $data) : ?>
                <div>
                    <?php Yii::app()->controller->renderPartial('//store/product/_item', ['data' => $data, 'isdelete' =>false]) ?>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
<?php endif; ?>