<?php
$this->title = $category->meta_title ?: $category->name;
$this->description = $category->meta_description;
$this->keywords = $category->meta_keywords;

$this->breadcrumbs = [$category->name];
?>

<div class="page-content">
    <div class="content">
        <?php $this->widget('application.components.MyTbBreadcrumbs', [
            'links' => $this->breadcrumbs,
        ]); ?>

        <h1><?= $category->name ?></h1>

        <?php  $this->widget(
            'bootstrap.widgets.TbListView',
            [
                'dataProvider' => $dataProvider,
                'id' => 'stock-box',
                'itemView' => '_itemStock',
                'template'=>'
                    {items}
                    {pager}
                ',
                'itemsCssClass' => "stock-box stock-page fl fl-wr-w",
                'htmlOptions' => [
                    // 'class' => 'news-box'
                ],
                'ajaxUpdate'=>true,
                'enableHistory' => true,
                // 'ajaxUrl'=>'GET',
                'pagerCssClass' => 'pagination-box',
                    'pager' => [
                    'header' => '',
                    'lastPageLabel' => '<i class="fa fa-angle-double-right" aria-hidden="true"></i>',
                    'firstPageLabel' => '<i class="fa fa-angle-double-left" aria-hidden="true"></i>',
                    // 'lastPageLabel'  => false,
                    // 'firstPageLabel' => false,
                    'prevPageLabel' => '<i class="fa fa-angle-left" aria-hidden="true"></i>',
                    'nextPageLabel' => '<i class="fa fa-angle-right" aria-hidden="true"></i>',
                    'maxButtonCount' => 5,
                    'htmlOptions' => [
                        'class' => 'pagination'
                    ],
                ]
            ]
        ); ?>
    </div>
</div>
