<div class="news-box__item">
    <div class="news-box__img box-style-img">
        <a href="<?= Yii::app()->createUrl('/news/news/view', ['slug' => $data->slug]); ?>">
            <picture>
                <source media="(min-width: 1px)" srcset="<?= $data->getImageUrlWebp(420, 240, true, null, 'image'); ?>" type="image/webp">
                <source media="(min-width: 1px)" srcset="<?= $data->getImageNewUrl(420, 240, true, null, 'image'); ?>">

                <img src="<?= $data->getImageNewUrl(420, 240, true, null, 'image'); ?>" alt="<?= $data->title; ?>">
            </picture>
        </a>
    </div>
    <div class="news-box__info">
        <div class="news-box__date"><?= date("d.m.Y", strtotime($data->date)); ?></div>
        <a class="but-link-svg" href="<?= Yii::app()->createUrl('/news/news/view', ['slug' => $data->slug]); ?>">
            <div class="news-box__name"><?= $data->title; ?></div>
        </a>
        <div class="news-box__desc"><?= $data->short_text; ?></div>
    </div>
</div>