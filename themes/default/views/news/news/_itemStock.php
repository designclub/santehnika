<div class="stock-box__item">
    <a class="stock-box__link" href="<?= Yii::app()->createUrl('/news/news/viewStock', ['slug' => $data->slug]); ?>">
        <div class="stock-box__img">
            <picture>
                <source media="(min-width: 1px)" srcset="<?= $data->getImageUrlWebp(0, 0, true, null, 'image'); ?>" type="image/webp">
                <source media="(min-width: 1px)" srcset="<?= $data->getImageNewUrl(0, 0, true, null, 'image'); ?>">

                <img src="<?= $data->getImageNewUrl(0, 0, true, null, 'image'); ?>" alt="<?= $data->title; ?>">
            </picture>
        </div>
    </a>
</div>