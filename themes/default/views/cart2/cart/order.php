<?php
$this->title = 'Оформление заказа';
$this->breadcrumbs = [
    Yii::t("CartModule.cart", 'Cart')
];
$assetsfm = Yii::app()->getAssetManager()->publish(
    Yii::getPathOfAlias('application.modules.delivery.views.assets')
);

Yii::app()->getClientScript()->registerScriptFile($assetsfm . '/js/jcfilter.min.js', CClientScript::POS_END);

?>

<div class="page-content cart-page-content">
    <div class="content">
        <?php $this->widget('application.components.MyTbBreadcrumbs', [
            'links' => $this->breadcrumbs,
        ]); ?>
        <h1><?= $this->title ?></h1>

        <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
            'id' => 'order-form',
            'htmlOptions' => [
                'class' => 'cart-form',
            ]
        ]); ?>

        <?= $form->errorSummary($order); ?>
        <!-- Информация из dadata заполняется автоматически -->
        <?= $form->hiddenField($order, 'region_obj') ?>
        <?= $form->hiddenField($order, 'city_obj') ?>
        <?= $form->hiddenField($order, 'street_obj') ?>
        <?= $form->hiddenField($order, 'house_obj') ?>
        <?= $form->hiddenField($order, 'apartment_obj') ?>
        <!-- Информация из dadata заполняется автоматически - end -->

        <div class="cart-section cart-section-2 fl fl-wr-w fl-ju-co-sp-b">
            <div class="cart-section__content">
                <?php $this->widget('application.modules.cart.widgets.CartStaticWidget') ?>

                <h4>Введите личные данные:</h4>
                <div class="cart-form-content cart-form m-b-25">
                    <div class="row">
                        <div class="col-sm-6">
                            <?= $form->textFieldGroup($order, 'name', [
                                'widgetOptions' => [
                                    'htmlOptions' => [
                                        'autocomplete' => 'off'
                                    ]
                                ]
                            ]); ?>
                        </div>
                        <div class="col-sm-3">
                            <?= $form->telFieldGroup($order, 'phone', [
                                'widgetOptions' => [
                                    'htmlOptions'=>[
                                        'class' => 'phone-mask',
                                        'data-phoneMask' => 'phone',
                                        'placeholder' => 'Телефон',
                                        'autocomplete' => 'off'
                                    ]
                                ]
                            ]); ?>
                        </div>
                        <div class="col-sm-3">
                            <?= $form->textFieldGroup($order, 'email', [
                                'widgetOptions' => [
                                    'htmlOptions' => [
                                        'autocomplete' => 'off'
                                    ]
                                ]
                            ]); ?>
                        </div>
                    </div>
                    <div class="cart-form-content__terms_of_use cart-color-757575 cart-text-13-normal">
                        * - поля, отмеченные звёздочкой, обязательны для заполнения
                    </div>
                </div>

                <h4>Город получения товара</h4>

                <div class="js-region-city cart-form">
                    <div class="row">
                        <div class="col-sm-4">
                            <?= $form->select2Group($order, 'region', [
                                'widgetOptions' => [
                                    'asDropDownList' => false,
                                    'options' => [
                                        'minimumInputLength' => 3,
                                        'ajax' => [
                                            'url' => Yii::app()->createUrl('/cart/dadata/getRegion'),
                                            'quietMillis' => 1000,
                                            'data' => 'js:function(res) {
                                                return {query: res};
                                            }',
                                            'results' => 'js: function(res) {
                                                return res;
                                            }'
                                        ],
                                    ],
                                    'events' => [
                                        'change.select2' => 'js:function(e) {
                                            $("input[name=\"Order[region_obj]\"").val(JSON.stringify(e.added));
                                            $("input[name=\"Order[region]\"").val(e.added.text);
                                        }'
                                    ],
                                    'htmlOptions' => [
                                        'autocomplete' => 'off'
                                    ]
                                ]
                            ]); ?>
                        </div>
                        <div class="col-sm-4">
                            <?= $form->select2Group($order, 'city', [
                                'widgetOptions' => [
                                    'asDropDownList' => false,
                                    'options' => [
                                        'minimumInputLength' => 3,
                                        'ajax' => [
                                            'url' => Yii::app()->createUrl('/cart/dadata/getCity'),
                                            'quietMillis' => 1000,
                                            'data' => 'js:function(res) {
                                                return {query: res, region: $("input[name=\"Order[region_obj]\"").val()};
                                            }',
                                            'results' => 'js: function(res) {
                                                return res;
                                            }'
                                        ],
                                    ],
                                    'events' => [
                                        'change.select2' => 'js:function(e) {
                                            $("input[name=\"Order[city_obj]\"").val(JSON.stringify(e.added));
                                            $("input[name=\"Order[city]\"").val(e.added.text);
                                            $(".preloader").addClass("active");
                                            $.ajax({
                                                url: "'.Yii::app()->createUrl('/delivery/deliveryAjax/deliveryList').'",
                                                data: $("#order-form").serialize(),
                                                type: "post",
                                                success: function(data) {
                                                    $(".js-delivery-list")
                                                        .fadeOut({
                                                            done: function() {
                                                                $(this)
                                                                    .html(data)
                                                                    .fadeIn();
                                                            }
                                                        });
                                                    $(".preloader").removeClass("active");
                                                }
                                            })
                                        }'
                                    ],
                                    'htmlOptions' => [
                                        'autocomplete' => 'off'
                                    ]
                                ]
                            ]); ?>
                        </div>
                    </div>
                </div>

                <div class="js-delivery-list">
                    <?php if ($order->city) : ?>
                        <?php $this->widget('application.modules.delivery.widgets.DeliveryWidget', [
                            'order' => $order,
                        ]); ?>
                    <?php endif ?>
                </div>
                <div class="cart-section__buttons cart-section-buttons fl fl-wr-w fl-ju-co-fl-e">
                    <div class="cart-section-buttons__item">
                        <?= CHtml::link('Назад', ['/cart/cart/index'], ['class' => 'bt-cart bt-cart-border-green']) ?>
                    </div>
                    <div class="cart-section-buttons__item">
                        <button type="submit" name="create" class="bt-cart bt-cart-red pickup-checkbox">Далее</button>
                    </div>
                </div>

            </div>
            <div class="cart-section__result">
                <?php $this->widget('application.modules.cart.widgets.PriceOverview') ?>
            </div>
        </div>

        <?php $this->endWidget() ?>
    </div>
</div>