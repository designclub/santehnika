<?php if(false === $favorite->has($product->id)):?>
    <a href="#" class="product-vertical-extra__button yupe-store-favorite-add fl fl-al-it-c fl-ju-co-c" data-id="<?= $product->id;?>">
		<?php //= CHtml::image(Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/icon-favorite.svg', ''); ?>
		<i class="ic-favorite"></i>
    </a>
<?php else:?>
    <a href="#" class="product-vertical-extra__button yupe-store-favorite-remove text-error fl fl-al-it-c fl-ju-co-c" data-id="<?= $product->id;?>">
		<?php //= CHtml::image(Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/icon-favorite.svg', ''); ?>
		<i class="ic-favorite"></i>
	</a>
<?php endif;?>