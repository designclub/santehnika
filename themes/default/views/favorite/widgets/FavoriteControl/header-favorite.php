<div class="header-favorite">
    <a class="but-favorite but-header fl fl-di-c fl-al-it-c fl-ju-co-c" href="<?= Yii::app()->createUrl('/favorite/default/index'); ?>" class="toolbar-button">
        <div class="but-favorite__icon but-header__icon">
            <i class="ic-favorite"></i>
            <span class="js-yupe-store-favorite-total but-favorite__count but-header__count fl fl-al-it-c fl-ju-co-c <?= (Yii::app()->favorite->count() != null) ? 'active' : ''; ?>"><?= Yii::app()->favorite->count();?></span>
        </div>
    </a>
</div>