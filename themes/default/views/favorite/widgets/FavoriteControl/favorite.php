<?php if(false === $favorite->has($product->id)):?>
    <div class="yupe-product-favorite-add" data-id="<?= $product->id;?>" data-text="В избранном">
		<?= CHtml::image(Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/icon-favorite.svg', ''); ?>
		<span>В избранное</span>
    </div>
<?php else:?>
    <div class="yupe-product-favorite-remove text-error" data-id="<?= $product->id;?>" data-text="В избранное">
    	<?= CHtml::image(Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/icon-favorite.svg', ''); ?>
    	<span>В избранном</span>
	</div>
<?php endif;?>