<?php if($models->images) : ?>
	<div class="footer__heading"><?= $models->name; ?></div>
	<div class="soc-box">
		<?php foreach ($models->images(['order' => 'position ASC']) as $key => $item): ?>
			<?php if($item->description) : ?>
				<a href="<?= $item->description; ?>">
					<?= CHtml::image($item->getImageUrl(), ''); ?>
				</a>
			<?php endif; ?>
		<?php endforeach ?>
	</div>
<?php endif; ?>