<?php if($models->images) : ?>
	<ul>
		<?php foreach ($models->images(['order' => 'position ASC']) as $key => $item): ?>
			<li>
				<?= CHtml::image($item->getImageUrl(), ''); ?>
			</li>
		<?php endforeach ?>
	</ul>
<?php endif; ?>