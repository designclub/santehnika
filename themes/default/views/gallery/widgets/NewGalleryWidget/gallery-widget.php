<?php if($models->images) : ?>
	<?php foreach ($models->images(['order' => 'position ASC']) as $key => $item) : ?>
		<picture class="fl fl-al-it-fl-e fl-ju-co-c">
			<source media="(min-width: 1px)" srcset="<?= $item->getImageUrlWebp(0, 0, false, null, 'file'); ?>" type="image/webp">
		    <source media="(min-width: 1px)" srcset="<?= $item->getImageNewUrl(0, 0, false, null, 'file'); ?>">

		    <img src="<?= $item->getImageNewUrl(0, 0, false,null, 'file'); ?>" alt="">
		</picture>
	<?php endforeach; ?>
<?php endif; ?>