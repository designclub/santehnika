<?php $photos = $model->getAttributeValue($code)['gallery']; ?>
<?php if($photos) : ?>
	<div class="payments-list-box fl fl-wr-w fl-al-it-c">
		<?php foreach ($photos as $key => $photo): ?>
			<div class="payments-list-box__item <?= $class; ?>">
                <picture>
		            <source media="(min-width: 1px)" srcset="<?= $model->geFieldGalImageWebp(0, 0, false,  $photo['image']); ?>" type="image/webp">
		            <source media="(min-width: 1px)" srcset="<?= $model->getFieldGalImageUrl(0, 0, false,  $photo['image']); ?>">

		            <img src="<?= $model->getFieldGalImageUrl(0, 0, false,  $photo['image']); ?>" alt="<?= $data->title; ?>">
		        </picture>
			</div>
		<?php endforeach ?>
	</div>
<?php endif; ?>