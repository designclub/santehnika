<!DOCTYPE html>
<html lang="<?= Yii::app()->language; ?>">
<head>
    <?php \yupe\components\TemplateEvent::fire(DefautThemeEvents::HEAD_START);?>
    
    <link rel="preconnect" href="https://mc.yandex.ru" />
    <link rel="preconnect" href="https://connect.facebook.net" />
    <link rel="preconnect" href="https://www.googletagmanager.com" />
    <link rel="preconnect" href="https://www.googleadservices.com" />
    <link rel="preconnect" href="https://www.google-analytics.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" />
    <link rel="preconnect" href="https://www.gstatic.com" />
    <link rel="preconnect" href="https://www.google.com" />
    <link rel="preconnect" href="https://stackpath.bootstrapcdn.com" />
    <link rel="preconnect" href="https://fonts.googleapis.com" />

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta http-equiv="Content-Language" content="ru-RU" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <title><?= $this->title;?></title>
    <meta name="description" content="<?= $this->description;?>" />
    <meta name="keywords" content="<?= $this->keywords;?>" />

    <link rel="apple-touch-icon" sizes="57x57" href="<?= $this->mainAssets; ?>/images/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?= $this->mainAssets; ?>/images/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?= $this->mainAssets; ?>/images/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?= $this->mainAssets; ?>/images/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?= $this->mainAssets; ?>/images/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?= $this->mainAssets; ?>/images/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?= $this->mainAssets; ?>/images/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?= $this->mainAssets; ?>/images/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?= $this->mainAssets; ?>/images/favicon/apple-icon-180x180.png">

    <link rel="icon" type="image/png" href="<?= $this->mainAssets; ?>/images/favicon/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="<?= $this->mainAssets; ?>/images/favicon/android-chrome-192x192.png" sizes="192x192">
    <link rel="icon" type="image/png" href="<?= $this->mainAssets; ?>/images/favicon/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="<?= $this->mainAssets; ?>/images/favicon/favicon-16x16.png" sizes="16x16">
    
    <?php if ($this->canonical): ?>
        <link rel="canonical" href="<?= $this->canonical ?>" />
    <?php else : ?>
        <link rel="canonical" href="<?= Yii::app()->request->hostInfo . '/' . Yii::app()->request->pathInfo; ?>">
    <?php endif; ?>

    <?php
    
    if(Yii::app()->request->queryString) {
        Yii::app()->clientScript->registerMetaTag('noindex, nofollow', 'robots');
    } else {
        Yii::app()->clientScript->registerMetaTag('index, follow', 'robots');
    }
    
    $indexCss = $this->mainAssets . "/css/index.css";
    $indexCss = $indexCss . "?v-" . filectime(Yii::getPathOfAlias('public') . $indexCss);
    Yii::app()->getClientScript()->registerCssFile($indexCss);


    $mainJs = $this->mainAssets . "/js/main.js";
    $mainJs = $mainJs . "?v-" . filectime(Yii::getPathOfAlias('public') . $mainJs);
    Yii::app()->getClientScript()->registerScriptFile($mainJs, CClientScript::POS_END);

    $storeJs = $this->mainAssets . "/js/store.js";
    $storeJs = $storeJs . "?v-" . filectime(Yii::getPathOfAlias('public') . $storeJs);
    Yii::app()->getClientScript()->registerScriptFile($storeJs, CClientScript::POS_END);
    
    // Yii::app()->getClientScript()->registerScriptFile('https://www.google.com/recaptcha/api.js');
    // Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/js/jquery.easing.min.js');
    Yii::app()->clientScript->registerMetaTag('telephone=no', 'format-detection');

    /* 
     * Шрифты 
    */
    Yii::app()->getClientScript()->registerScriptFile('https://use.fontawesome.com/289bda1ff9.js', CClientScript::POS_END);
    // Yii::app()->getClientScript()->registerLinkTag('preload stylesheet', 'text/css', 'https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', NULL, ['as'=> 'style']);
    Yii::app()->getClientScript()->registerLinkTag('preload stylesheet', 'text/css', 'https://fonts.googleapis.com/css2?family=Ubuntu:wght@300;400;500;700&display=swap', NULL, ['as'=> 'style']);

    ?>
    <script type="text/javascript">
        var yupeTokenName = "<?= Yii::app()->getRequest()->csrfTokenName;?>";
        var yupeToken = "<?= Yii::app()->getRequest()->getCsrfToken();?>";
        var yupeCartDeleteProductUrl = "<?= Yii::app()->createUrl('/cart/cart/delete/')?>";
        var yupeCartUpdateUrl = "<?= Yii::app()->createUrl('/cart/cart/update/')?>";
        var yupeCartWidgetUrl = "<?= Yii::app()->createUrl('/cart/cart/widget/')?>";
        var phoneMaskTemplate = "<?= Yii::app()->getModule('user')->phoneMask; ?>";
        // delivery
        var deliveryMethodUrl = "<?= Yii::app()->createUrl('/delivery/deliveryAjax/deliveryMethod') ?>";
        // delivery end
    </script>

    <?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
        'id' => 5
    ]); ?>


    <!--[if IE]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!-- <link rel="stylesheet" href="http://yandex.st/highlightjs/8.2/styles/github.min.css">
    <script src="http://yastatic.net/highlightjs/8.2/highlight.min.js"></script> -->
    <?php \yupe\components\TemplateEvent::fire(DefautThemeEvents::HEAD_END);?>
</head>

<body>

<?php \yupe\components\TemplateEvent::fire(DefautThemeEvents::BODY_START);?>

<div class="wrapper">
    <div class="wrap1">
        
        <?php $this->renderPartial('//layouts/_header'); ?>

        <?= $this->decodeWidgets($content); ?>
    </div>
    <div class="wrap2">
        <?php $this->renderPartial('//layouts/_footer'); ?>
    </div>
</div>

<?php $fancybox = $this->widget(
    'gallery.extensions.fancybox3.AlFancybox', [
        'target' => '[data-fancybox]',
        'lang'   => 'ru',
        'config' => [
            'animationEffect' => "fade",
            'buttons' => [
                "zoom",
                "close",
            ]
        ],
    ]
); ?>

<div id="messageModal" class="modal modal-my modal-my-xs fade" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header box-style">
                <div data-dismiss="modal" class="modal-close"><div></div></div>
                <div class="box-style__header">
                    <div class="box-style__heading">
                        Уведомление
                    </div>
                </div>
            </div>
            <div class="modal-body">
                <div class="message-success">
                    Ваша заявка успешно отправлена!
                </div>
            </div>
        </div>
    </div>
</div>

<div id="addCartModal" class="modal modal-my modal-my-sm fade" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header box-style">
                <div data-dismiss="modal" class="modal-close"><div></div></div>
                <div class="box-style__header">
                    <div class="box-style__heading">
                        Товар добавлен в корзину!
                    </div>
                </div>
            </div>
            <div class="modal-body js-addCartModal-body">
            </div>
            <div class="modal-footer">
                <div class="fl fl-ju-co-fl-e addCartModal-button">
                    <a class="but but-border" data-dismiss="modal" href="#">Продолжить покупки</a>
                    <a class="but but-animation" href="<?= Yii::app()->createUrl('cart/cart/index'); ?>">Перейти в корзину</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="mobileNav">
    <div class="mobileNav__icon-close"><div></div></div>
    <div class="mobileNav-box">
        <div class="mobileNav-box-2">
            <ul class="menu-mobile">
            </ul>
        </div>
    </div>
</div>

<div class="menu-fix menu-main-fix">
    <div class="menu-fix__icon-close"><div></div></div>
    <div class="menu-fix__box">
        <div class="menu-fix__box2">
            <div class="menu-fix__box3">
                <?php if (Yii::app()->hasModule('menu')) : ?>
                    <?php $this->widget('application.modules.menu.widgets.MenuWidget', [
                        'view' => 'footermenu',
                        'name' => 'top-menu'
                    ]); ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<div class="menu-fix catalog-menu-fix">
    <!-- <div class="menu-fix__header">Каталог</div> -->
    <div class="menu-fix__icon-close"><div></div></div>
    <div class="menu-fix__box">
        <div class="menu-fix__box2">
            <div class="menu-fix__box3">
                <?php //if($this->beginCache('store::catalog::menufix', ['duration' => $this->yupe->coreCacheTime])):?>
                    <?php $this->widget('application.modules.store.widgets.CatalogWidget', [
                        'view' => 'catalog-menu-fix'
                    ]); ?>
                    <?php //$this->endCache();?>
                <?php // endif;?>
            </div>
        </div>
    </div>
</div>

<?php //Модалка для поиска ?>
<?php $this->widget('application.modules.store.widgets.SearchProductWidget', [
    'view' => 'search-product-form-modal'
]); ?>


 <!-- Заказать звонок -->
<?php $this->widget('application.modules.mail.widgets.GeneralFeedbackWidget', [
    'id' => 'requestCallModal',
    'formClassName' => 'StandartForm',
    'buttonModal' => false,
    'titleModal' => 'Заказать звонок',
    'showCloseButton' => false,
    'isRefresh' => true,
    'eventCode' => 'zakazat-zvonok',
    'successKey' => 'zakazat-zvonok',
    'modalHtmlOptions' => [
        'class' => 'modal-my',
    ],
    'formOptions' => [
        'htmlOptions' => [
            'class' => 'form-my',
        ]
    ],
    'modelAttributes' => [
        'theme' => 'Заказать звонок',
    ],
]) ?>
<?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
        'id' => 6
    ]); ?>
<?php \yupe\components\TemplateEvent::fire(DefautThemeEvents::BODY_END);?>

<div class='notifications top-right' id="notifications"></div>
<div class="ajax-loading"></div>
</body>
</html>
