<footer>
    <div class="footer">
        <div class="footer-top">
            <div class="content fl fl-wr-w fl-ju-co-sp-b">
                <div class="footer-top__item footer-top__item_catalog">
                    <div class="footer__heading">Каталог</div>
                    <?php $this->widget('application.modules.store.widgets.CatalogWidget', [
                        'view' => 'catalog-footer-menu',
                        'limit' => 7
                    ]); ?>
                </div>
                <div class="footer-top__item footer-top__item_menu">
                    <?php if(Yii::app()->hasModule('menu')): ?>
                        <?php $this->widget('application.modules.menu.widgets.MenuWidget', [
                            'view' => 'footermenu', 
                            'name' => 'nizhnee-menyu'
                        ]); ?>
                    <?php endif; ?>
                </div>
                <div class="footer-top__item footer-top__item_contact footer-contact">
                    <div class="footer__heading">Контакты</div>
                    <div class="footer-contact__mode">
                        <?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
                            'id' => 7
                        ]); ?>
                    </div>
                    <div class="footer-contact__phone">
                        <?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
                            'id' => 1
                        ]); ?>
                    </div>
                    <div class="footer-contact__but">
                        <a class="but but-border but-border-orange" data-target="#requestCallModal" data-toggle="modal" href="#">
                            Перезвоните мне
                        </a>
                    </div>
                    <div class="footer-contact__email">
                        <?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
                            'id' => 2
                        ]); ?>
                    </div>
                    <div class="footer-contact__location">
                        <?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
                            'id' => 3
                        ]); ?>
                    </div>
                    <div class="footer-contact__but">
                        <a class="footer-contact__link but-link but-link-blue" data-fancybox data-type="iframe" data-src="<?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', ['id' => 8]); ?>" href="javascript:;">Показать на карте</a>
                    </div>
                </div>
                <div class="footer-top__item footer-top__item_info footer-contact">
                    <div class="footer-contact__soc">
                        <?php $this->widget('application.modules.gallery.widgets.NewGalleryWidget', [
                            'id' => 1,
                            'view' => 'view-soc'
                        ]); ?>
                    </div>
                    <div class="footer-contact__logo footer-logo">
                        <a href="/">
                            <?= CHtml::image($this->mainAssets . '/images/logo.svg') ?>
                        </a>
                    </div>
                    <div class="footer-top__offer footer__copy">
                        <?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
                            'id' => 11
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="footer-bot">
            <div class="content fl fl-wr-w fl-al-it-c fl-ju-co-sp-b">
                <div class="footer-bot__paymentLogo">
                    <?php $this->widget('application.modules.gallery.widgets.NewGalleryWidget', [
                        'id' => 2,
                        'view' => 'payments-logo-footer'
                    ]); ?>
                </div>
                <div class="footer-bot__copy footer__copy">
                    &copy; <?= date("Y"); ?> <?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', ['id' => 10]); ?>
                </div>
                <div class="footer-bot__dc-logo">
                    <a class="fl fl-wr-w fl-al-it-c" target="_blank" href="https://dcmedia.ru">
                        <?= CHtml::image(Yii::app()->getTheme()->getAssetsUrl() . '/images/dc_logo.png');?>
                        <span>Создание и продвижение сайтов</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</footer>