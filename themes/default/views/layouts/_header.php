<?php $lkurl = (Yii::app()->user->isGuest) ? Yii::app()->createUrl('user/account/login') : Yii::app()->createUrl('order/user/index'); ?>
<?php $lkname = (Yii::app()->user->isGuest) ? 'Войти' : Yii::app()->user->getProfile()->getNamee(); ?>

<?php $this->widget('application.modules.slider.widgets.SliderWidget', [
    'category_id' => 2,
    'view' => 'carousel-header-widget'
]); ?>

<header id="header" class="<?= ($this->action->id=='index' && $this->id=='hp') ? 'header-home' : 'header-page'; ?>">
    <div class="content">
        <div class="header-top fl fl-wr-w fl-al-it-c fl-ju-co-sp-b">
            <div class="header-top__item header-menu">
                <?php if (Yii::app()->hasModule('menu')) : ?>
                    <?php $this->widget('application.modules.menu.widgets.MenuWidget', [
                        'view' => 'main',
                        'name' => 'top-menu'
                    ]); ?>
                <?php endif; ?>
            </div>
            <div class="header-top__item header-contact fl fl-al-it-c fl-ju-co-fl-e">
                <div class="header-contact__item header-contact__item_mode">
                    <div class="header-contact__mode">
                        <?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
                            'id' => 7
                        ]); ?>
                    </div>
                </div>
                <div class="header-contact__item header-contact__item_phone">
                    <div class="header-contact__phone">
                        <?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
                            'id' => 1
                        ]); ?>
                    </div>
                </div>
                <div class="header-contact__item header-contact__item_callback">
                    <div class="header-contact__callback">
                        <a class="but but-border but-border-orange" data-target="#requestCallModal" data-toggle="modal" href="#">
                            Перезвоните мне
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="header fl fl-wr-w fl-al-it-c fl-ju-co-sp-b">
            <div class="header__logo">
                <a class="logo fl fl-al-it-c" href="/">
                    <?= CHtml::image($this->mainAssets . '/images/logo.svg', ''); ?>
                </a>
            </div>
            <div class="header__wrap header-wrap fl fl-al-it-c">
                <div class="header-wrap__item header-wrap__item_icon-menu">
                    <div class="menu-fix-icon js-menu-mobileNav fl fl-al-it-c fl-ju-co-c" data-menu=".mobileNav">
                        <?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/icon-menu.svg'); ?>
                    </div>
                </div>
                <div class="header-wrap__item header-wrap__item_catalog">
                    <a href="<?= Yii::app()->createUrl('/store/product/index'); ?>">
                        <div class="catalog-menu-icon but but-svg but-svg-left fl fl-al-it-c fl-ju-co-c">
                            <?= CHtml::image($this->mainAssets . '/images/svg/icon-catalog.svg', '', [
                                'class' => 'icon-catalog'
                            ]); ?>
                            <?= CHtml::image($this->mainAssets . '/images/svg/icon-close.svg', '', [
                                'class' => 'icon-close'
                            ]); ?>
                            <span>Каталог</span>
                        </div>
                    </a>
                    <div class="header-catalog fl">
                        <?php //if ($this->beginCache('store::category::menu', ['duration' => $this->yupe->coreCacheTime])) :?>
                            <?php $this->widget('application.modules.store.widgets.CategoryWidget', ['depth' => 1]); ?>
                            <?php //$this->endCache();?>
                        <?php //endif;?>
                    </div>
                </div>
                <div class="header-wrap__item header-wrap__item_search">
                    <div class="header-search fl fl-al-it-c">
                        <?php $this->widget('application.modules.store.widgets.SearchProductWidget'); ?>
                    </div>
                </div>
                <div class="header-wrap__item header-wrap__item_lk">
                    <div class="header-lk">
                        <a class="header-lk__item but-header fl fl-di-c fl-al-it-c fl-ju-co-c" href="<?= $lkurl; ?>">
                            <div class="but-header__icon">
                                <i class="ic-account"></i>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="header-wrap__item header-wrap__item_favorite">
                    <?php if(Yii::app()->hasModule('favorite')):?>
                        <?php $this->widget('application.modules.favorite.widgets.FavoriteControl', [
                            'view' => "header-favorite"
                        ]);?>
                    <?php endif;?>
                </div>
                <div class="header-wrap__item header-wrap__item_cart">
                    <?php if (Yii::app()->hasModule('cart')) : ?>
                        <div class="header-cart shopping-cart-widget js-shopping-cart-widget" id="shopping-cart-widget">
                            <?php $this->widget('application.modules.cart.widgets.ShoppingCartWidget'); ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</header>

<div class="header-mobile fl fl-wr-w fl-al-it-c">
    <div class="header-mobile__item header-mobile__home">
        <a class="fl fl-di-c fl-al-it-c fl-ju-co-c <?= ($this->action->id=='index' && $this->id=='hp') ? 'active' : ''; ?>" href="/">
            <div class="header-mobile__icon">
                <?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/icon-home-mobile.svg'); ?>
            </div>
            <span>Главная</span>
        </a>
    </div>
    <div class="header-mobile__item header-mobile__catalog">
        <a class="fl fl-di-c fl-al-it-c fl-ju-co-c js-menu-fix" data-menu=".catalog-menu-fix" href="#">
            <div class="header-mobile__icon">
                <?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/icon-catalog-mobile.svg'); ?>
            </div>
            <span>Каталог</span>
        </a>
    </div>
    <div class="header-mobile__item header-mobile__search">
        <a class="fl fl-di-c fl-al-it-c fl-ju-co-c js-search-modal-but" data-modal="#search-form-Modal" href="#">
            <div class="header-mobile__icon">
                <?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/icon-search-mobile.svg'); ?>
            </div>
            <span>Поиск</span>
        </a>
    </div>
    <div class="header-mobile__item header-mobile__cart">
        <?php if (Yii::app()->hasModule('cart')) : ?>
            <?php $this->widget('application.modules.cart.widgets.ShoppingCartWidget', ['view' => 'shoppingCart-mobile']); ?>
        <?php endif; ?>
    </div>
    <div class="header-mobile__item header-mobile__lk">
        <a class="fl fl-di-c fl-al-it-c fl-ju-co-c" href="<?= $lkurl; ?>">
            <div class="header-mobile__icon">
                <?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/icon-login-mobile.svg'); ?>
            </div>
            <span>Профиль</span>
        </a>
    </div>
</div>