<?php
$currency = Yii::app()->getModule('store')->currency;
?>
<div class="but-cart but-header js-cart js-cart-widget" data-cart-widget-url="<?= Yii::app()->createUrl('/cart/cart/widget'); ?>">
    <a class="fl fl-al-it-c fl-ju-co-fl-s" href="<?= Yii::app()->createUrl('/cart/cart/index') ?>">
        <div class="but-cart__icon but-header__icon fl fl-al-it-c fl-ju-co-c">
            <i class="ic-cart"></i>
            <div class="but-cart__count but-header__count fl fl-al-it-c fl-ju-co-c <?= (empty(Yii::app()->cart->isEmpty())) ? 'active' : ''; ?>">
                <?= Yii::app()->cart->getItemsCount(); ?>
            </div>
        </div>
        <div class="but-cart__text but-header__text">
            <?php if (empty(Yii::app()->cart->isEmpty())): ?>
                <div class="header-cart__cost-price mobile-hidden">
                    <span class="js-cart__subtotal">
                        <?= str_replace('.00', '', number_format(Yii::app()->cart->getCost(), 2, '.', ' ')); ?>
                    </span>
                    <span class="ruble">
                        <?= CHtml::image(Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/icon-ruble.svg', ''); ?>
                    </span>
                </div>
                <div class="mobile-visible"><?= Yii::t("CartModule.cart", "Корзина"); ?></div>
            <?php else : ?>
                <?= Yii::t("CartModule.cart", "Корзина"); ?>
            <?php endif; ?>
        </div>
    </a>
</div>