<?php
/** @var $order Order */

$this->title = 'Оформление заказа';
$this->breadcrumbs = [
    Yii::t("CartModule.cart", 'Cart')
];
$assetsfm = Yii::app()->getAssetManager()->publish(
    Yii::getPathOfAlias('application.modules.delivery.views.assets')
);

Yii::app()->getClientScript()->registerScriptFile($assetsfm . '/js/jcfilter.min.js', CClientScript::POS_END);
Yii::app()->getClientScript()->registerScriptFile('https://cdn.jsdelivr.net/npm/suggestions-jquery@latest/dist/js/jquery.suggestions.min.js', CClientScript::POS_END);
Yii::app()->getClientScript()->registerCssFile('https://cdn.jsdelivr.net/npm/suggestions-jquery@latest/dist/css/suggestions.min.css');

?>

    <div class="page-content cart-page-content">
        <div class="content">
            <?php $this->widget('application.components.MyTbBreadcrumbs', [
                'links' => $this->breadcrumbs,
            ]); ?>
            <h1><?= $this->title ?></h1>

            <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
                'id'          => 'order-form',
                'htmlOptions' => [
                    'class' => 'cart-form',
                ]
            ]); ?>

            <?= $form->errorSummary($order); ?>
            <!-- Информация из dadata заполняется автоматически -->
            <?= $form->hiddenField($order, 'address_obj') ?>
            <!-- Информация из dadata заполняется автоматически - end -->

            <div class="cart-section cart-section-2 fl fl-wr-w fl-ju-co-sp-b">
                <div class="cart-section__content">
                    <?php $this->widget('application.modules.cart.widgets.CartStaticWidget') ?>

                    <h4>Введите личные данные:</h4>
                    <div class="cart-form-content cart-form m-b-25">
                        <div class="row">
                            <div class="col-sm-6">
                                <?= $form->textFieldGroup($order, 'name', [
                                    'widgetOptions' => [
                                        'htmlOptions' => [
                                            'autocomplete' => 'off'
                                        ]
                                    ]
                                ]); ?>
                            </div>
                            <div class="col-sm-3">
                                <?= $form->telFieldGroup($order, 'phone', [
                                    'widgetOptions' => [
                                        'htmlOptions' => [
                                            'class'          => 'phone-mask',
                                            'data-phoneMask' => 'phone',
                                            'placeholder'    => 'Телефон',
                                            'autocomplete'   => 'off'
                                        ]
                                    ]
                                ]); ?>
                            </div>
                            <div class="col-sm-3">
                                <?= $form->textFieldGroup($order, 'email', [
                                    'widgetOptions' => [
                                        'htmlOptions' => [
                                            'autocomplete' => 'off'
                                        ]
                                    ]
                                ]); ?>
                            </div>
                        </div>
                        <div class="cart-form-content__terms_of_use cart-color-757575 cart-text-13-normal">
                            * - поля, отмеченные звёздочкой, обязательны для заполнения
                        </div>
                    </div>

                    <h4>Город получения товара</h4>

                    <div class="js-region-city cart-form">
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group">
                                    <?= $form->labelEx($order, 'fullAddress'); ?>
                                    <?= $form->textField($order, 'fullAddress', [
                                        'class' => 'form-control',
                                        'placeholder' => 'Например: г. Челябинск, ул. Доватора, 27'
                                    ]) ?>
                                    <div class="js-fulladdress-error help-block error hidden">Необходимо ввести полный адрес. Например: г. Челябинск, ул. Доватора, 27</div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="js-delivery-list" style="min-height: 70px;">
                        <?php if ($order->address_obj) : ?>
                            <?php $this->widget('application.modules.delivery.widgets.DeliveryWidget', [
                                'order' => $order,
                            ]); ?>
                        <?php endif ?>
                    </div>
                    <div class="cart-section__buttons cart-section-buttons fl fl-wr-w fl-ju-co-fl-e">
                        <div class="cart-section-buttons__item">
                            <?= CHtml::link('Назад', ['/cart/cart/index'], ['class' => 'bt-cart bt-cart-border-green']) ?>
                        </div>
                        <div class="cart-section-buttons__item">
                            <button type="submit" name="create" class="bt-cart bt-cart-red pickup-checkbox js-next-but-submit">Далее
                            </button>
                        </div>
                    </div>

                </div>
                <div class="cart-section__result">
                    <?php $this->widget('application.modules.cart.widgets.PriceOverview') ?>
                </div>
            </div>

            <?php $this->endWidget() ?>
        </div>
    </div>

<?php
Yii::app()->getClientScript()->registerScript(__FILE__, "
$('#Order_fullAddress').suggestions({
    token: '" . Yii::app()->dadata->token . "',
    type: 'ADDRESS',
    bounds: 'city-house',
    deferRequestBy: 500,
    minChars: 3,
    onSelectNothing: function (query){
        $('.js-next-but-submit').addClass('button-disabled');
        $('.js-fulladdress-error')
            .removeClass('hidden')
            .parents('.form-group').addClass('has-error')
            .find('input[type=text]').addClass('error');
    },
    onSelect: function(suggestion, changed) {
        if(!suggestion.data.postal_code || !suggestion.data.house) {
            $('.js-next-but-submit').addClass('button-disabled');
            $('.js-fulladdress-error')
                .removeClass('hidden')
                .parents('.form-group').addClass('has-error')
                .find('input[type=text]').addClass('error');
        } else {
            $('input[name=\'Order[address_obj]\'').val(JSON.stringify(suggestion.data));
            $('.preloader').addClass('active');
            $.ajax({
                url: '" . Yii::app()->createUrl('/delivery/deliveryAjax/deliveryList') . "',
                data: $('#order-form').serialize(),
                type: 'post',
                success: function(data) {
                    $('.js-delivery-list')
                        .fadeOut({
                            done: function() {
                                $(this)
                                    .html(data)
                                    .fadeIn();
                            }
                        });
                    $('.preloader').removeClass('active');
                },
                complete: function(){
                    $('.js-next-but-submit').removeClass('button-disabled');
                    $('.js-fulladdress-error').addClass('hidden')
                        .parents('.form-group').removeClass('has-error')
                        .find('input[type=text]').removeClass('error');;
                }
            })
        }
    },
});
")
?>